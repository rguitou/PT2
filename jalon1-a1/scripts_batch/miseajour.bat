@echo off

rem ------------------------------------------------------
rem Script de mise � jour global : 
rem - mise � jour de toutes les donn�es de la base, 
rem - ainsi que du site web.
rem 
rem Pr�requis :
rem - les programmes ont �t� compil�s (voir compiler.bat)
rem - la base a �t� cr��e (voir le script de cr�ation).
rem ------------------------------------------------------

set PROJET=Z:\Desktop\jalon1-a1
set BIN=%PROJET%\programmes\bin
set CONFIG_CSV=%PROJET%\config.csv
set DONNEES_CSV=%PROJET%\donnees\opendata\
set REQUETES_CREATION=%PROJET%\requetes\creation_base
set REQUETES_INSERTION=%PROJET%\requetes\insertion
set REQUETES_EXTRACTION=%PROJET%\requetes\extraction
set QUARTIERS_CSV=%REQUETES_EXTRACTION%\quartiers\quartiers.csv
set SITE=%PROJET%\site_web
set JSON_RELATIF=json
set JSON=%SITE%\%JSON_RELATIF%
set LOGS=%PROJET%\logs
set BD_LOGIN=-U ETD -P ETD
set BD_SERVEUR=info-dormeur
set BD_NOM=PT2_A1_1

rem ------------------------------------
rem Insertion des quartiers dans la base
rem ------------------------------------

set COLONNE_CLE_QUARTIER=1
set COLONNE_NOM_QUARTIER=3

echo insertion des quartiers : generation des requetes sql a partir de sigquartiers.csv
%BIN%\insererQuartiers %DONNEES_CSV%\sigquartiers.csv %REQUETES_CREATION%\quartiers.sql %COLONNE_CLE_QUARTIER% %COLONNE_NOM_QUARTIER%

echo insertion des quartiers : execution de la requete sql
sqlcmd -S %BD_SERVEUR% %BD_LOGIN% -d %BD_NOM% -i %REQUETES_CREATION%\quartiers.sql -o %LOGS%\quartiers.log

rem ---------------------------------------------------
rem Insertion des cat�gories et des points dans la base
rem ---------------------------------------------------

echo insertion des categories et points : generation sql a partir des fichiers csv
%BIN%\insererCategoriesEtPoints %CONFIG_CSV% %DONNEES_CSV% %REQUETES_INSERTION%\

echo insertion des categories et points : execution des requetes sql
rem on se deplace dans %REQUETES_INSERTION% puis on y ex�cute toutes les requ�tes de tous les fichiers
pushd %REQUETES_INSERTION%
for %%f in (*) do sqlcmd -S %BD_SERVEUR% %BD_LOGIN% -d %BD_NOM% -i %%f -o %LOGS%\insertion-%%f.log
popd

rem ----------------------
rem G�n�ration du site web
rem ----------------------


echo extraction des donnees depuis la base : sql vers csv


rem extraction des quartiers
sqlcmd -S %BD_SERVEUR% %BD_LOGIN% -d %BD_NOM% -i %REQUETES_EXTRACTION%\quartiers\quartiers.sql -o %QUARTIERS_CSV%  -s ";" -h-1 -W

rem generation des requetes d'extraction
%BIN%\extrairePoints %CONFIG_CSV% %QUARTIERS_CSV% %REQUETES_EXTRACTION%\

rem extraction des points, par categorie
pushd %REQUETES_EXTRACTION%
for %%f in (*) do sqlcmd -S %BD_SERVEUR% %BD_LOGIN% -d %BD_NOM% -i %%f -o %REQUETES_EXTRACTION%\csv\%%f.csv  -s ";" -h-1 -W
popd

rem extraction des points, par categorie et par quartier
pushd %REQUETES_EXTRACTION%\quartiers\categories
for %%f in (*) do sqlcmd -S %BD_SERVEUR% %BD_LOGIN% -d %BD_NOM% -i %%f -o %REQUETES_EXTRACTION%\quartiers\categories\csv\%%f.csv  -s ";" -h-1 -W
popd


echo extraction des donnees depuis la base : csv vers json


%BIN%\exportJson %CONFIG_CSV% %QUARTIERS_CSV% %REQUETES_EXTRACTION%\csv\ %JSON%\ %REQUETES_EXTRACTION%\ images/icones/


echo generation du site web


%BIN%\genererSite %CONFIG_CSV% %QUARTIERS_CSV% %SITE%\ %JSON_RELATIF%/ %SITE%\templates\ %REQUETES_EXTRACTION%\csv\
