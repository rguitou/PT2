/*
 * Fonctions utiles pour la génération de requêtes SQL.
 */
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include "sql.h"
#include "csv.h"
#include "chaines.h"

using namespace std;

/* Remplace le séparateur virgule par un point. */
string virgulePoint(string s) {
  string gauche, droite;
  bool ok = coupeEnDeux(s, ',', gauche, droite);
  if (!ok) {
	cerr << "Mauvais format numérique pour " << s << endl;
  }
  return gauche + "." + droite;
}

/* echappe (double) les guillemets simples dans une chaine de caracteres */
string echapperQuote(string s) {
  string chaine = s;
  size_t trouve = chaine.find("'");
  while (trouve != string::npos) {
	chaine.replace(trouve,1,"''");
	trouve = chaine.find("'", trouve+2);
  }
  return chaine;
}

/* Ajoute la requete SQL d'insertion d'un quartier dans le fichier SQL */
void sqlInsererQuartier(fstream &fichierSql,
						int cleQuartier, string nomQuartier) {
  fichierSql << "INSERT INTO "
			 << "QUARTIERS(Num_Quartier, Nom_Quartier) "
			 << "VALUES("
			 << cleQuartier << ", "
			 << "N'" << echapperQuote(nomQuartier) << "')"
			 << endl << endl;
}

/* Ajoute la requête SQL de suppression de tous les quartiers dans 
 * le fichier SQL.
 */
void sqlSupprimerQuartiers(fstream &fichierSql) {
  fichierSql << "DELETE FROM QUARTIERS" << endl << endl;
}

/* Ajoute la requete SQL d'insertion d'une categorie dans le fichier SQL */
void sqlInsererCategorie(fstream &fichierSql, string nomCategorie,
						 string cheminIcone, string date) {
  fichierSql << "INSERT INTO "
			 << "CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication) "
			 << "VALUES("
			 << "N'" << echapperQuote(nomCategorie) << "', "
			 << "N'" << echapperQuote(cheminIcone) << "', "
			 << "'" << date << "')" << endl << endl;
}

/* Ajoute la requete SQL de suppression des points d'une categorie
   dans le fichier SQL */
void sqlSupprimerPointsCategorie(fstream &fichierSql, string nomCategorie) {
  fichierSql << "DELETE FROM POINTS WHERE Num_Categorie IN "
			 << "(SELECT Num_Categorie FROM CATEGORIES "
			 << "WHERE Nom_Categorie = '" << echapperQuote(nomCategorie) << "')"
			 << endl << endl;
}

/* Ajoute la requete SQL de suppression d'une categorie dans le fichier SQL */
void sqlSupprimerCategorie(fstream &fichierSql, string nomCategorie) {
  fichierSql << "DELETE FROM CATEGORIES "
			 << "WHERE Nom_Categorie='" << echapperQuote(nomCategorie) << "'"
			 << endl << endl;
}

/* Ajoute la requete SQL d'insertion d'un point dans le fichier SQL */
void sqlInsererPoint(fstream &fichierSql, string nomCategorie, string x,
                     string y, string txt, string cle, bool avecQuartier,
                     string cleQuartier) {
  string reqQuartier;
  if (avecQuartier) {
	reqQuartier  = cleQuartier + ",";
  } else {
	reqQuartier  = "Null,";
  }
  
  fichierSql << "INSERT INTO "
			 << "POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,"
			 << "Num_Quartier, Num_Categorie) "
			 << "VALUES("
			 << virgulePoint(x) << ", "
			 << virgulePoint(y) << ", "
			 << "N'" << echapperQuote(txt) << "', "
			 << cle << ", "
			 << reqQuartier
			 << "(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = "
			 << "N'" << echapperQuote(nomCategorie) << "')"
			 << ")" << endl << endl ;
}

/* Ajoute la requete SQL d'extraction des points d'une categorie 
 * dans le fichier SQL.
 */
void sqlExtrairePointsCategorie(infoCategorie categorie, fstream &fichierSql) {
  fichierSql << "SET NOCOUNT ON" << endl
			 << "SELECT X_Longitude, Y_Latitude, Texte_Point, Chemin_icone"
			 << endl
			 << "FROM POINTS " << endl
			 << "  INNER JOIN CATEGORIES " << endl
			 << "    ON POINTS.Num_Categorie = CATEGORIES.Num_Categorie " <<endl
			 << "WHERE Nom_Categorie='"<< echapperQuote(categorie.nom) << "'"
			 << endl << endl;
}

/* Ajoute la requete SQL d'extraction des points d'une categorie, 
 * dans un quartier, vers le fichier SQL.
 */
void sqlExtrairePointsCategorieQuartier(infoCategorie categorie,
										infoQuartier quartier,
										fstream &fichierSql) {
  fichierSql << "SET NOCOUNT ON" << endl
			 << "SELECT X_Longitude, Y_Latitude, Texte_Point, Chemin_icone"
			 << endl
			 << "FROM POINTS " << endl
			 << "  INNER JOIN CATEGORIES " << endl
			 << "    ON POINTS.Num_Categorie = CATEGORIES.Num_Categorie " <<endl
			 << "WHERE Nom_Categorie='"<< echapperQuote(categorie.nom) << "'"
			 << endl
			 << "  AND POINTS.Num_Quartier="<< quartier.id << endl
			 << endl << endl;
}

/*
 * Nom du fichier contenant la requête SQL d'extraction des points d'une
 * catégorie, pour un quartier donné.
 */
string nomFichierSqlQuartier(string cheminSqlExtraction,
							 const infoQuartier &quartier,
							 const infoCategorie &categorie) {
  return cheminSqlExtraction + "quartiers/categories/" + int2string(quartier.id)
	+ "-" + categorie.id + ".sql";
}
