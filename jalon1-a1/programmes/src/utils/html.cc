/*
 * Quelques fonctions utiles pour la génération de code HTML.
 */
#include "chaines.h"
#include "html.h"

using namespace std;

/*
 * Contenu d'un mot-clé CHARGER_JSON_CATEGORIES pour une catégorie donnée.
 */
string chargerJsonCategories(string nomMap, string cheminJson,
							 infoCategorie categorie) {
  return "        " + nomMap + ".data.loadGeoJson('"
	+ cheminJson + categorie.id + ".json');\n";
}

/*
 * Contenu d'un mot-clé LISTE_PAGES_CATEGORIES pour une catégorie donnée,
 * sur la page index.html.
 */
string listePagesCategories(infoCategorie categorie) {
  return "<li><img src=\"images/icones/" + categorie.cheminIcone
  + "\"/> <a href=\"categories-" + categorie.id + ".html\">"
  + categorie.nom + "</a>\n";
}

/*
 * Contenu d'une ligne décrivant un point, sur la page d'une catégorie.
 */
string pointCategorie(string x, string y, string texte) {
  return "      <tr><td>" + x + "</td><td>" + y + "</td><td>" + texte
	+ "</td></tr>\n";
}

/*
 * Renvoie le nom d'une carte (map) pour un quartier donné.
 */
string nomDivMap(const infoQuartier &quartier) {
  return "map-" + int2string(quartier.id);
}

/*
 * Contenu d'un mot-clé SECTIONS_QUARTIERS de quartiers.html,
 * pour un quartier donné.
 */
string sectionQuartier(const infoQuartier &quartier) {
  string section = "\n<h2>" + quartier.nom + "</h2>\n";
  section += "<div id=\"" + nomDivMap(quartier)
	+ "\" class=\"map-canvas\"></div>\n";
  return section;
}

/*
 * Nom de la variable javascript associée à la map d'un quartier.
 */
string nomVarMap(infoQuartier quartier) {
  return "map_" + int2string(quartier.id);
}

/*
 * Contenu d'un mot-clé DECLARER_MAPS_QUARTIERS_CATEGORIES de quartiers.html,
 * pour un quartier donné.
 */
string declarerMapQuartier(const infoQuartier &quartier) {
  return "      var " + nomVarMap(quartier) + ";\n";
}

/*
 * Contenu d'un mot-clé CHARGER_JSON_QUARTIERS_CATEGORIES de quartiers.html,
 * pour un quartier donné.
 */ 
string chargerJsonQuartierCategories(const infoQuartier &quartier,
									 const vector<infoCategorie> &categories) {
  string chargerJson = "        " + nomVarMap(quartier)
	+ " = new google.maps.Map(document.getElementById('"
	+ nomDivMap(quartier) + "'), mapOptions);\n";
  for (infoCategorie categorie : categories) {
	if (categorie.colQuartier != -1) {
	  chargerJson += "        " + nomVarMap(quartier)
		+ ".data.loadGeoJson('json/"
		+ int2string(quartier.id) + "-" + categorie.id + ".json');\n";
	}
  }
  return chargerJson;
}

/*
 * Contenu d'un mot-clé SETSTYLE_MAPS_QUARTIERS de quartiers.html,
 * pour un quartier donné.
 */ 
string setStyleMapQuartier(const infoQuartier &quartier) {
  string indent = "        ";
  string setStyleMap = indent +
	nomVarMap(quartier) + ".data.setStyle(setColorStyleFn);\n";
  setStyleMap += indent + nomVarMap(quartier)
	+ ".data.addListener('mouseover', function(event) {\n";
  setStyleMap += indent
	+ "  infowindow.setContent(event.feature.getProperty('description'));\n";
  setStyleMap += indent + "  infowindow.setPosition(event.latLng);\n";
  setStyleMap += indent +
	"  infowindow.setOptions({pixelOffset: new google.maps.Size(0,-34)});\n";
  setStyleMap += indent + "  infowindow.open(" + nomVarMap(quartier) + ");\n";
  setStyleMap += indent + "});\n";
  setStyleMap += indent + "google.maps.event.addListener("
	+ nomVarMap(quartier) + ", 'click', function() {\n";
  setStyleMap += indent
	+ "  infowindow.close(); // un clic sur la carte ferme les infowindows\n";
  setStyleMap += indent + "});\n";
  return setStyleMap;
}
