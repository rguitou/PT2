#ifndef HTML_H
#define HTML_H

#include <string>
#include "csv.h"

std::string chargerJsonCategories(std::string nomMap, std::string cheminJson,
								  infoCategorie categorie);

std::string listePagesCategories(infoCategorie categorie);

std::string pointCategorie(std::string x, std::string y, std::string texte);

std::string sectionQuartier(const infoQuartier &quartier);

std::string declarerMapQuartier(const infoQuartier &quartier);

std::string chargerJsonQuartierCategories(const infoQuartier &quartier,
										  const std::vector<infoCategorie> &categories);

std::string setStyleMapQuartier(const infoQuartier &quartier);
#endif // HTML_H
