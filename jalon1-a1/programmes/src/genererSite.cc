/* Générer le site web, à partir :
 * - des catégories présentes dans config.csv,
 * - des données extraites de la base (quartiers, points),
 * - des templates de pages web.
 */
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "utils/csv.h"
#include "utils/html.h"

using namespace std;

/*
 * On génère toutes les parties du site (templates) qui dépendent des 
 * catégories. Les données sur les catégories sont récupérées depuis config.csv.
 */
void lireCategories(map<string,string> &motsClesTemplates,
					string configCsv, string cheminJson,
					vector<infoCategorie> &categories) {

    ifstream configCsvFic;
    string ligne;

    // ouverture fichier csv en lecture
    configCsvFic.open(configCsv.c_str(), ios::in);
    if (configCsvFic.fail()) {
        cerr << "ouverture du fichier " << configCsv
			 << " impossible." << endl;
        exit(EXIT_FAILURE);
    }
	
    // lecture des lignes du fichier de configuration
	bool premiereLigne = true;
    while (getline(configCsvFic, ligne)) {
	    if (premiereLigne) { // on ne traite pas la ligne des titres
		    premiereLigne = false;
		    continue;
	    }
		infoCategorie categorie;
		bool ok = lireLigneConfigCsv(ligne, configCsv, categorie);
		if (!ok) {
		  break;
		}
		categories.push_back(categorie);
		// on ajoute l'import du json de la catégorie dans index.html
		motsClesTemplates["[[CHARGER_JSON_CATEGORIES]]"] +=
		  chargerJsonCategories("map", cheminJson, categorie);
		// on ajoute la catégorie dans la liste sur la page index.html
		motsClesTemplates["[[LISTE_PAGES_CATEGORIES]]"] += 
		  listePagesCategories(categorie);
    }
    configCsvFic.close();
}

/*
 * Lecture du fichier quartiers.csv pour remplir le template des quartiers
 * avec le bon contenu.
 */
void lireQuartiers(map<string,string> &motsClesTemplates,
				   string nomQuartiersCsv, vector<infoCategorie> categories) {
  vector<infoQuartier> quartiers;
  // on lit le fichier quartiers.csv
  chargerQuartiers(nomQuartiersCsv, quartiers);
  for (infoQuartier quartier : quartiers) {
	// on ajoute, pour chaque quartier sur quartiers.html :
	// - la déclaration de la variable javascript de sa map
	motsClesTemplates["[[DECLARER_MAPS_QUARTIERS]]"] +=
	  declarerMapQuartier(quartier);
	// - l'initialisation de la variable javascript de la map
	motsClesTemplates["[[CHARGER_JSON_QUARTIERS_CATEGORIES]]"] +=
	  chargerJsonQuartierCategories(quartier, categories);
	// - le chargement du style de la map
	motsClesTemplates["[[SETSTYLE_MAPS_QUARTIERS]]"] +=
	  setStyleMapQuartier(quartier);
	// - sa section
	motsClesTemplates["[[SECTIONS_QUARTIERS]]"] += sectionQuartier(quartier);
  }
}

/* Renvoie la liste des points d'une catégorie, au format html. 
 * Pour cela on lit le fichier csv généré après extraction de la base.
 */
string listePointsCategorie(infoCategorie categorie, string cheminCsv) {

  string listePoints;
  
  // ouverture fichier csv en lecture
  string nomFichierCsv = cheminCsv + categorie.id + ".sql.csv";
  ifstream fichierCsv;
  fichierCsv.open(nomFichierCsv.c_str(), ios::in);
  if (fichierCsv.fail()) {
	cerr << "ouverture du fichier " << nomFichierCsv
		 << " impossible." << endl;
	exit(EXIT_FAILURE);
  }
  
  // lecture des lignes du fichier csv
  string ligne;
  while (getline(fichierCsv, ligne)) {
	int nbCol;
	TabLigne colonnes;
	coupeEnPlusieurs(ligne, CSV_SEP, nbCol, colonnes);
	if (nbCol!=NB_COL_CATEGORIE_CSV) {
	  cerr << "Une ligne du fichier " << nomFichierCsv;
	  cerr << " ne contient pas le bon nombre de colonnes : " << endl;
	  cerr << ligne << endl << "Fichier ignoré." << endl;
	  break;
	}
	string x = colonnes[0];
	string y = colonnes[1];
	string texte = colonnes[2];
	string icone = colonnes[3];
	listePoints += pointCategorie(x, y, texte);
  }
  fichierCsv.close();
  return listePoints;
}

/*
 * Dans une chaine, remplacer tous les mots-clés par leur valeur.
 * Limitations : 
 * - on suppose une seule substitution par ligne, par mot-clé
 * - on suppose qu'un mot-clé n'est pas remplacé par un autre...
 */
string substituer(string &chaine, const map<string,string> &chainesARemplacer) {
  string sortie = chaine;
  for (auto chaineARemplacer : chainesARemplacer) {
	string motcle = chaineARemplacer.first;
	string valeur = chaineARemplacer.second;
	size_t position = sortie.find(motcle);
	if (position != string::npos) {
	  sortie.replace(position, motcle.length(), valeur);
	}
  }
  return sortie;
}

/*
 * Lecture d'un fichier template : on remplace chaque mot-clé à remplacer
 * par une valeur, pour constituer un nouveau fichier.
 */
void remplacerChaines(string nomFichierEntree, string nomFichierSortie,
					  const map<string,string> &chainesARemplacer) {

  // ouverture du fichier en entrée
  ifstream fichierEntree;
  fichierEntree.open(nomFichierEntree.c_str(), ios::in);
  if (fichierEntree.fail()) {
	cerr << "ouverture du fichier " << nomFichierEntree << " impossible."
		 << endl;
	exit(EXIT_FAILURE);
  }
  
  // ouverture du fichier en sortie
  fstream fichierSortie;
  fichierSortie.open(nomFichierSortie.c_str(), ios::out);
  if (fichierSortie.fail()) {
	cerr << "ouverture du fichier " << nomFichierSortie << " impossible."
		 << endl;
	exit(EXIT_FAILURE);
  }

  // remplacement des mots-clés par leur valeur sur chaque ligne
  string ligne;
  while (getline(fichierEntree, ligne)) {
	fichierSortie << substituer(ligne, chainesARemplacer) << endl;
  }

  // fermeture
  fichierEntree.close();
  fichierSortie.close();  
}

/*
 * Initialise la map des chaines à remplacer (mots-clés des templates).
 */
void initialiserMotsClesTemplates(map<string,string> &motsClesTemplates) {
  motsClesTemplates["[[CHARGER_JSON_CATEGORIES]]"] = "";
  motsClesTemplates["[[NOM_CATEGORIE]]"] = "";
  motsClesTemplates["[[LISTE_POINTS_CATEGORIE]]"] = "";
  motsClesTemplates["[[LISTE_PAGES_CATEGORIES]]"] = "";
  motsClesTemplates["[[SECTIONS_QUARTIERS]]"] = "";
  motsClesTemplates["[[DECLARER_MAPS_QUARTIERS]]"] = "";
  motsClesTemplates["[[CHARGER_JSON_QUARTIERS_CATEGORIES]]"] = "";
  motsClesTemplates["[[SETSTYLE_MAPS_QUARTIERS]]"] = "";
  motsClesTemplates["[[SECTIONS_QUARTIERS]]"] = "";
}

/*
 * Analyse de la ligne de commande.
 */
void analyserLigneDeCommande(int argc, char *argv[],
							 string &configCsv, string &quartiersCsv,
							 string &cheminSite, string &cheminJson,
							 string &cheminTemplates,
							 string &cheminExtractions) {
    if( argc != 7 ) {
	  cout << "Erreur : nombre d'arguments incorrect." << endl;
	  cout << "Usage : " << argv[0]
		   << " config.csv quartiers.csv cheminSite cheminJson cheminTemplates "
		   << "cheminExtractions" << endl
		   << " Les chemins doivent se terminer par un séparateur "
		   << "(/ ou \\ selon le système)." << endl;
	  exit(-1);
	}
	configCsv  = argv[1];
	quartiersCsv  = argv[2];
	cheminSite = argv[3];
	cheminJson = argv[4];
	cheminTemplates = argv[5];
	cheminExtractions = argv[6];
}

/*
 * Procédure principale.
 */
int main( int argc, char* argv[] )
{
  string configCsv, quartiersCsv;
  string cheminSite, cheminJson, cheminTemplates, cheminExtractions;
  vector<infoCategorie> categories;
  
  // chaines à remplacer (mots-clés) dans les templates html
  map<string,string> motsClesTemplates;
  initialiserMotsClesTemplates(motsClesTemplates);

  // analyse de la ligne de commande et ouverture des fichiers
  analyserLigneDeCommande(argc, argv, configCsv, quartiersCsv, cheminSite,
						  cheminJson, cheminTemplates, cheminExtractions);

  // on lit le fichier config.csv, et on construit les chaînes qui
  // remplaceront les mots-clés dans les templates
  lireCategories(motsClesTemplates, configCsv, cheminJson, categories);

  // on génère les pages html, en remplaçant dans les templates les mots-clés
  // par leur valeur :
  string pageTemplate, pageHtml;

  // - page index.html
  pageTemplate = cheminTemplates + "index.html";
  pageHtml = cheminSite + "index.html";
  remplacerChaines(pageTemplate, pageHtml, motsClesTemplates);

  // - pages des categories (jeux de données)
  for (infoCategorie categorie : categories) {
	motsClesTemplates["[[NOM_CATEGORIE]]"] = categorie.nom;
	motsClesTemplates["[[LISTE_POINTS_CATEGORIE]]"] =
	  listePointsCategorie(categorie, cheminExtractions);
	motsClesTemplates["[[CHARGER_JSON_CATEGORIE]]"] =
	  chargerJsonCategories("map", cheminJson, categorie);
	pageTemplate = cheminTemplates + "categorie.html";
	pageHtml = cheminSite + "categories-" + categorie.id + ".html";
	remplacerChaines(pageTemplate, pageHtml, motsClesTemplates);
  }

  // - page des quartiers
  pageTemplate = cheminTemplates + "quartiers.html";
  pageHtml = cheminSite + "quartiers.html";
  lireQuartiers(motsClesTemplates, quartiersCsv, categories);
  remplacerChaines(pageTemplate, pageHtml, motsClesTemplates);
  
  return EXIT_SUCCESS;
}
