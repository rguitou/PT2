/*
 * Programme générant les requêtes SQL d'insertion des quartiers dans la base.
 */
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>
#include "utils/csv.h"
#include "utils/sql.h"

using namespace std;

/*
 * Traiter une ligne de quartiers.csv, c'est-à-dire un quartier.
 */
void traiterQuartier(string ligne, string nomFichierCsv, fstream &fichierSql,
					 int colCle, int colNom) {
  // lecture de la ligne d'un quartier dans le fichier csv
  int nbCol;
  TabLigne colonnes;
  const int nbColConfig = 6;
  coupeEnPlusieurs(ligne, CSV_SEP, nbCol, colonnes);
  if (nbCol!=nbColConfig) {
	cerr << "Une ligne du fichier " << nomFichierCsv;
	cerr << " ne contient pas le bon nombre de colonnes : " << endl;
	cerr << ligne << endl;
  }
  int cleQuartier = atoi(colonnes[colCle].c_str()); // stoi()
  string nomQuartier = colonnes[colNom];

  // ajout de la requete d'insertion du quartier, dans le fichier SQL
  sqlInsererQuartier(fichierSql, cleQuartier, nomQuartier);
}

/*
 * Ouverture du fichier sigquartiers.csv, et pour chacune de ses lignes,
 * traitement du quartier correspondant.
 */
void traiterQuartiers(string nomQuartiersCsv, string nomQuartiersSql,
					  int colCle, int colNom) {

    ifstream quartiersCsv;
    string ligne;

    // ouverture fichier csv en lecture
    quartiersCsv.open(nomQuartiersCsv.c_str(), ios::in);
    if (quartiersCsv.fail()) {
        cerr << "ouverture du fichier " << nomQuartiersCsv
			 << " impossible." << endl;
        exit(EXIT_FAILURE);
    }
	
	// ouverture fichier sql en ecriture
	fstream quartiersSql;
	quartiersSql.open(nomQuartiersSql.c_str(), ios::out);
	if (quartiersSql.fail()) {
	  cerr << "ouverture du fichier " << nomQuartiersSql
		   << " impossible." << endl;
	  exit(EXIT_FAILURE);
	}

	// suppression des anciens quartiers
	sqlSupprimerQuartiers(quartiersSql);

    // lecture des lignes du fichier des quartiers
	bool premiereLigne = true;
    while (getline(quartiersCsv, ligne)) {
	    if (premiereLigne) { // on ne traite pas la ligne des titres
		    premiereLigne = false;
		    continue;
	    }
		traiterQuartier(ligne, nomQuartiersCsv, quartiersSql, colCle, colNom);
    }
    quartiersCsv.close();
	quartiersSql.close();
}

/*
 * Analyse de la ligne de commande.
 */
void analyserLigneDeCommande(int argc, char *argv[],
							 string &cheminCsv, string &cheminSql,
							 int &colCle, int &colNom){

    if( argc != 5 ) {
	  cout << "Erreur : nombre d'arguments incorrect." << endl;
	  cout << "Usage : " << argv[0]
		   << " sigquartiers.csv sigquartiers.sql colCle colNom" << endl;
	  exit(-1);
	}
	cheminCsv = argv[1];
	cheminSql = argv[2];
	colCle = atoi(argv[3])-1;
	colNom = atoi(argv[4])-1;
}

/*
 * Procédure principale.
 */
int main( int argc, char* argv[] )
{
  string cheminCsv, cheminSql;
  int colCle, colNom;
  analyserLigneDeCommande(argc, argv, cheminCsv, cheminSql, colCle, colNom);
  traiterQuartiers(cheminCsv, cheminSql, colCle, colNom);
  return EXIT_SUCCESS;
}
