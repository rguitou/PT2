DELETE FROM QUARTIERS

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(7, N'Bastide')

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(2, N'Chartrons - Grand Parc - Jardin Public')

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(3, N'Centre ville')

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(5, N'Nansouty - Saint Genès')

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(6, N'Bordeaux Sud')

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(4, N'Saint Augustin - Tauzin - Alphonse Dupeux')

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(8, N'Caudéran')

INSERT INTO QUARTIERS(Num_Quartier, Nom_Quartier) VALUES(1, N'Bordeaux Maritime')

