DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Parcs, jardins et squares')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Parcs, jardins et squares'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication) VALUES(N'Parcs, jardins et squares', N'tree.png', '06/05/2015')

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59848036133239, 44.8264628940148, N'Parc de la Béchade', 1336, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56363500914887, 44.8490860371795, N'Parc aux Angéliques séquence 1', 1115, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5804615766991, 44.8378156427562, N'Jardin de la Mairie', 1119, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56352782729974, 44.8634384524561, N'Parc Chantecrit', 1117, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.53980293808487, 44.8856333789349, N'Parc des Berges de Garonne', 1161, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5775805296348, 44.8192322450814, N'Square Bertrand de Goth', 1165, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.63152446326111, 44.8541666320585, N'Parc Monséjour', 1166, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5649715593407, 44.8463501049228, N'Parc des Berges de Queyries', 1285, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54285074157771, 44.8769884542364, N'Aire de jeux du Parc du Port de la Lune', 1126, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57124378012408, 44.872849410881, N'Parc de la Cité des Aubiers', 1351, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56241149721864, 44.8291975217503, N'Jardin André Meunier', 1350, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57635112519142, 44.8883743911242, N'Berges du lac', 1446, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57027209701738, 44.846346095224, N'Terrasse des Quinconces', 1358, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54152067753823, 44.8772540959615, N'Parc du Port de la Lune', 1360, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60335594695551, 44.8303537416189, N'La Maison aux personnages des Kabakov', 1445, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58058202793965, 44.8411412725735, N'Place Gambetta', 1447, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56897029888861, 44.8416916800611, N'Jardin des Lumières', 1448, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.62224804013049, 44.8575876871705, N'Parc de la piscine Stéhélin', 1048, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61458668909203, 44.8458218119136, N'Square Armand Faulat', 1184, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570369978126, 44.8642805685378, N'Square Haussmann', 1368, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59238201224352, 44.8263184637852, N'Square des Tilleuls', 1369, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57144978236588, 44.8394332588418, N'Square Vinet', 1171, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55440958300831, 44.8155134613628, N'Jardin Brascassat', 1050, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56025882919705, 44.8611222721839, N'Square Joséphine', 1137, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54551958250028, 44.8508400589585, N'Square Souriaux', 1392, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54539587286897, 44.8442351736474, N'Parc Pinçon', 1391, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60242645385582, 44.8529143319951, N'Parc Bordelais', 1414, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58533428258489, 44.8427104724202, N'Square des Martyrs de la Résistance', 1053, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57244915044476, 44.8142929529499, N'Jardin d''Ars', 1378, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56041431503595, 44.8310992826452, N'Square Dom Bedos', 1421, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58881728045945, 44.8306854976094, N'Aire de jeux place d''Arlac', 1394, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5817728525313, 44.8371307857454, N'Square Saint John Perse', 1395, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58206181056096, 44.8378291814857, N'Square André Lhôte', 1396, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56961128095368, 44.8366388717985, N'Square Jean Bureau', 1397, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57836299172279, 44.8489591240912, N'Jardin Public', 1185, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58198719193787, 44.8591673209751, N'Square de l''Europe', 1058, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59492906425748, 44.8228499690165, N'Square Valmy', 1379, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57613254506874, 44.8185477209283, N'Eglise Sainte Geneviève', 1383, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5731224718106, 44.9034039913513, N'Bois de Bordeaux', 1401, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54402749865433, 44.8741199689853, N'Parc de Bacalan', 1404, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56165587940918, 44.9041430679512, N'Parc Floral', 1405, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60694555876733, 44.8402748527427, N'Jardin de Lussy', 1406, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58682066323661, 44.8551293940604, N'Parc Rivière', 1408, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61485387468071, 44.8551224268659, N'Square Honoré d''Estienne d''Orves', 1201, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58203835007396, 44.825017299018, N'Jardin des Dames de la foi', 1074, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61122843553524, 44.8330696740192, N'Jardin de Lili', 1076, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61461002009681, 44.8512589464279, N'Square de l''église Saint Amand', 1388, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58735051211613, 44.8915971007383, N'Berges du lac_Nord', 1389, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60800164142515, 44.8365622040709, N'Square Emile Combes', 1409, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.6236382808561, 44.8567844247201, N'Parc Stéhélin', 1213, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56416506274888, 44.8457883297168, N'Place des Droits de l''Enfant', 1416, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57024382926165, 44.8194923120669, N'Square Paul Antin', 1417, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57389228082191, 44.8178483205415, N'Square Jean Mermoz', 1419, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56080700911914, 44.8474737240455, N'Square Reignier', 1221, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57535658264159, 44.8322299683336, N'Place Francis de Pressensé', 1223, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56523831759999, 44.8307363466072, N'Jardin des Remparts', 40692, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56077049435909, 44.8592991849519, N'Jardin de ta soeur', 1092, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57713029127432, 44.8247482807509, N'Square Argonne', 1422, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.6371486173325, 44.8601641564201, N'Square Les Jasmins', 1425, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59328791291832, 44.8419990651074, N'Jardin Georges Mandel', 1232, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59507875635849, 44.8483120393279, N'Jardin de la Visitation', 1237, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.6170311381988, 44.860619757365, N'Square Hortense Schneider', 1318, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60859796110473, 44.8236473227632, N'Parc de la Cité Carreire', 1428, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5441542793271, 44.8793053251661, N'Square docteur Roger Hypoustéguy', 1429, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57978118078902, 44.8827414268288, N'Berges du lac', 1434, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5604283051404, 44.8511076592916, N'Parc aux Angéliques séquence 2', 1120, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59364380670929, 44.8331451702635, N'Square Gaviniès', 1211, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57666592556124, 44.8643386874518, N'Square Charazac', 1046, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59906513926498, 44.8198199113962, N'Square Saint Julien', 1056, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56394836801076, 44.830691565307, N'Jardin des Cèdres', 40693, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58170859217271, 44.8606333884683, N'Parc de la Cité du Grand Parc', 1128, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61259594932301, 44.8246499654276, N'Square Alfred Smith', 1088, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56091738189362, 44.8337029237602, N'Parc des Sports Saint Michel', 40919, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56631019968645, 44.8131424881763, N'Square Liotard', 1309, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs, jardins et squares'))

