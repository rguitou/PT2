DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Aires de jeux')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Aires de jeux'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication) VALUES(N'Aires de jeux', N'themepark.png', '01/01/2017')

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578932285046847, 44.8779825499956, N'Berges du Lac_Plage', 976, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564294382315667, 44.845843033572, N'Place des Droits de l''Enfant', 978, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594928469747018, 44.8228503127685, N'Place Valmy', 984, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575308846560817, 44.8323190884165, N'Place Francis de Pressensé', 45881, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578084705837757, 44.8613135443103, N'Parc de la Cité du Grand Parc_Maryse Bastié', 40898, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.631524036048745, 44.8538518191816, N'Parc Monséjour', 40824, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545712896408886, 44.8463829625899, N'Square du Petit Cardinal', 40899, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544462820497935, 44.8727664707991, N'Parc de Bacalan', 960, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594768197776438, 44.8482839595445, N'Jardin de la Visitation', 40872, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588817278555827, 44.830685473849, N'place d''Arlac_Aire de jeux', 962, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580467635372337, 44.8381053574816, N'Jardin de la Mairie', 963, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581809575211002, 44.8250637051958, N'Jardin des Dames de la Foi', 968, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.542464593073604, 44.8769180696157, N'Parc du Port de la Lune', 969, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566047983306671, 44.8127561958737, N'Jardin des Barrières', 971, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549829719336809, 44.8430369803828, N'Place Calixte Camelle', 40875, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607728419248386, 44.8364815587535, N'Square Emile Combes-Jean Gautier', 40849, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57750733295014, 44.8228859790501, N'Place Simiot', 40836, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606153526358133, 44.8402181154232, N'Jardin de Lussy', 1015, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599399085518086, 44.8196421534657, N'Jardin Saint Julien', 40851, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.612272380436312, 44.8245679330357, N'Square Alfred Smith', 40856, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560307095016231, 44.8335386001928, N'Parc des Sports Saint Michel', 40904, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564612266385122, 44.8154946799513, N'Place Renée Seilhan', 1003, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577327990473745, 44.8248715320859, N'Square du professeur Jacques Lasserre', 40876, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.620455441301507, 44.8443806327835, N'Square Raymond Poincaré', 40878, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.593639599170848, 44.8420676913323, N'Square Georges Mandel', 40880, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576508032339511, 44.8641996580728, N'Square Charazac', 40891, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569623430678419, 44.8497072444067, N'quai des Chartrons-Arnozan_Aire de jeux', 40905, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567278783082032, 44.852533050652, N'quai des Chartrons-Martinique_Aire de jeux', 40906, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564434590686411, 44.8267800251846, N'Place Pierre Jacques Dormoy', 40869, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.543905779435418, 44.8789869166071, N'Square docteur Roger Hypoustéguy', 40795, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583399365585414, 44.8601359558296, N'Parc de la Cité du Grand Parc_Grands', 40893, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576945108618767, 44.8777453255515, N'Berges du Lac_Forêt', 40835, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569550040671983, 44.8366080098571, N'Square Jean Bureau', 40862, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.604169377584603, 44.8550083242965, N'Parc Bordelais_Petits', 40914, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603913460094544, 44.8552821579487, N'Parc Bordelais_Grands', 40915, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601930413206061, 44.852085248005, N'Parc Bordelais_Déversoir', 40916, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562713800846843, 44.82960983829, N'Jardin André Meunier', 40918, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560577193750791, 44.8594404221145, N'Jardin de ta soeur', 40909, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.611226480755493, 44.8330687982863, N'Jardin de Lili', 42297, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.637034715036613, 44.8601579895219, N'Square Les Jasmins', 40827, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563354176711058, 44.8638072716621, N'Parc Chantecrit_Moyens', 40828, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54544383849291, 44.8508782353175, N'Square des Copains', 40832, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56055847497138, 44.847486526758, N'Square Reignier', 40830, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599721018971519, 44.8265265956558, N'Jardin de la Béchade', 42298, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583948275126051, 44.8372044629341, N'Esplanade Charles de Gaulle', 965, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571436497969608, 44.8394516858381, N'Square Vinet', 40833, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549890337180739, 44.8702075292271, N'Place Adolphe Buscaillet', 967, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.614805105067077, 44.8509155096914, N'Square de l''église Saint Amand', 997, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563287282512679, 44.8222611454799, N'Place du Cardinal Donnet', 40842, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577603926455313, 44.8192915109707, N'Square Bertrand de Goth', 40857, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55412097743344, 44.8155067417108, N'Jardin Brascassat', 40860, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585220021875601, 44.8430008749437, N'Place des Martyrs de la Résistance', 1017, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.608569420087154, 44.823651569193, N'Parc de la Cité Carreire', 70430, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581987180251547, 44.859167315628, N'Square de l''Europe', 46485, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545692699828547, 44.8451467415558, N'Parc Pinçon', 45168, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571024416165762, 44.8725407046512, N'Parc de la Cité des Aubiers_Bâtiment', 48893, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586390674070841, 44.8544138007459, N'Parc Rivière', 57015, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579280485024505, 44.8483363748257, N'Jardin Public_Muséum', 60749, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.593565572127199, 44.8331486482628, N'Square Gaviniès', 69800, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603242135583595, 44.8504577278356, N'Parc Céré_Grands', 69022, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603808092313818, 44.8502268759373, N'Parc Céré_PMR', 69023, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564238820336955, 44.8634203668993, N'Parc Chantecrit_Petits', 64355, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574048207388075, 44.8177708226525, N'Jardin de la Croix du Sud', 64356, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570523260577036, 44.8641107265022, N'Square Etienne Morin', 64357, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603742359633456, 44.8505067841067, N'Parc Céré_Petits', 64360, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566734582402586, 44.8324430923504, N'Place du Maucaillou', 64362, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583446898339576, 44.8602296575597, N'Parc de la Cité du Grand Parc_Petits', 64363, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583409410144762, 44.8604914850486, N'Parc de la Cité du Grand Parc_Moyens', 64364, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572532877696808, 44.8749930325333, N'Aire de jeux du Lauzun', 66690, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551058234269053, 44.8241092098202, N'Place Ferdinand Buisson', 64702, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577150600491842, 44.8490933005025, N'Jardin Public_Ile aux enfants', 64506, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572511935201098, 44.8778975649973, N'Jardin du Petit Nicolas', 68992, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

