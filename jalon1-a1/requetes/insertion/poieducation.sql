DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Ecoles maternelles et primaires')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Ecoles maternelles et primaires'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication) VALUES(N'Ecoles maternelles et primaires', N'school.png', '01/01/2017')

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55447793065199, 44.8413305452594, N'Ecole Elémentaire Montaud', 80, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569335285909121, 44.8242278061674, N'Ecole Elémentaire Cazemajor', 81, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.550965680035777, 44.8237515977361, N'Ecole Maternelle Beck', 82, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559761449556435, 44.8314735336634, N'Conservatoire Jacques Thibaud', 83, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576114758488725, 44.8325755048473, N'Ecole Elémentaire Henri IV', 84, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599040567518977, 44.8470694184455, N'Ecole maternelle et élémentaire Bon Pasteur', 85, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567490180592419, 44.8329455111254, N'Ecole Elémentaire Menuts', 86, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588591298327565, 44.8437053413673, N'Ecole Maternelle Paix', 87, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.624373496904539, 44.8569860920593, N'Ecole Maternelle Stéhélin', 88, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.615633968715867, 44.8548952814368, N'Ecole Maternelle Saint André', 90, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569845322686287, 44.8727399534919, N'Ecole Elémentaire Lac II', 91, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545467003619194, 44.8462197201296, N'Ecole Elémentaire Benauge', 94, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567331973751481, 44.8333276731152, N'Ecole Maternelle Menuts', 95, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568400274399978, 44.8563882628963, N'Ecole Elémentaire Stendhal', 96, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574897231231871, 44.8750171172332, N'Ecole Maternelle Lac III', 97, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580072929574423, 44.8589399488308, N'Ecole Maternelle Condorcet', 98, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561373638615043, 44.8244186420298, N'Ecole Elémentaire Francin', 99, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.589521498186354, 44.8385131980181, N'Ecole Elémentaire Saint Bruno', 101, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574501622153541, 44.872920047126, N'Ecole Elémentaire Jean Monnet', 103, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56800623302867, 44.8561010120436, N'Ecole Maternelle Stendhal', 104, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570973806595309, 44.8638512927303, N'Ecole Elémentaire Sousa Mendes', 105, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576102106402274, 44.827488259415, N'Ecole Maternelle Argonne', 106, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582319868406437, 44.8390103568129, N'Ecole Maternelle Anatole France', 108, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569773835604797, 44.856515840459, N'Ecole Elémentaire Balguerie', 112, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583335452130717, 44.8507446123758, N'Ecole Maternelle Lagrange', 114, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584972138811141, 44.8477768199648, N'Ecole Maternelle Naujac', 115, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.615256988865278, 44.851185998904, N'Ecole Maternelle Paul Lapie', 117, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566854917866641, 44.8338996419148, N'Ecole maternelle et élémentaire Saint Michel', 118, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561602525781096, 44.8446369357307, N'Ecole Elémentaire Nuyens', 119, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58997320034672, 44.8437514216124, N'Ecole maternelle et élémentaire Saint Seurin', 120, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561247855964491, 44.8443756899922, N'Ecole Maternelle Nuyens', 121, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.589270609913704, 44.8380513089314, N'Ecole Maternelle Saint Bruno', 122, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574293001732203, 44.8727992341853, N'Ecole Maternelle Jean Monnet', 123, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609646812666569, 44.8332837675547, N'Ecole Maternelle Flornoy', 125, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610564897212831, 44.83317032475, N'Ecole Elémentaire Flornoy', 126, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.553716181260891, 44.8415386782168, N'Ecole Maternelle Nuits', 127, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56256303289267, 44.8274863708908, N'Rock School Barbey', 128, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561364932424993, 44.8715389730358, N'Ecole du Cirque', 129, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.625137360418585, 44.858821490874, N'Ecole Elémentaire Pins Francs', 130, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.589313097423492, 44.8462623799653, N'Ecole maternelle et élémentaire Saint Gabriel', 131, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571951303315331, 44.8802056716655, N'Ecole Maternelle Vaclav Havel', 133, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586090060167888, 44.8242827042556, N'Ecole Elémentaire Jacques Prévert', 135, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588716899564366, 44.8312758709457, N'Ecole Maternelle Alphonse Dupeux', 136, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584626774797832, 44.8507480245209, N'Ecole Elémentaire David Johnston', 137, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574125895757045, 44.8261693368375, N'Ecole Elémentaire Deyries Sablières', 138, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586610072942437, 44.8445838519938, N'Ecole Elémentaire Albert Barraud', 139, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580784844484795, 44.86204654042, N'Ecole Elémentaire Albert Schweitzer', 140, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.619519118989713, 44.8445938058908, N'Ecole Elémentaire Raymond Poincaré', 141, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575595349211268, 44.8323094471799, N'Ecole Maternelle Francis de Pressensé', 142, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572112033194457, 44.8564390618662, N'Ecole Maternelle Paul Berthelot', 143, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.550879439893571, 44.8237987739627, N'Ecole Elémentaire Ferdinand Buisson', 144, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594363253050396, 44.8521363067669, N'Ecole maternelle et élémentaire L''Assomption', 148, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588593270850824, 44.8310808978086, N'Ecole Elémentaire Alphonse Dupeux', 149, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.622899911146813, 44.8641464484701, N'Ecole maternelle et élémentaire Saint Ferdinand', 150, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572543686736514, 44.8281287455902, N'Ecole maternelle et élémentaire Saint Julien Victoire', 151, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574682489872842, 44.8521243013726, N'Ecole maternelle et élémentaire Saint Louis - Sainte Thérèse', 152, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552546684087625, 44.8745148042218, N'Ecole Maternelle Charles Martin', 153, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555670059876923, 44.8157640751242, N'Ecole Maternelle Carle Vernet', 154, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570422015437927, 44.8637030797431, N'Ecole Maternelle Sousa Mendes', 155, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.615225129694624, 44.8520602594335, N'Ecole Elémentaire Paul Lapie', 156, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560561050805088, 44.8304990274583, N'Ecole d''enseignement supérieur d''art de Bordeaux - EBABX', 157, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580466573236072, 44.8617574194043, N'Ecole Maternelle Albert Schweitzer', 159, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.631343573607823, 44.844894280781, N'Ecole Maternelle Clos Montesquieu', 160, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607784149969315, 44.8349970723336, N'Ecole maternelle et élémentaire Sainte Monique', 161, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58090838514396, 44.8249040592416, N'Ecole maternelle et élémentaire Saint Genès', 163, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596906072789333, 44.8536781651991, N'Ecole maternelle et élémentaire Saint Joseph de Tivoli', 164, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606068659450576, 44.850293018859, N'Ecole maternelle et élémentaire Sainte Marie Grand Lebrun', 166, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564260914414194, 44.8303432322149, N'Ecole Elémentaire André Meunier', 170, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.600037341138788, 44.8223444562493, N'Ecole Elémentaire Loucheur', 171, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579259471243062, 44.859383664244, N'Ecole Elémentaire Condorcet', 4, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560901355779213, 44.8240793037428, N'Ecole Maternelle Fieffé', 7, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585095077259788, 44.8598757457137, N'Ecole Maternelle Pierre Trébot', 8, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.547860264617536, 44.8716693120422, N'Ecole Elémentaire Achard', 9, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.543280631977446, 44.8501460501298, N'Ecole Elémentaire Thiers', 14, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572096310579154, 44.8181703464995, N'Ecole Elémentaire Somme', 15, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574358572983837, 44.8361945224202, N'Ecole Maternelle Paul Bert', 16, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.547715592422604, 44.8718169868364, N'Ecole Maternelle Achard', 19, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607776436230809, 44.8391213922746, N'Ecole Elémentaire Bel Air', 27, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545140137651899, 44.8457506335697, N'Ecole Maternelle Benauge', 29, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551671264693196, 44.8819201587296, N'Ecole Elémentaire Labarde', 30, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.612164395976106, 44.8237697168904, N'Ecole maternelle et élémentaire Sainte Thérèse', 31, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60528320263513, 44.8386175543527, N'Ecole Maternelle Bernard Adour', 32, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60621525470459, 44.8430601295443, N'Ecole Elémentaire Jules Ferry', 33, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552906150475585, 44.8746138800864, N'Ecole Elémentaire Charles Martin', 34, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571084424383803, 44.8416165572332, N'Ecole Elémentaire Vieux Bordeaux', 35, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597233845508405, 44.8287639037142, N'Ecole Elémentaire Albert Thomas', 37, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59690192289528, 44.8256747331573, N'Ecole Maternelle Béchade', 38, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561415451039343, 44.860543394683, N'Ecole Elémentaire Dupaty', 39, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.605574618244184, 44.8426532126679, N'Ecole Maternelle Jules Ferry', 40, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549418601934002, 44.8801574975953, N'Ecole Maternelle Le Point du Jour', 41, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580123456243713, 44.8401839562396, N'Ecole maternelle et élémentaire Notre Dame', 42, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.625245894819354, 44.8592149156134, N'Ecole Maternelle Pins Francs', 43, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577306705403472, 44.8522022943475, N'Ecole Maternelle Montgolfier', 44, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544057925089921, 44.8307042514915, N'Ecole Elémentaire Franc Sanson', 45, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.62022064967946, 44.8444498073337, N'Ecole Maternelle Raymond Poincaré', 46, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569182164427155, 44.8729006785022, N'Ecole Maternelle Lac II', 47, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562261648480828, 44.8599909357878, N'Ecole Maternelle Joséphine', 49, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.600606287875099, 44.8483173178771, N'Ecole Maternelle Jean Cocteau', 50, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570046517000919, 44.8242819635529, N'Ecole Maternelle Yser', 51, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564254230856258, 44.8302083325184, N'Ecole Maternelle Noviciat', 52, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555622429424423, 44.8160804246703, N'Ecole Elémentaire Carle Vernet', 53, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582235895170833, 44.8240152970315, N'Ecole maternelle et élémentaire Albert Legrand', 54, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58224232014394, 44.8387241691881, N'Ecole Elémentaire Anatole France', 56, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571477803733305, 44.8187081719301, N'Ecole Maternelle Paul Antin', 57, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585016158026839, 44.8235534614247, N'Ecole Maternelle Solferino', 58, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.616941267864067, 44.8569689866746, N'Ecole Elémentaire Paul Doumer', 59, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571882012132239, 44.8383098636573, N'Ecole Maternelle Pas Saint Georges', 60, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575008430442008, 44.8365383421238, N'Ecole Elémentaire Paul Bert', 61, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552019260708818, 44.8425445464764, N'Ecole maternelle et élémentaire Sainte Marie La Bastide', 62, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554027182834601, 44.860908324529, N'Ecole Maternelle Lucien Faure', 63, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578290500465746, 44.852384609552, N'Ecole Elémentaire Montgolfier', 64, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.542537377455785, 44.8502185793858, N'Ecole Maternelle Thiers', 67, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.616647869016392, 44.8574267073722, N'Ecole Maternelle Paul Doumer', 68, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.602373883757068, 44.8482273929346, N'Ecole Elémentaire Jean Cocteau', 69, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596563150836882, 44.8287629109557, N'Ecole Maternelle Albert Thomas', 71, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562645644727438, 44.8281326229709, N'Ecole Maternelle Barbey', 73, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544058062818567, 44.8314786274256, N'Ecole Maternelle Franc Sanson', 74, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571818295761244, 44.8805871966826, N'Ecole Elémentaire Vaclav Havel', 76, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.624700275971241, 44.8554379348027, N'Ecole Elémentaire Stéhélin', 77, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576623166806662, 44.8363232565712, N'Ecole maternelle et élémentaire Sévigné', 78, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561234013451891, 44.8598279039148, N'Ecole maternelle Bassins à flot', 1066, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561166192143837, 44.8598728927621, N'Ecole élémentaire Bassins à flot', 1078, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Ecoles maternelles et primaires'))

