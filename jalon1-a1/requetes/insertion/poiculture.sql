DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Etablissements culturels')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Etablissements culturels'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication) VALUES(N'Etablissements culturels', N'theater.png', '01/01/2017')

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57996033094378, 44.838898803286, N'Musée des Arts Décoratifs', 596, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569788222488411, 44.8410008174732, N'Musée national des Douanes', 598, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572176541509934, 44.8485675572329, N'Entrepôt Lainé - CAPC Musée d''art contemporain', 599, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558433976876994, 44.8703967587519, N'Base sous-marine', 602, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581915029222318, 44.8387594432029, N'Galerie des Beaux-Arts', 603, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577659972638739, 44.8387046699165, N'Centre Jean Moulin', 606, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601916530460525, 44.8204597507053, N'Relais-lecture Lire à Tauzin ', 607, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554443847215953, 44.8616542775781, N'FRAC d''aquitaine', 608, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581121228129668, 44.8380681162253, N'Musée des Beaux Arts', 609, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574875113282499, 44.8358933739582, N'Musée Goupil', 610, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.550625445847239, 44.8213797840793, N'Bibliothèque Flora Tristan', 611, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574699622324933, 44.8356726794755, N'Musée d''Aquitaine', 612, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58014188430362, 44.8483849727316, N'Muséum d''Histoire naturelle - Hôtel de Lisleferme', 613, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599436738123866, 44.8525071197929, N'Bibliothèque mobile - Parc bordelais', 614, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566984052726453, 44.830905196305, N'Bibliothèque Capucins - St Michel', 615, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572126362383552, 44.8203036713662, N'Bibliothèque mobile - Place Nansouty', 617, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5741436730258, 44.8517946118373, N'Bibliothèque mobile - Place Paul Doumer', 618, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572946079890182, 44.8737245426519, N'Bibliothèque de Bordeaux Lac', 619, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580188512277851, 44.8480506307819, N'Bibliothèque du Jardin Public', 620, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607625311199981, 44.8406762867447, N'Bibliothèque mobile - Avenue Bel Air', 621, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5673465263622, 44.8629609175212, N'Bibliothèque mobile - Cours Saint Louis', 622, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.623131734815062, 44.8579358015084, N'Bibliothèque mobile - Marché Stéhelin', 624, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.542171497551466, 44.8432943383881, N'Bibliothèque de la Bastide', 625, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574198848341553, 44.843672274326, N'Office du Tourisme de Bordeaux', 626, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.546221452408897, 44.8718182045595, N'Bibliothèque de Bacalan', 627, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583974556940011, 44.8578126490484, N'Bibliothèque du Grand Parc', 628, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586807488102576, 44.8353582087954, N'Bibliothèque Centrale de Mériadeck', 629, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556916871544911, 44.8251245646387, N'Office de Tourisme - Gare St Jean', 630, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561023045686356, 44.8415703185136, N'Bibliothèque mobile - Rue Serr', 631, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569460646547184, 44.8545427361766, N'Musée du vin et du négoce de Bordeaux', 632, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576423417954933, 44.8420855000529, N'Comité départemental de tourisme de la Gironde', 633, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579478836738451, 44.8490500142768, N'Muséum d''Histoire naturelle - Pavillon administratif', 634, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.614608147580614, 44.8511215141936, N'Bibliothèque mobile - Place des Martyrs de la Resistance', 635, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570846672445185, 44.8498255992353, N'Comité régional du tourisme Aquitaine', 636, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61137734911348, 44.8328114239498, N'Bibliothèque Jean de La Ville de Mirmont', 1061, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements culturels'))

