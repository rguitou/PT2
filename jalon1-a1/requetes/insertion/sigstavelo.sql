DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Mobiliers urbains : Stationnement deux-roues')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Mobiliers urbains : Stationnement deux-roues'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication) VALUES(N'Mobiliers urbains : Stationnement deux-roues', N'parking_bicycle-2.png', '01/01/2017')

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561129742905569, 44.8633636144148, N'Arceau vélo', 19379, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572445361892433, 44.8195933324231, N'Arceau vélo', 19336, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562167249330207, 44.8637102098026, N'Arceau vélo', 19064, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58002625504963, 44.8363525378076, N'Arceau vélo', 19378, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562857840512229, 44.8334860188402, N'Rack', 19324, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544854739902213, 44.842362345504, N'Rack', 8185, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544691281726248, 44.8428849407113, N'Arceau vélo', 8186, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569217919404499, 44.8308271350071, N'Arceau vélo', 19049, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552665101148584, 44.8422042707425, N'Arceau vélo', 19373, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599484261467213, 44.840167627521, N'Arceau vélo', 19043, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567131984393656, 44.8384595553843, N'Arceau vélo', 19042, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567029211117205, 44.8361536634007, N'Arceau vélo', 19141, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57032026930916, 44.8529118378501, N'Arceau vélo', 19372, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566477474235884, 44.8332782152937, N'Arceau vélo', 20668, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569914368052655, 44.8249343875405, N'Arceau vélo', 19065, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571489845356743, 44.8553779312237, N'Arceau vélo', 19070, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562277108186236, 44.8428193614373, N'Arceau vélo', 19075, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563076434714253, 44.8423323178236, N'Arceau vélo', 19076, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563483458740996, 44.843721396165, N'Arceau vélo', 19078, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562902380399984, 44.8435714108901, N'Arceau vélo', 19080, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569676770652856, 44.8351866783793, N'Arceau vélo', 19058, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574671913098879, 44.843714157714, N'Arceau vélo', 19088, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583370351871903, 44.8415188090726, N'Arceau vélo', 19091, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575082470139099, 44.8354121445664, N'Arceau vélo', 19093, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573405267274533, 44.8317462173047, N'Arceau vélo', 19095, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574328908291777, 44.8334158533909, N'Arceau vélo', 19097, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572025513325496, 44.8544892364387, N'Arceau vélo', 19099, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606971287696474, 44.8395794410116, N'Arceau vélo', 19102, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.600581482735468, 44.8401616513658, N'Arceau vélo', 19106, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.600559060371063, 44.8400824583345, N'Arceau vélo', 19107, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599484288405619, 44.8402551860661, N'Arceau vélo', 19110, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578111446438516, 44.8330327588081, N'Arceau vélo', 19113, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586675617124043, 44.8340116378268, N'Arceau vélo', 19114, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571116310814067, 44.8519580484139, N'Arceau vélo', 19115, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572070353630364, 44.8305516007863, N'Arceau vélo', 19121, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58525153548354, 44.8498729342879, N'Rack', 19125, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559829410213724, 44.8435448890364, N'Arceau vélo', 19126, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5854888100212, 44.8342929820793, N'Arceau vélo', 19105, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585481500244405, 44.8345864262586, N'Arceau vélo', 19138, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588969019990771, 44.8437960037482, N'Arceau vélo', 19142, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568466839311269, 44.8308614138189, N'Arceau vélo', 19145, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57657389520771, 44.8396530204599, N'Arceau vélo', 19146, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.590181961357266, 44.8471651547115, N'Arceau vélo', 19148, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560707475832803, 44.8437863069612, N'Arceau vélo', 19153, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544135074475109, 44.8496657477578, N'Arceau vélo', 19182, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58207551116193, 44.8382819842114, N'Arceau vélo', 19157, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575247307432428, 44.835714139966, N'Arceau vélo', 19158, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.595632822666131, 44.8506738871518, N'Arceau moto', 19002, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58864521939105, 44.8312802471623, N'Rack', 19136, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571420667248857, 44.8557450163844, N'Arceau vélo', 19170, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.600902610251577, 44.8400569947922, N'Arceau vélo', 19174, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586695545372073, 44.8340999197049, N'Arceau vélo', 19180, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.543789617729951, 44.8503405657418, N'Arceau vélo', 19183, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571183431995306, 44.8518907243969, N'Arceau vélo', 19187, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571506584571569, 44.8324353147968, N'Rack', 19190, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573501963481954, 44.8312295413464, N'Arceau vélo', 19199, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561839383848331, 44.8431528458826, N'Arceau vélo', 19209, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575298053009284, 44.8253448093585, N'Arceau vélo', 19227, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.608426252163318, 44.8395525964656, N'Arceau vélo', 19166, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57736345780043, 44.8449522332733, N'Arceau vélo', 19167, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562925240736441, 44.8434640406428, N'Arceau vélo', 19213, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585646593034646, 44.8344136730348, N'Arceau vélo', 19222, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586525532807778, 44.8443531697276, N'Arceau vélo', 19228, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577987351388128, 44.824650323845, N'Arceau vélo', 19229, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586829758832818, 44.8495253999658, N'Arceau vélo', 19247, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564920409086134, 44.8316503506117, N'Arceau vélo', 19201, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582019764485353, 44.8794532782428, N'Arceau vélo', 19380, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567042974664563, 44.9056388285475, N'Arceau vélo', 19397, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579209074707895, 44.9022543547034, N'Autre', 19402, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572628674565288, 44.8494831047643, N'Arceau vélo', 19413, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569056051990607, 44.8721595896671, N'Arceau vélo', 19381, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569918467742439, 44.8991513247541, N'Arceau vélo', 19386, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.543710459292935, 44.8460203264945, N'Arceau vélo', 19809, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560448561404315, 44.8573800391778, N'Arceau vélo', 19765, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570474235115345, 44.8563451849438, N'Arceau vélo', 19764, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561640127400679, 44.8569000208547, N'Arceau vélo', 19766, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554489625142218, 44.8413122604709, N'Arceau vélo', 19812, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552824461076425, 44.8416780063396, N'Arceau vélo', 19813, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559546371157345, 44.8442652817445, N'Arceau vélo', 19814, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568531421596405, 44.8521438844152, N'Arceau vélo', 19777, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562885920083538, 44.8562384930203, N'Arceau vélo', 19782, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55188267103545, 44.8449071124018, N'Arceau vélo', 19793, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552914380346125, 44.8442690341616, N'Arceau vélo', 19794, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554315101720564, 44.8434146707704, N'Arceau vélo', 19795, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554841814233495, 44.843572522754, N'Arceau vélo', 19796, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554990801492735, 44.8430015391888, N'Arceau vélo', 19797, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554732858921609, 44.8431611102983, N'Arceau vélo', 19798, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556465040434855, 44.8421002612685, N'Arceau vélo', 19799, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.543727069792037, 44.8461009703471, N'Arceau vélo', 19808, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556868889780603, 44.8423332863781, N'Arceau vélo', 19800, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556845280255986, 44.8418566026248, N'Arceau vélo', 19801, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557028261458011, 44.8422302083714, N'Arceau vélo', 19802, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557066576045528, 44.8417250155491, N'Arceau vélo', 19803, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557618953321362, 44.8413780568636, N'Arceau vélo', 19805, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551377238962218, 44.8456941445416, N'Arceau vélo', 19791, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570779788680954, 44.8438933687611, N'Arceau vélo', 19786, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570559462437289, 44.8453434567251, N'Arceau vélo', 19788, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551612622547507, 44.8450759715445, N'Arceau vélo', 19792, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.540382286514983, 44.8519560535181, N'Arceau vélo', 19789, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549079861454436, 44.8471021357229, N'Arceau vélo', 19790, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552771050141765, 44.8541864635081, N'Arceau vélo', 19811, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549538940089295, 44.8430656936488, N'Arceau vélo', 19820, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558442740510532, 44.8452383352106, N'Arceau vélo', 19816, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.540266232697602, 44.8472456665472, N'Arceau vélo', 19817, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549618338233704, 44.8431357308966, N'Arceau vélo', 19821, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552012387343829, 44.8431750725694, N'Arceau vélo', 19819, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61022664340578, 44.8322334280877, N'Arceau vélo', 19834, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609682724382817, 44.832238341878, N'Arceau vélo', 19833, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545792297539433, 44.8746742302079, N'Arceau vélo', 19828, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609254124513587, 44.8320269113258, N'Arceau vélo', 19835, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610041678854051, 44.8323282073273, N'Arceau vélo', 19832, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610195537422605, 44.8323693582148, N'Arceau vélo', 19831, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572037304792114, 44.8391433347649, N'Arceau vélo', 19857, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572410150735624, 44.8499573904331, N'Arceau vélo', 19862, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565965038228344, 44.8277071460049, N'Arceau vélo', 19861, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566729844121131, 44.8280752007215, N'Arceau vélo', 19860, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562749530219562, 44.8562959090706, N'Arceau vélo', 19870, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565895942651492, 44.8542617480633, N'Arceau vélo', 19871, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561640127400679, 44.8569000208547, N'Arceau vélo', 19872, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579003523138751, 44.8430236568866, N'Arceau moto', 20669, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570692879583025, 44.843181759491, N'Arceau vélo', 19038, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571354215332096, 44.8368553862223, N'Arceau vélo', 19044, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579458459555907, 44.8422881624592, N'Arceau vélo', 19045, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578919948493607, 44.8431661804951, N'Arceau vélo', 19046, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559881383046359, 44.8298047469894, N'Arceau vélo', 19048, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569449752955714, 44.8359667788321, N'Arceau vélo', 19050, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572049036249554, 44.8355592994931, N'Arceau vélo', 19051, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578826414002433, 44.8433216285339, N'Arceau vélo', 19052, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588915385071704, 44.8256249797921, N'Arceau vélo', 19053, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578100063946085, 44.8444924555635, N'Arceau vélo', 19054, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579980310361911, 44.8446342028248, N'Arceau vélo', 19055, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562170929650825, 44.8333670914479, N'Arceau vélo', 19056, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571942219048298, 44.8482131704815, N'Arceau vélo', 19057, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570627800031253, 44.8352258400976, N'Arceau vélo', 19059, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581024531218452, 44.8371431124905, N'Arceau vélo', 19060, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597195449373703, 44.8287558561398, N'Arceau vélo', 19061, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575135787663082, 44.849276464047, N'Arceau vélo', 19062, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57718139238049, 44.8410399026827, N'Arceau vélo', 19066, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61983944154991, 44.8526898407134, N'Arceau vélo', 19067, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576249506861617, 44.826890235144, N'Arceau vélo', 19068, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575025849880869, 44.8446736561635, N'Arceau moto', 19006, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573924353820559, 44.8305146369305, N'Arceau vélo', 19071, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575221233904843, 44.8430071554463, N'Arceau moto', 19003, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5724735761491, 44.8252564407543, N'Arceau vélo', 19073, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579242343428061, 44.8426388879857, N'Arceau vélo', 19074, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581425933622876, 44.8431659824293, N'Arceau vélo', 19079, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581464702923744, 44.8434351847471, N'Arceau vélo', 19081, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572468959500377, 44.8200699282876, N'Arceau vélo', 19082, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573826747744413, 44.8420428672511, N'Arceau vélo', 19083, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577158248369624, 44.8443901854901, N'Arceau vélo', 19084, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57754461917663, 44.8371497930756, N'Arceau vélo', 19085, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57153777284114, 44.8369858264185, N'Arceau vélo', 19087, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572379356244029, 44.8350737552327, N'Arceau vélo', 19089, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576111792235352, 44.8444705462189, N'Arceau vélo', 19224, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56559869598041, 44.8361445406457, N'Arceau vélo', 19225, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569995834073082, 44.8377899869423, N'Arceau vélo', 19226, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555427791640871, 44.816238381009, N'Arceau vélo', 19232, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579152339781455, 44.8422418507377, N'Arceau vélo', 19233, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561848047925136, 44.8301696336244, N'Arceau vélo', 19234, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567450209956096, 44.8357108727808, N'Arceau vélo', 19236, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568676827455618, 44.8354369392479, N'Arceau vélo', 19237, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569413837231184, 44.8354994881941, N'Arceau vélo', 19238, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569200448273951, 44.8352797201983, N'Arceau vélo', 19239, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572607089530809, 44.8481450095004, N'Arceau vélo', 19240, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57204099805107, 44.8425289778156, N'Arceau vélo', 19243, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570149955134352, 44.8350307668493, N'Arceau vélo', 19244, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569510302103284, 44.8341284131074, N'Arceau vélo', 19245, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576958667478795, 44.8383441261791, N'Arceau vélo', 19249, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575961962126055, 44.8371356740983, N'Arceau vélo', 19250, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580383198556721, 44.8367180247173, N'Arceau vélo', 19252, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57361590118576, 44.8411116617114, N'Arceau vélo', 19255, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570914588080276, 44.8348854346522, N'Arceau vélo', 19256, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597928112295216, 44.827719719402, N'Arceau vélo', 19258, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597455470295766, 44.8283891093295, N'Arceau vélo', 19259, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597573773570096, 44.828224032118, N'Arceau vélo', 19260, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572619582783847, 44.8348156091154, N'Arceau vélo', 19263, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556870743975938, 44.8264944337706, N'Arceau vélo', 19265, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576999078474051, 44.8462760697127, N'Arceau vélo', 19267, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576764565035174, 44.8466511488985, N'Arceau vélo', 19269, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.615458603637434, 44.8524235144946, N'Arceau vélo', 19271, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574997938334069, 44.8394072585822, N'Arceau vélo', 19273, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572643968492027, 44.8375973370199, N'Arceau vélo', 19274, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573348140311268, 44.8339418313527, N'Arceau vélo', 19275, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.591830093701595, 44.8333041372003, N'Arceau vélo', 19276, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.592116643563164, 44.8332158193738, N'Arceau vélo', 19277, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555360298207616, 44.8597044058801, N'Arceau vélo', 19278, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57198102803654, 44.836980020106, N'Arceau vélo', 19286, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596899737001278, 44.8301771457066, N'Arceau vélo', 19287, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560917938814607, 44.8609790786143, N'Arceau vélo', 19288, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574804276887849, 44.8349766978903, N'Arceau vélo', 19290, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5998867691983, 44.8291828456931, N'Arceau vélo', 19291, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587681316450362, 44.841450179129, N'Arceau vélo', 19297, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573445026604611, 44.8407287811661, N'Arceau vélo', 19300, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575532651979126, 44.8412254882841, N'Arceau vélo', 19303, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.600328683548976, 44.8291625563655, N'Arceau vélo', 19307, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581159355897244, 44.840715166899, N'Arceau vélo', 19310, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57148591275021, 44.8481487605856, N'Arceau moto', 19134, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57604788923313, 44.8276697350973, N'Arceau vélo', 19312, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574288822401634, 44.8223202667336, N'Arceau vélo', 19314, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573272105760257, 44.8222287967978, N'Arceau vélo', 19315, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576431962732209, 44.8275069571383, N'Arceau vélo', 19316, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571675920746192, 44.8408170204033, N'Arceau moto', 20675, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580959111670528, 44.8256729226687, N'Arceau vélo', 19320, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58093123392667, 44.8405588491117, N'Arceau vélo', 19321, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581328537189259, 44.8249612747276, N'Arceau vélo', 19322, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56990731783705, 44.8362614896404, N'Arceau vélo', 19323, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58072138109997, 44.8407269673957, N'Arceau vélo', 19327, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.600497042229497, 44.8287351049099, N'Arceau vélo', 19332, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570408139609682, 44.8358977057105, N'Arceau vélo', 19335, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573589505897508, 44.8517825963671, N'Arceau vélo', 19337, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576389514388047, 44.8472549442685, N'Arceau vélo', 19339, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573542511581351, 44.8518558658342, N'Arceau vélo', 19340, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575347168334868, 44.8430940901276, N'Arceau vélo', 19341, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572350186137834, 44.8390724382797, N'Arceau vélo', 19344, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578179492815783, 44.8422754168377, N'Arceau vélo', 19348, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596554231862895, 44.8326261276217, N'Arceau vélo', 19350, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575514940144481, 44.8363301687508, N'Arceau vélo', 19352, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574032125592034, 44.840065637823, N'Arceau vélo', 19353, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574040519632805, 44.8404886621097, N'Arceau vélo', 19358, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571883600497084, 44.8404317141152, N'Arceau vélo', 19359, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577081379657935, 44.8319118444671, N'Arceau vélo', 19363, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579174656419916, 44.860223209435, N'Arceau vélo', 19364, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.602930318060688, 44.8298168076139, N'Arceau vélo', 19366, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601565332544064, 44.8273302486402, N'Arceau vélo', 19367, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61101521261014, 44.8323124523007, N'Arceau vélo', 19374, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5998913879616, 44.8287685033032, N'Arceau vélo', 19376, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573944583428938, 44.8418687344523, N'Arceau vélo', 19382, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57089104341503, 44.8377224423831, N'Arceau vélo', 19383, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5788954876267, 44.8421672348315, N'Arceau vélo', 19384, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572010075796873, 44.8324807639004, N'Arceau vélo', 19388, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572054343460944, 44.832353531805, N'Arceau vélo', 19389, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57798093560506, 44.8319341850428, N'Arceau vélo', 19390, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577827537762875, 44.8319020757503, N'Arceau vélo', 19391, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579729512989849, 44.8332497633218, N'Arceau vélo', 19396, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573296285410891, 44.8354469025942, N'Arceau vélo', 19398, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574434662972772, 44.8336533163921, N'Arceau vélo', 19399, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573691494875811, 44.8321952867919, N'Arceau vélo', 19400, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574098683969099, 44.8330044813133, N'Arceau vélo', 19401, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568331387576356, 44.8379129071874, N'Arceau vélo', 19403, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562066248757084, 44.8332976741613, N'Arceau vélo', 19404, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.589361821557819, 44.8381030062597, N'Arceau vélo', 19405, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574219035532843, 44.8514606095802, N'Arceau vélo', 19407, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574069836234762, 44.8510051646153, N'Arceau vélo', 19408, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571915783014767, 44.8359318212497, N'Arceau vélo', 19410, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56471183367852, 44.8353563828415, N'Arceau vélo', 19412, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598269549310523, 44.8331524699722, N'Arceau vélo', 19414, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572670175714868, 44.8373625754171, N'Arceau vélo', 19415, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.593788178881454, 44.8327322659111, N'Arceau vélo', 19416, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581371024570626, 44.8298766444038, N'Arceau vélo', 19417, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572958378532432, 44.8378145577736, N'Arceau vélo', 19418, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581718437357947, 44.8302461197428, N'Arceau vélo', 19421, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.604486090243181, 44.8303085691438, N'Arceau vélo', 19424, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575760679676569, 44.8409766373617, N'Arceau vélo', 19426, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568402879698395, 44.837568901064, N'Arceau vélo', 19428, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607674815048356, 44.8298042187336, N'Arceau vélo', 19430, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.62520235617348, 44.8558380043858, N'Arceau vélo', 19433, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579088301876599, 44.8428984808963, N'Arceau vélo', 19435, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570614693056526, 44.8387829817504, N'Arceau vélo', 19437, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567722909167398, 44.8378921712808, N'Arceau vélo', 19439, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578625573381844, 44.8408144245362, N'Arceau vélo', 19443, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575915762270054, 44.8395139911311, N'Arceau vélo', 19445, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581256998213743, 44.8385059254206, N'Arceau vélo', 19446, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579986852411512, 44.8404469562775, N'Arceau vélo', 19447, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572690632532647, 44.8390458958138, N'Arceau vélo', 19448, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573081513372158, 44.839018038932, N'Arceau vélo', 19449, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573570502054625, 44.8389246740009, N'Arceau vélo', 19450, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58552249452397, 44.8268543746097, N'Arceau vélo', 19451, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578619564074073, 44.8436606209484, N'Arceau vélo', 19452, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578905219971268, 44.8438869284088, N'Arceau vélo', 19453, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578686324622673, 44.8435592527253, N'Arceau vélo', 19454, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578566448564863, 44.843753362712, N'Arceau vélo', 19455, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578764265121109, 44.8441065637406, N'Arceau vélo', 19456, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572643391140075, 44.8396233528956, N'Arceau vélo', 19458, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573185905668866, 44.8406091838681, N'Arceau vélo', 19459, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598627030685067, 44.8541304478277, N'Arceau vélo', 19460, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610337644024212, 44.8289079410427, N'Arceau vélo', 19462, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609806032624884, 44.8290643446409, N'Arceau vélo', 19463, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577594118989531, 44.8320430051856, N'Arceau vélo', 19464, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610794015949097, 44.8316877240005, N'Arceau vélo', 19465, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59963930151961, 44.8524389567792, N'Arceau vélo', 19466, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583437813221533, 44.8398555932288, N'Arceau vélo', 19468, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578717710273409, 44.8441887399794, N'Arceau vélo', 19470, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578393199452601, 44.8440258994879, N'Arceau vélo', 19471, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601409685142006, 44.8497594169118, N'Arceau vélo', 19474, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578322781408545, 44.8441356699183, N'Arceau vélo', 19477, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610926759765377, 44.8320625782175, N'Arceau vélo', 19478, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567137728357328, 44.8327292875849, N'Arceau vélo', 19479, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569760837539418, 44.8399390168772, N'Arceau vélo', 19480, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.614514210342271, 44.8521684853448, N'Arceau vélo', 19482, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578905219971268, 44.8438869284088, N'Arceau vélo', 19485, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610873182118616, 44.8330094219944, N'Arceau vélo', 19488, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598233205622128, 44.8272438072572, N'Arceau vélo', 19490, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598233205622128, 44.8272438072572, N'Arceau vélo', 19493, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5763907281808, 44.8332889089338, N'Arceau vélo', 19495, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587036243463388, 44.8391793927146, N'Arceau vélo', 19497, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569809721758721, 44.8381188535419, N'Arceau vélo', 19500, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60389156231526, 44.8434679220396, N'Arceau vélo', 19502, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.619856138919234, 44.8437478039376, N'Arceau vélo', 19504, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582806079779344, 44.8212565603228, N'Arceau vélo', 19505, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581562976998425, 44.836012946835, N'Arceau vélo', 19507, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571805850586954, 44.830969230183, N'Arceau vélo', 19392, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57792208579466, 44.8315124761673, N'Arceau vélo', 19393, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572224391333772, 44.8324663786081, N'Arceau vélo', 19394, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577191079426164, 44.8320801793977, N'Arceau vélo', 19509, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574434662972772, 44.8336533163921, N'Arceau vélo', 19510, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574956997413882, 44.8352609681293, N'Arceau vélo', 19511, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584818139255514, 44.8381999383218, N'Arceau vélo', 19514, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571791829698018, 44.8390415024598, N'Arceau vélo', 19517, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579052889910255, 44.8380430753846, N'Arceau vélo', 19519, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583437813221533, 44.8398555932288, N'Arceau vélo', 19521, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576597781351598, 44.8387224581176, N'Arceau vélo', 19528, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574734208693159, 44.8389134352136, N'Arceau vélo', 19529, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576912940553229, 44.8386965175411, N'Arceau vélo', 19530, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575827044465992, 44.8481605410447, N'Arceau vélo', 19538, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57293814531793, 44.8423996535766, N'Arceau vélo', 19539, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571878695493223, 44.8474134500493, N'Arceau vélo', 19541, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576588820309418, 44.8469347505377, N'Arceau vélo', 19542, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580370025218292, 44.8415416328888, N'Arceau vélo', 19546, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580172407995872, 44.8575520243081, N'Arceau vélo', 19550, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574709714041616, 44.8430664631192, N'Arceau vélo', 19561, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574224697561762, 44.8442813881928, N'Arceau vélo', 19562, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573459491644423, 44.8509484814694, N'Arceau vélo', 19566, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572802074678065, 44.8527479347068, N'Arceau vélo', 19567, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572448828864921, 44.8537923325158, N'Arceau vélo', 19568, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573391612047597, 44.8640966464834, N'Arceau vélo', 19569, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577524873559771, 44.8586169602163, N'Arceau vélo', 19570, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581391237541876, 44.8589118236961, N'Arceau vélo', 19571, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58177151626408, 44.8500777205314, N'Arceau vélo', 19572, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573257377710843, 44.8402652647162, N'Arceau vélo', 19577, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584850762028577, 44.8428094341225, N'Arceau vélo', 19578, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585388180099483, 44.8421655399139, N'Arceau vélo', 19579, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58492589636428, 44.8420330918272, N'Arceau vélo', 19580, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574526936578752, 44.854595463426, N'Arceau vélo', 19583, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575144445209686, 44.8499606227855, N'Arceau vélo', 19589, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560050495021577, 44.8577772192269, N'Arceau vélo', 19596, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571104316593623, 44.8415080394817, N'Arceau vélo', 19597, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544279792699888, 44.8776472515817, N'Arceau vélo', 19600, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588131370946138, 44.8382499099627, N'Arceau vélo', 19604, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.590343130915271, 44.8376639797585, N'Arceau vélo', 19607, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567430320364557, 44.8387589073463, N'Arceau vélo', 19613, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560302448481845, 44.8326999797065, N'Arceau vélo', 19624, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559809980726287, 44.8319353873254, N'Arceau vélo', 19626, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558992474283083, 44.8310503452366, N'Arceau vélo', 19628, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568821440463788, 44.822286715856, N'Arceau vélo', 19633, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562627339211314, 44.8305732945013, N'Arceau vélo', 19634, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562828580501566, 44.8305502380518, N'Arceau vélo', 19635, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561920797573514, 44.8307054936613, N'Arceau vélo', 19636, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.623928977637614, 44.8421938064145, N'Arceau vélo', 19639, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601021609115293, 44.8269324501471, N'Arceau vélo', 19640, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597432212862271, 44.832468743681, N'Arceau vélo', 19641, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.592181269421536, 44.8274242454563, N'Arceau vélo', 19642, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.592597032067136, 44.8273867051373, N'Arceau vélo', 19643, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59877733671823, 44.8312820775626, N'Arceau vélo', 19644, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575135787663082, 44.849276464047, N'Arceau vélo', 19645, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577579705947741, 44.8503135136737, N'Arceau vélo', 19646, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579885533133086, 44.8506065574766, N'Arceau vélo', 19647, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.615854463899339, 44.8513482394801, N'Arceau vélo', 19656, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588886271938319, 44.834666274369, N'Arceau vélo', 19657, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.590965036872458, 44.8296701753615, N'Arceau vélo', 19659, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585735876191249, 44.8354393149055, N'Arceau vélo', 19661, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579911041398329, 44.8595022902089, N'Arceau vélo', 19663, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594682485680841, 44.8389678318757, N'Arceau vélo', 19664, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601821770446646, 44.8276478677686, N'Arceau vélo', 19668, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.625710167238734, 44.8426254166912, N'Arceau vélo', 19670, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574500673418693, 44.8410893681964, N'Arceau vélo', 19675, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582286418428242, 44.8406230316671, N'Arceau vélo', 19677, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.622324562740253, 44.857776935128, N'Arceau vélo', 19681, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.620374578533048, 44.856941832892, N'Arceau vélo', 19682, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574204670307522, 44.8552429013559, N'Arceau vélo', 19685, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58463163827675, 44.849577357233, N'Arceau vélo', 19686, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594282590502299, 44.8524667251161, N'Arceau vélo', 19688, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596274070625063, 44.8497689375178, N'Arceau vélo', 19691, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54963562908394, 44.8178148807999, N'Arceau vélo', 19692, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556991447732861, 44.8302192398011, N'Arceau vélo', 19694, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566834575614152, 44.8307300937232, N'Arceau vélo', 19695, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562109512860032, 44.8566631065427, N'Arceau vélo', 19696, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561297899809595, 44.8418081464981, N'Arceau vélo', 19697, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.547445842364775, 44.848106296233, N'Arceau vélo', 19698, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.540087302474718, 44.8521343963466, N'Arceau vélo', 19700, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557659929629076, 44.8439972813353, N'Arceau vélo', 19701, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544549236890312, 44.8769472415827, N'Arceau vélo', 19702, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588526387951173, 44.8373923117498, N'Arceau vélo', 19705, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.591153816601049, 44.8407860178016, N'Arceau vélo', 19706, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587601197955057, 44.8421275924444, N'Arceau vélo', 19709, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588882678434031, 44.83889858254, N'Arceau vélo', 19711, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.591098497309315, 44.8298109038857, N'Arceau vélo', 19713, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601969373841797, 44.8328578572659, N'Arceau vélo', 19714, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586745225943792, 44.835125638686, N'Arceau vélo', 19715, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598033927310215, 44.8313459460373, N'Arceau vélo', 19717, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586745225943792, 44.835125638686, N'Arceau vélo', 19718, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586215073657299, 44.8427568995499, N'Arceau vélo', 19720, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587598492875507, 44.8373372548352, N'Arceau vélo', 19722, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586745225943792, 44.835125638686, N'Arceau vélo', 19725, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596031339679826, 44.8300190281353, N'Arceau vélo', 19727, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568132159679324, 44.8309496494166, N'Arceau vélo', 19728, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568399953756924, 44.8302638473207, N'Arceau vélo', 19729, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5673710460672, 44.8300347902111, N'Arceau vélo', 19730, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566976935815063, 44.8306699611488, N'Arceau vélo', 19731, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554513566381577, 44.8251396218458, N'Arceau vélo', 19738, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557222274879929, 44.8246679753649, N'Arceau vélo', 19745, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557357555888283, 44.8266173052068, N'Arceau vélo', 19746, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560639654041604, 44.8323429805847, N'Arceau vélo', 19747, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558012381020705, 44.841143079751, N'Arceau vélo', 19748, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558110813092357, 44.841086584885, N'Arceau vélo', 19749, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55962907712251, 44.8397969937502, N'Arceau vélo', 19750, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558073880904357, 44.8380350303646, N'Arceau vélo', 19751, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564746986372985, 44.8616034766276, N'Arceau vélo', 19753, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570779788680954, 44.8438933687611, N'Arceau vélo', 19755, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571161628202832, 44.847503512935, N'Arceau vélo', 19756, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557117572573696, 44.8588860850572, N'Arceau vélo', 19758, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568330320200082, 44.8539588155738, N'Arceau vélo', 19759, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559297185949641, 44.857904097688, N'Arceau vélo', 19760, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556461292716108, 44.8591816933432, N'Arceau vélo', 19761, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563342163820957, 44.8595676995173, N'Arceau vélo', 19762, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565650462644597, 44.8540890984181, N'Arceau vélo', 19763, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554878340209544, 44.8599415931836, N'Arceau vélo', 19767, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557117572573696, 44.8588860850572, N'Arceau vélo', 19768, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555483628532124, 44.8596383090778, N'Arceau vélo', 19769, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570789744651912, 44.8489355656773, N'Arceau vélo', 19770, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570454393072429, 44.8495743055818, N'Arceau vélo', 19771, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570237029632399, 44.8500390016057, N'Arceau vélo', 19772, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570001952143967, 44.8506572087368, N'Arceau vélo', 19773, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569906531508465, 44.8507766752828, N'Arceau vélo', 19774, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5689656010355, 44.8517097772215, N'Arceau vélo', 19775, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568603711653716, 44.8520700042813, N'Arceau vélo', 19776, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568531421596405, 44.8521438844152, N'Arceau vélo', 19778, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568015440122905, 44.8527151335854, N'Arceau vélo', 19779, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567053675078279, 44.8534866366902, N'Arceau vélo', 19780, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566858937995596, 44.8536446141374, N'Arceau vélo', 19781, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569226767015758, 44.8513610849576, N'Arceau vélo', 19783, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567114184770766, 44.8534310527188, N'Arceau vélo', 19784, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563186618917334, 44.8597516468217, N'Arceau vélo', 19785, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57022067461648, 44.8426538414698, N'Arceau vélo', 19787, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562715823244903, 44.8487150431614, N'Arceau vélo', 19810, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560393289657602, 44.8445823526673, N'Arceau vélo', 19815, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552489379752897, 44.8418124163712, N'Arceau vélo', 19818, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.542748688183026, 44.8439643140953, N'Arceau vélo', 19822, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.542358806982554, 44.8437579193318, N'Arceau vélo', 19823, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557997937922268, 44.8446912266623, N'Arceau vélo', 19824, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.546140718635348, 44.8740352611863, N'Arceau vélo', 19825, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564937611609436, 44.8986990056115, N'Arceau vélo', 19826, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562176785343876, 44.871824815621, N'Arceau vélo', 19827, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545129837964562, 44.8758793010676, N'Arceau vélo', 19829, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.546396266588477, 44.8789632518012, N'Arceau vélo', 19830, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.608798574763961, 44.8320177577833, N'Arceau vélo', 19836, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.608890761698507, 44.8319333097576, N'Arceau vélo', 19837, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60775762129426, 44.8317634031825, N'Arceau vélo', 19838, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607881786659507, 44.8316538901965, N'Arceau vélo', 19839, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606825503560649, 44.8315366680204, N'Arceau vélo', 19840, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606312831939573, 44.831405632471, N'Arceau vélo', 19841, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606907738839893, 44.8314084869267, N'Arceau vélo', 19842, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606408017971805, 44.8312861396223, N'Arceau vélo', 19843, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.605403302270817, 44.8311856469531, N'Arceau vélo', 19844, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.605549484799618, 44.8310739076087, N'Arceau vélo', 19845, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60495998920754, 44.8309268335347, N'Arceau vélo', 19846, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.604301016956029, 44.8309075315011, N'Arceau vélo', 19847, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.604768224952573, 44.830886696441, N'Arceau vélo', 19848, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603660537428704, 44.8307527872856, N'Arceau vélo', 19849, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573523931586443, 44.8232219520762, N'Arceau vélo', 19850, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57318321424445, 44.8222220403832, N'Arceau vélo', 19851, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575215381841796, 44.8231434039247, N'Arceau vélo', 19852, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580054067869483, 44.8321429981622, N'Arceau vélo', 19853, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576933892239145, 44.8327619782875, N'Arceau vélo', 19854, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573925509583106, 44.8402123943764, N'Arceau vélo', 19855, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572350186137834, 44.8390724382797, N'Arceau vélo', 19856, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580932260795949, 44.8382555414376, N'Arceau vélo', 19858, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560073703481028, 44.8325874210496, N'Arceau vélo', 19859, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572086592974199, 44.8505778392945, N'Arceau vélo', 19863, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574323041597626, 44.8522774345515, N'Arceau vélo', 19864, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571222250909044, 44.8518407730354, N'Arceau vélo', 19865, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570067867816228, 44.853228279206, N'Arceau vélo', 19866, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570425556059938, 44.8528166534533, N'Arceau vélo', 19867, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559396074709185, 44.8578565963602, N'Arceau vélo', 19868, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557947023284108, 44.8585051598026, N'Arceau vélo', 19869, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572746164236664, 44.8370322969133, N'Arceau moto', 20390, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558988750105699, 44.8236495029406, N'Arceau vélo', 20677, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584789221208966, 44.8424044771674, N'Arceau moto', 19137, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598071699279046, 44.8331649424144, N'Arceau moto', 19009, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586619802046696, 44.8394107857004, N'Arceau vélo', 20433, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583535954420549, 44.8400594184806, N'Arceau vélo', 20432, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571033636081832, 44.8479042422325, N'Arceau vélo', 20431, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579872831848159, 44.8423405264996, N'Arceau vélo', 20410, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571738492674439, 44.843984531456, N'Arceau vélo', 20434, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572072844923911, 44.8439490849335, N'Arceau vélo', 20435, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58257321067382, 44.8415658872764, N'Arceau vélo', 20590, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583603099346688, 44.841494904936, N'Arceau vélo', 20591, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607858541846745, 44.8509641700626, N'Arceau vélo', 20678, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580728598992721, 44.8427006795338, N'Arceau vélo', 20679, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559998792925311, 44.823274515195, N'Arceau vélo', 20680, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570544365223955, 44.8199221778813, N'Arceau vélo', 20681, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.608385021937342, 44.8511975495824, N'Arceau vélo', 20682, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609286146356139, 44.851427696268, N'Arceau vélo', 20683, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.611079898646403, 44.8516854635998, N'Arceau vélo', 20684, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56963085047744, 44.8514938017266, N'Arceau vélo', 20685, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577375863591592, 44.8360530522232, N'Arceau vélo', 20686, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566373011939915, 44.8341347285394, N'Arceau vélo', 20687, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.608974452850088, 44.8513441637979, N'Arceau vélo', 20688, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57503781068074, 44.8515965887403, N'Arceau vélo', 20689, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576552979605458, 44.8190361040771, N'Arceau vélo', 20690, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566556704917178, 44.833603082987, N'Arceau vélo', 20691, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565942523391278, 44.8346470843583, N'Arceau vélo', 20692, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557432806762682, 44.826042225064, N'Arceau moto', 20693, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55702686974052, 44.8272888300336, N'Arceau moto', 20694, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557112628507604, 44.8260186366469, N'Arceau moto', 20695, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564996808044724, 44.8267782264133, N'Arceau vélo', 20696, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564895579927974, 44.826801306116, N'Arceau vélo', 20697, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557093918740099, 44.8261461440044, N'Arceau moto', 20698, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55763522496877, 44.8255505455326, N'Arceau moto', 20699, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573012929202333, 44.8299943125214, N'Arceau moto', 20700, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573412722954762, 44.8306683501715, N'Arceau moto', 20701, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571372184406497, 44.8309635663102, N'Arceau moto', 20702, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572003945970887, 44.8315753779016, N'Arceau moto', 20703, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583507926636347, 44.8381394158619, N'Arceau moto', 20704, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573268444718241, 44.8434114245694, N'Arceau moto', 19011, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573392828896194, 44.8429570270449, N'Arceau moto', 19004, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570282418017905, 44.8428083737213, N'Arceau moto', 19010, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576497669665437, 44.8382845702338, N'Arceau moto', 19014, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579185475999824, 44.8370634210555, N'Arceau moto', 19874, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580321232282787, 44.8405997525803, N'Arceau moto', 19013, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579471526783301, 44.8422682039324, N'Arceau moto', 19873, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579236188550287, 44.8426539448782, N'Arceau moto', 19109, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579154865957579, 44.8428000911757, N'Arceau moto', 19069, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579107448214957, 44.8428642786477, N'Arceau moto', 19311, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578927559868958, 44.8431460333038, N'Arceau moto', 19177, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578872962407965, 44.8432393623368, N'Arceau moto', 19176, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578627253817711, 44.843641732448, N'Arceau moto', 19111, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578581136914286, 44.8437329930115, N'Arceau moto', 19318, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578498946801071, 44.8438611498791, N'Arceau moto', 19197, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578428402788654, 44.8439709243508, N'Arceau moto', 19181, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578357990464138, 44.8440807847087, N'Arceau moto', 19072, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578181829312415, 44.8443553944272, N'Arceau moto', 19163, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57808884106552, 44.8445115451832, N'Arceau moto', 19184, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598397392162442, 44.8331420310876, N'Arceau moto', 19012, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.590233474993789, 44.8340640084914, N'Arceau moto', 20705, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573689529207945, 44.8552313313837, N'Arceau moto', 19875, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570590427971117, 44.8525121452352, N'Arceau moto', 20706, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594873851379587, 44.8521398839096, N'Arceau moto', 20708, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606095606389158, 44.8396575468036, N'Arceau moto', 20709, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594834127978277, 44.8516110099663, N'Arceau moto', 20710, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571961034476024, 44.8523817623685, N'Arceau moto', 19063, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585050328600127, 44.8503796715077, N'Rack', 19150, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569752210633998, 44.8361406037561, N'Arceau vélo', 19047, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559745267921835, 44.8715146018217, N'Arceau vélo', 20750, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544316877568246, 44.8764778687003, N'Arceau vélo', 20751, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.543783399481521, 44.8750228597754, N'Arceau vélo', 20752, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566434536160793, 44.8881922164455, N'Arceau vélo', 20753, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565885598359107, 44.8922110935964, N'Arceau vélo', 20754, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566015066595153, 44.8932718688912, N'Arceau vélo', 20755, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566112017204597, 44.8944929224718, N'Arceau vélo', 20756, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566791560797444, 44.8557670263631, N'Arceau vélo', 20757, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573010805600949, 44.8350488840047, N'Arceau vélo', 19090, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574178188023394, 44.8348573720226, N'Arceau vélo', 19092, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598575059174256, 44.8310350755608, N'Arceau vélo', 19094, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571987949705112, 44.8376409025775, N'Arceau vélo', 19096, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57708851027629, 44.8343428971911, N'Arceau vélo', 19098, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573344010996926, 44.8358958961162, N'Arceau vélo', 19100, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598408571676001, 44.830994219377, N'Arceau vélo', 19101, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577458652549948, 44.8420324323449, N'Arceau vélo', 19103, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571965458446359, 44.8145177692655, N'Arceau vélo', 19104, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581974953883516, 44.8219374427332, N'Arceau vélo', 19108, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578362845957062, 44.8446134587472, N'Arceau moto', 20670, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582064655774912, 44.8468686472728, N'Arceau moto', 20671, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572124060017679, 44.8205108038816, N'Arceau vélo', 19112, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.6014469293964, 44.8466571763507, N'Arceau vélo', 19116, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.6015065267355, 44.8465835855716, N'Arceau vélo', 19117, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579060900447712, 44.8429465449446, N'Arceau vélo', 19118, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.610094956760666, 44.8326329770223, N'Arceau vélo', 19119, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586013333140561, 44.824266636147, N'Arceau vélo', 19120, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578577191737601, 44.8444174594459, N'Arceau vélo', 19122, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578030388182427, 44.8446203086285, N'Arceau vélo', 19123, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579069165842664, 44.8436216195573, N'Arceau vélo', 19124, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56277404243913, 44.8337932030827, N'Arceau vélo', 19127, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568803453706068, 44.8341641877355, N'Arceau vélo', 19128, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565448128737769, 44.8315740916505, N'Arceau vélo', 19129, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581441421301811, 44.8432786364463, N'Arceau vélo', 19131, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58175239592717, 44.8443823376391, N'Arceau vélo', 19132, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565549183776676, 44.833867696982, N'Arceau vélo', 19133, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567273748480199, 44.8326627777149, N'Arceau vélo', 19135, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575501227722532, 44.8418655966773, N'Arceau vélo', 19139, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551568098867175, 44.8741880441934, N'Arceau vélo', 19140, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567925557037447, 44.8355999431155, N'Arceau vélo', 19143, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575071384860125, 44.8428939477907, N'Arceau vélo', 19144, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568757459048815, 44.8388476392796, N'Arceau vélo', 19149, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580670341768949, 44.8366387262322, N'Arceau vélo', 19151, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571874805587299, 44.8348523232502, N'Arceau vélo', 19152, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573624292678686, 44.8517187181938, N'Arceau vélo', 19154, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573644100555468, 44.8350598933985, N'Arceau vélo', 19155, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579781181856581, 44.84162979035, N'Arceau vélo', 19156, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577979042449839, 44.8321503482043, N'Arceau vélo', 19159, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574631362119166, 44.8340445722352, N'Arceau vélo', 19160, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574638105588859, 44.8349448953408, N'Arceau vélo', 19161, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575639472947575, 44.841592002387, N'Arceau vélo', 19162, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574971448102378, 44.8437420418078, N'Arceau moto', 19008, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57321742112745, 44.8353857824169, N'Arceau vélo', 19164, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581792894039177, 44.8342782289113, N'Arceau vélo', 19165, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574248316128538, 44.8431397781526, N'Arceau vélo', 19168, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563654710893811, 44.8344554565586, N'Arceau vélo', 19169, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574332131103976, 44.843551875855, N'Arceau vélo', 19171, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572819639077218, 44.8301822549927, N'Arceau vélo', 19172, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577846344705218, 44.8251925408379, N'Arceau vélo', 19173, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580510251154727, 44.822757799008, N'Arceau vélo', 19175, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572197557314525, 44.8481732680212, N'Arceau moto', 19130, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558609073031145, 44.8611335210813, N'Arceau moto', 20672, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580978289620513, 44.8225028209321, N'Arceau vélo', 19178, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577084302964526, 44.8230692492086, N'Arceau vélo', 19179, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575384608171937, 44.8431403862519, N'Arceau moto', 19005, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57614769255129, 44.8439189362416, N'Arceau moto', 20673, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579587197320767, 44.8420778282776, N'Arceau vélo', 19185, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57941147487125, 44.8423613445312, N'Arceau vélo', 19186, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579330153136752, 44.8425074909592, N'Arceau vélo', 19188, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579159017083925, 44.8427813139224, N'Arceau vélo', 19189, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572096752203341, 44.836008293531, N'Arceau vélo', 19191, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572072960097673, 44.835783839487, N'Arceau vélo', 19192, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572090238022037, 44.8195752083748, N'Arceau vélo', 19193, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.617514028362817, 44.8472473858857, N'Arceau vélo', 19194, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.6032033077793, 44.8407441085256, N'Arceau vélo', 19195, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578244566368279, 44.8442564951233, N'Arceau moto', 20674, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576317461325441, 44.8440347595254, N'Arceau moto', 19007, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579257098545757, 44.8433287119504, N'Arceau vélo', 19198, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.60997863163157, 44.8333383181135, N'Arceau vélo', 19200, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568955591169558, 44.8221843196349, N'Arceau vélo', 19202, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567184153116423, 44.8321337533433, N'Arceau vélo', 19203, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578494995053082, 44.8445455263947, N'Arceau vélo', 19204, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580076075070983, 44.841984439423, N'Arceau vélo', 19205, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570848456658173, 44.8327890885036, N'Arceau vélo', 19206, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562984393675169, 44.8339500213743, N'Arceau vélo', 19207, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581232126529188, 44.8422054250171, N'Arceau vélo', 19208, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581862269412786, 44.8450639436998, N'Arceau vélo', 19210, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563624812429685, 44.8325742616562, N'Arceau vélo', 19211, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579118676022955, 44.8428452788286, N'Arceau vélo', 19214, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565336675684164, 44.8336659107658, N'Arceau vélo', 19215, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576330986974134, 44.8437896758373, N'Arceau vélo', 19217, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566526464318956, 44.834879385289, N'Arceau vélo', 19219, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576597456685633, 44.8448184884251, N'Arceau vélo', 19223, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55276868448188, 44.8737755909901, N'Arceau vélo', 19387, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581407973322338, 44.8430583606683, N'Arceau vélo', 19212, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575742132860498, 44.8272322750464, N'Arceau vélo', 19251, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571436491859761, 44.8489486923069, N'Arceau vélo', 19216, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61553897108471, 44.8522593542238, N'Arceau vélo', 19220, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570604783872587, 44.8370543817946, N'Arceau vélo', 19221, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.592373466218206, 44.8453153641088, N'Arceau vélo', 19305, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575662176681397, 44.8433202937833, N'Arceau vélo', 19231, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582743294373759, 44.8404001110783, N'Arceau vélo', 19306, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5776773744708, 44.8244853158146, N'Arceau vélo', 19313, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.550963843761402, 44.8237606502411, N'Arceau vélo', 19319, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.541523738269833, 44.8517230000686, N'Arceau vélo', 19325, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58169927405098, 44.8304652593139, N'Arceau vélo', 19357, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601714756471843, 44.8400742647863, N'Arceau vélo', 19371, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56100185610066, 44.8247878126271, N'Arceau vélo', 19356, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568960271253397, 44.8308142285569, N'Arceau vélo', 19241, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561980529276401, 44.8636197571397, N'Arceau vélo', 19261, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580852827133193, 44.8444872150914, N'Arceau vélo', 19246, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5575911223398, 44.86007934111, N'Arceau vélo', 19262, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577966408068112, 44.8334961008078, N'Arceau vélo', 19264, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599011594210125, 44.8403049987598, N'Arceau vélo', 19268, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.595442482045739, 44.8404561991233, N'Arceau vélo', 19270, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581578252363455, 44.8387226378887, N'Arceau vélo', 19272, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.546511095795725, 44.8389261071609, N'Arceau vélo', 19280, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544691281726248, 44.8428849407113, N'Arceau vélo', 19281, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545189661312629, 44.8423052513573, N'Rack', 19282, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544854739902213, 44.842362345504, N'Rack', 19283, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573529971472727, 44.8348017039861, N'Arceau vélo', 19266, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.621586990179321, 44.8539210857718, N'Arceau vélo', 19284, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570697029301143, 44.8555612555028, N'Arceau vélo', 19293, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57170057925729, 44.8552942102349, N'Arceau vélo', 19294, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609461846367392, 44.8394746257849, N'Arceau vélo', 19295, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571397394728928, 44.8550715869326, N'Arceau vélo', 19296, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603261023743874, 44.8398734416493, N'Arceau vélo', 19299, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571693750487712, 44.8548655529535, N'Arceau vélo', 19279, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587782452460952, 44.8322003414216, N'Rack', 19301, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574851004706568, 44.8323511836891, N'Rack', 19302, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585503406735476, 44.8489270775805, N'Arceau vélo', 19289, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566567561433638, 44.8586079578579, N'Arceau vélo', 19304, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571155003063533, 44.855251335136, N'Arceau vélo', 19292, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.590394863533745, 44.8399728414237, N'Arceau vélo', 19298, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586612374783915, 44.8357593131179, N'Rack', 19309, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561005025563252, 44.8442799119002, N'Arceau vélo', 19317, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59264752965339, 44.8330971756095, N'Arceau vélo', 19308, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.550011613714568, 44.8460594147957, N'Arceau vélo', 19326, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551251341865433, 44.8236916830517, N'Arceau vélo', 19329, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585804283846352, 44.8474728806547, N'Arceau vélo', 19330, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585364709702761, 44.847441508964, N'Arceau vélo', 19331, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560192995371124, 44.8292693805466, N'Arceau vélo', 19334, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561095491324631, 44.8293688694363, N'Arceau vélo', 19338, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572507240301462, 44.8201178199834, N'Rack', 19342, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568918747023552, 44.830885703628, N'Arceau vélo', 19343, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575657118575702, 44.8373684733898, N'Arceau vélo', 19346, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570471502074533, 44.8635383409191, N'Arceau vélo', 19349, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573073565324755, 44.8369201357111, N'Arceau vélo', 19347, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57230498660308, 44.820587365877, N'Arceau vélo', 19333, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586537685251331, 44.8237472729801, N'Rack', 19354, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576481524136556, 44.8439669075882, N'Arceau vélo', 19355, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568149278247411, 44.8326057780893, N'Rack', 19345, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581886783210262, 44.840732178141, N'Arceau vélo', 19351, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581397759050506, 44.8304818671533, N'Arceau vélo', 19361, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564466248376317, 44.8482595600657, N'Arceau vélo', 19362, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554950791713493, 44.8435062431133, N'Arceau vélo', 19370, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603661026148844, 44.8305096405129, N'Arceau vélo', 19365, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559913100036963, 44.8318423513892, N'Rack', 19368, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55878287561362, 44.8406810193161, N'Arceau vélo', 19369, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55134741886424, 44.8726970948984, N'Arceau vélo', 19385, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552421267039602, 44.8428698221435, N'Arceau vélo', 19377, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57507548129393, 44.8174163262285, N'Arceau vélo', 19409, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577592291254556, 44.8123041075904, N'Arceau vélo', 19411, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564700778320967, 44.8348974111407, N'Rack', 19419, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574346194282686, 44.8374734366847, N'Arceau vélo', 19422, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586485972643668, 44.8448850835935, N'Rack', 19425, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573127726944289, 44.8315045098999, N'Arceau vélo', 19395, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561189193991804, 44.8248370774542, N'Arceau vélo', 19427, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572808265636604, 44.8493080262407, N'Arceau vélo', 19406, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573227252482021, 44.8306986770755, N'Arceau vélo', 19441, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.5734601954532, 44.8309296239964, N'Arceau vélo', 19457, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572866041159649, 44.8313694096922, N'Arceau vélo', 19472, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586920668012554, 44.8505072998781, N'Arceau vélo', 19473, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586987702629387, 44.8504348340137, N'Arceau vélo', 19476, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57461401538366, 44.8410775205172, N'Arceau vélo', 19429, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555966923786316, 44.8455210126071, N'Arceau vélo', 19434, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579023451624992, 44.8430020413469, N'Arceau vélo', 19438, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558089332559139, 44.8436884856953, N'Arceau vélo', 19486, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581497593604152, 44.8372481213869, N'Arceau vélo', 19440, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55815015626995, 44.8438040478026, N'Arceau vélo', 19487, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581764904668297, 44.8446323684592, N'Arceau vélo', 19489, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.548491054936975, 44.8488737050261, N'Arceau vélo', 19461, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578220616367798, 44.8321713067961, N'Arceau vélo', 19467, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58159034199627, 44.8442523167676, N'Arceau vélo', 19492, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572061836492697, 44.8398901414181, N'Arceau vélo', 19469, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578147484152752, 44.8444282685241, N'Arceau vélo', 19475, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579740222027542, 44.8418488038664, N'Arceau vélo', 19481, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588396271832533, 44.8916843614134, N'Arceau vélo', 19491, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58154834400638, 44.8439189885182, N'Arceau vélo', 19494, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581097322321879, 44.8419046537823, N'Arceau vélo', 19496, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.601856086754811, 44.8207674913131, N'Arceau vélo', 19498, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570515965865342, 44.8401090884256, N'Arceau vélo', 19499, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582789763251025, 44.8486873436745, N'Arceau vélo', 19537, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551949158776011, 44.8247085145051, N'Arceau vélo', 19501, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579035377977389, 44.8424338879538, N'Arceau vélo', 19560, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.605991030896544, 44.8428047215987, N'Arceau vélo', 19503, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573267431722812, 44.8402109908583, N'Arceau vélo', 19526, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581414522282581, 44.8381519946181, N'Arceau vélo', 19527, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57670952487351, 44.834865658231, N'Arceau vélo', 19506, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581175242237939, 44.8475305356878, N'Arceau vélo', 19534, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579027223605266, 44.8346914387471, N'Arceau vélo', 19508, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576373030051576, 44.8469221645603, N'Arceau vélo', 19535, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575332066336292, 44.8466062604265, N'Arceau vélo', 19536, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572422186341124, 44.8323713491474, N'Arceau vélo', 19513, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571973801693608, 44.8313341880892, N'Arceau vélo', 19512, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577416130503644, 44.8399084077688, N'Arceau vélo', 19516, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584435884860601, 44.8413431036613, N'Arceau vélo', 19523, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583098699444388, 44.8386395501458, N'Arceau vélo', 19515, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575076554773198, 44.8445236107626, N'Arceau vélo', 19556, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573988719238666, 44.8402108595512, N'Arceau vélo', 19525, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574889080728017, 44.8379550678915, N'Arceau vélo', 19518, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577153127090213, 44.8407254191101, N'Arceau vélo', 19524, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581901303931359, 44.8405157072957, N'Rack', 19520, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579552422980042, 44.8421417083501, N'Arceau vélo', 19559, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571618863095772, 44.8391269228357, N'Arceau vélo', 19522, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569172429289166, 44.8217106299329, N'Arceau vélo', 19631, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55768469430689, 44.8260688489673, N'Arceau vélo', 19619, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557371692199675, 44.8258785694547, N'Arceau vélo', 19620, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557385572837243, 44.8253919749049, N'Arceau vélo', 19621, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561594268574057, 44.8329673530101, N'Arceau vélo', 19623, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556439552755128, 44.826507733643, N'Arceau vélo', 19622, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.548890754736621, 44.8663761121012, N'Arceau vélo', 19531, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577541037086873, 44.83707784119, N'Arceau vélo', 19532, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579167123198281, 44.8366946673972, N'Arceau vélo', 19533, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576032728484889, 44.8456791180866, N'Arceau vélo', 19557, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567091082513719, 44.8376558817063, N'Arceau vélo', 19617, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557750407334583, 44.8258600608926, N'Arceau vélo', 19618, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573438860691922, 44.84213359822, N'Arceau vélo', 19540, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575745273670729, 44.8482976903292, N'Arceau vélo', 19544, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575973800133336, 44.8450052761668, N'Arceau vélo', 19555, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575192899162118, 44.8438093112122, N'Arceau vélo', 19545, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580206135481726, 44.8481492525582, N'Arceau vélo', 19543, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581285321544168, 44.8426292924698, N'Arceau vélo', 19552, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576077866590514, 44.8455699634971, N'Arceau vélo', 19554, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575263162183215, 44.8462387350368, N'Arceau vélo', 19553, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579093428950335, 44.8570028644339, N'Arceau vélo', 19547, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574755868320402, 44.8584346115406, N'Arceau vélo', 19548, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580390205063901, 44.8577265961231, N'Arceau vélo', 19549, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584389779005279, 44.8414343666034, N'Arceau vélo', 19558, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584995572355701, 44.8477402252251, N'Arceau vélo', 19551, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580607191108607, 44.8636769755519, N'Arceau vélo', 19565, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559288189759179, 44.8305914163634, N'Arceau vélo', 19592, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.55913024596865, 44.8308358794343, N'Arceau vélo', 19593, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567972839406814, 44.8296647867942, N'Arceau vélo', 19594, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567157333365429, 44.8583824740807, N'Arceau vélo', 19595, 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557290436615328, 44.8436913912796, N'Arceau vélo', 19598, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559043632198543, 44.8440797673773, N'Arceau vélo', 19599, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565084533180135, 44.8334291538685, N'Arceau vélo', 19611, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565101929197941, 44.833779830962, N'Arceau vélo', 19612, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567674841981306, 44.838199500413, N'Arceau vélo', 19616, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57335174234286, 44.8523468473636, N'Arceau vélo', 19563, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572147438374989, 44.8504399986046, N'Arceau vélo', 19564, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588132189944139, 44.8461842274156, N'Arceau vélo', 19602, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575031921408457, 44.8591479353437, N'Arceau vélo', 19587, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.602377853598483, 44.831415687277, N'Arceau vélo', 19575, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575793794664551, 44.8368066699446, N'Arceau vélo', 19574, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581230066269042, 44.8624237300843, N'Arceau vélo', 19573, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594838451121234, 44.8521915376219, N'Arceau vélo', 19590, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579166413461115, 44.8322348821853, N'Arceau vélo', 19576, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59517625370753, 44.8432684858279, N'Arceau vélo', 19603, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566791166835873, 44.8326298940444, N'Arceau vélo', 19610, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575173364421829, 44.8479968627957, N'Arceau vélo', 19581, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57836480673693, 44.8472681617481, N'Arceau vélo', 19582, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573000182755986, 44.8529140394136, N'Arceau vélo', 19585, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573285564856471, 44.8527987857572, N'Arceau vélo', 19586, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588475952324503, 44.8545676789453, N'Arceau vélo', 19588, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575312049952391, 44.8596901582793, N'Arceau vélo', 19584, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569172429289166, 44.8217106299329, N'Arceau vélo', 19591, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568405150696981, 44.8391446022767, N'Arceau vélo', 19615, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587916169231815, 44.8464057912146, N'Arceau vélo', 19601, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58672495488473, 44.8380860721473, N'Arceau vélo', 19605, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.589195522362502, 44.8365494866282, N'Arceau vélo', 19606, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587163167882621, 44.8372183798146, N'Arceau vélo', 19608, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563050169262372, 44.8340023699575, N'Arceau vélo', 19609, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568837373045833, 44.839692066858, N'Arceau vélo', 19614, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555308514131523, 44.8289376719322, N'Arceau vélo', 19625, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567815787151437, 44.8226721859979, N'Arceau vélo', 19632, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.550163402785032, 44.8174865550134, N'Arceau vélo', 19627, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559900319030976, 44.827886264245, N'Arceau vélo', 19630, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558807516957259, 44.8275354517366, N'Arceau vélo', 19629, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56679119905943, 44.8303402353391, N'Arceau vélo', 19637, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.591168690184547, 44.8334919352637, N'Arceau vélo', 19638, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580025776284195, 44.8605349679019, N'Arceau vélo', 19649, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.583670816349802, 44.8584800278863, N'Arceau vélo', 19650, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.585014682069897, 44.8597877520318, N'Arceau vélo', 19648, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59508939516001, 44.8493667677761, N'Arceau vélo', 19652, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.605741435647203, 44.8535265194834, N'Arceau vélo', 19654, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584645233704808, 44.8584733982524, N'Autre', 19651, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598990691904648, 44.8521942679906, N'Arceau vélo', 19653, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.619901178376733, 44.8521569310934, N'Arceau vélo', 19655, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.602208074104306, 44.8318162512651, N'Arceau vélo', 19658, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.590290272693073, 44.8341445506087, N'Arceau vélo', 19660, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598169804426774, 44.8305845121408, N'Arceau vélo', 19669, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.611759593128113, 44.8325185385382, N'Arceau vélo', 19662, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575257718657264, 44.8351903548167, N'Arceau vélo', 19672, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597655028836473, 44.828077872407, N'Arceau vélo', 19667, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.589315586407021, 44.8343942892685, N'Arceau vélo', 19665, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588710921583359, 44.8373528016422, N'Arceau vélo', 19666, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578048067839063, 44.8325177787131, N'Arceau vélo', 19673, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580526651587113, 44.8348116349519, N'Arceau vélo', 19671, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572465480423621, 44.8314608079011, N'Arceau vélo', 19674, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572049690547299, 44.8396473057822, N'Arceau vélo', 19676, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577139487022932, 44.8460474461245, N'Arceau vélo', 19679, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581144685217543, 44.8415259951686, N'Arceau vélo', 19680, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.621140068243064, 44.8536290226766, N'Arceau vélo', 19689, 8,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.576919822547523, 44.8500239269182, N'Arceau vélo', 19683, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574568817131237, 44.8518750741695, N'Arceau vélo', 19684, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594104614583309, 44.8527053855569, N'Arceau vélo', 19687, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596013510661656, 44.8501266862568, N'Arceau vélo', 19690, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555391409279443, 44.8155009541301, N'Arceau vélo', 19693, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.540516019171953, 44.8523578781295, N'Arceau vélo', 19699, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.598541164888308, 44.8402025899581, N'Arceau vélo', 19704, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.591512759274638, 44.8327808726985, N'Arceau vélo', 19703, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599450309567523, 44.8298152467207, N'Arceau vélo', 19708, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.59437696404588, 44.8329939108067, N'Arceau vélo', 19707, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599076788670253, 44.8440699709114, N'Arceau vélo', 19710, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588150118799263, 44.8343697931841, N'Arceau vélo', 19712, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.588268270642775, 44.8410391892197, N'Arceau vélo', 19724, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570457786843024, 44.824154532815, N'Arceau vélo', 19736, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554403041574312, 44.8234135460017, N'Arceau vélo', 19737, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558888237445383, 44.8294243325365, N'Arceau vélo', 19740, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599093712913548, 44.8398644244657, N'Arceau vélo', 19716, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557152891733202, 44.8281081285733, N'Arceau vélo', 19741, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556723571152937, 44.8276146722701, N'Arceau vélo', 19742, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599149457389867, 44.8364322604824, N'Arceau vélo', 19719, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.594873686010567, 44.8335512265613, N'Arceau vélo', 19721, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.597091391199489, 44.8481454239028, N'Arceau vélo', 19723, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.591692792381924, 44.8333436031955, N'Arceau vélo', 19726, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561458125284494, 44.8314940339585, N'Arceau vélo', 19743, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565606708433686, 44.8217551587848, N'Arceau vélo', 19732, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562787225576772, 44.8276337518696, N'Arceau vélo', 19733, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.563296865709371, 44.8271142435856, N'Arceau vélo', 19734, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567722094012812, 44.8299697345377, N'Arceau vélo', 19735, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551785997523285, 44.8249917740952, N'Arceau vélo', 19744, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561043022880333, 44.8305229939436, N'Arceau vélo', 19739, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571715939809442, 44.8522890144577, N'Arceau vélo', 19752, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571209353115449, 44.8426039026837, N'Arceau vélo', 19757, 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570855916997813, 44.8520494521671, N'Arceau vélo', 19754, 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.557323263089708, 44.8420517275887, N'Arceau vélo', 19804, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558059865773353, 44.8415920833, N'Arceau vélo', 19806, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558379716488068, 44.8414039948122, N'Arceau vélo', 19807, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566894812174509, 44.855859635901, N'Arceau vélo', 20758, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565726505240592, 44.8589908425086, N'Arceau vélo', 20759, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567786906252804, 44.8560745329907, N'Arceau vélo', 20760, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567571427891386, 44.8559121166372, N'Arceau vélo', 20761, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564510088698927, 44.8341848166667, N'Arceau vélo', 20770, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561200404999244, 44.8234435496587, N'Arceau vélo', 20772, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567205622101993, 44.8301292463855, N'Arceau vélo', 20773, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568065566286501, 44.8301768675984, N'Arceau vélo', 20774, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56695482484815, 44.8298956944821, N'Arceau vélo', 20775, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565912604675224, 44.8295924555717, N'Arceau vélo', 20776, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552353498275343, 44.8224284973111, N'Arceau vélo', 20777, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559294939540897, 44.8302724127568, N'Arceau vélo', 20778, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564243360797203, 44.8301558751186, N'Arceau vélo', 20779, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566184075452108, 44.8279104374381, N'Arceau vélo', 20780, 6,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561302950177733, 44.8240275709422, N'Arceau vélo', 20781, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566993379405716, 44.832761467607, N'Arceau vélo', 20782, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.553966988481269, 44.8242054572138, N'Arceau vélo', 20783, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56378758108849, 44.8223370592228, N'Arceau vélo', 20784, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566446867268442, 44.8307224299145, N'Arceau vélo', 20785, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566785153213466, 44.8322505748601, N'Arceau vélo', 20786, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.567180419682833, 44.8325416633555, N'Arceau vélo', 20787, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.565048150983808, 44.834655832946, N'Arceau vélo', 20788, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.566642305400514, 44.8343743682608, N'Arceau vélo', 20789, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57092683928835, 44.8332469391435, N'Arceau vélo', 20790, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570916715625638, 44.833097903643, N'Arceau vélo', 20791, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562398846689282, 44.83425435939, N'Arceau vélo', 20792, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562553141369358, 44.8343440223796, N'Arceau vélo', 20793, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554130983918798, 44.8288876258945, N'Arceau vélo', 20794, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573845134246122, 44.8329680359089, N'Arceau vélo', 20795, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570581519319441, 44.8316786677955, N'Arceau vélo', 20796, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570248156663194, 44.8317289444065, N'Arceau vélo', 20797, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.56310349655467, 44.8221716616227, N'Arceau vélo', 20798, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.564703382253727, 44.8266736495092, N'Arceau vélo', 20799, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580703605913986, 44.835306105042, N'Arceau vélo', 20830, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580650738112762, 44.8351684137208, N'Arceau vélo', 20831, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580407561351298, 44.8351300346296, N'Arceau vélo', 20832, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580025688133761, 44.8333405245432, N'Arceau vélo', 20833, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579979057747993, 44.8332175901415, N'Arceau vélo', 20834, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.57972341787951, 44.8330925839519, N'Arceau vélo', 20835, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603287294195927, 44.8295920998732, N'Arceau vélo', 20836, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609896255711943, 44.8348501693521, N'Arceau vélo', 20837, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.604694585463014, 44.8310167655472, N'Arceau vélo', 20838, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579032994152858, 44.8370870441015, N'Arceau vélo', 20839, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609845346743348, 44.8321748999578, N'Arceau vélo', 20840, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.609312884831296, 44.8321282818319, N'Arceau vélo', 20841, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.603710157519554, 44.8306535669759, N'Arceau vélo', 20842, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.605272773897061, 44.8310357422234, N'Arceau vélo', 20843, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.589741645631854, 44.833466703526, N'Arceau vélo', 20844, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.596301338514355, 44.8311767808888, N'Arceau vélo', 20845, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586647336947888, 44.8340698208717, N'Arceau vélo', 20846, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587348034446451, 44.8338336984491, N'Arceau vélo', 20847, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.591456767638181, 44.8289401283226, N'Arceau vélo', 20848, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584772846374226, 44.8322779081235, N'Arceau vélo', 20849, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579211878706541, 44.835208188706, N'Arceau vélo', 20850, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580191654437315, 44.8348154200232, N'Arceau vélo', 20851, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572771556328667, 44.8368925956679, N'Arceau vélo', 20852, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.599729694849452, 44.8297788424044, N'Arceau vélo', 20853, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572157887748977, 44.8348545163683, N'Arceau vélo', 20854, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570577562212551, 44.8349831187622, N'Arceau vélo', 20855, 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582447981601944, 44.8218647151869, N'Arceau vélo', 20856, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572781674738553, 44.8323177236076, N'Arceau vélo', 20857, 5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562986684998616, 44.823221960666, N'Arceau moto', 20910, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.560989149570479, 44.8657844851219, N'Arceau vélo', 21170, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.559778893183559, 44.8650944923199, N'Arceau vélo', 21171, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.61331877268067, 44.8461902049629, N'Arceau moto', 21199, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.556685133934167, 44.8402000002999, N'Arceau vélo', 21200, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.555461167639901, 44.8403962483154, N'Arceau vélo', 21201, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554576003534264, 44.8405022702381, N'Arceau vélo', 21202, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.554380931930763, 44.8404125188495, N'Arceau vélo', 21203, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.553429680265362, 44.8404508745258, N'Arceau vélo', 21204, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.551817998158854, 44.8406939597007, N'Arceau vélo', 21205, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549458427927606, 44.8408570306722, N'Arceau vélo', 21206, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.548246186292823, 44.8410304045054, N'Arceau vélo', 21207, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.547129230374188, 44.8414271639114, N'Arceau vélo', 21208, 7,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.605913169208641, 44.8396657595145, N'Arceau vélo', 21219, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.606328304665647, 44.8396351296676, N'Arceau vélo', 21220, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.607347952670596, 44.8396401087351, N'Arceau vélo', 21221, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586223209757506, 44.8563222896002, N'Arceau vélo', 21224, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.578521687070391, 44.8350170365819, N'Arceau moto', 21225, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570435489396111, 44.8243094503074, N'Arceau vélo', 21230, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.549552927810225, 44.8421749145562, N'Arceau vélo', 21232, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.553874432651746, 44.8425806957992, N'Arceau vélo', 21233, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.553032804373047, 44.8419726369717, N'Arceau vélo', 21234, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.602430918682018, 44.8372387025298, N'Arceau vélo', 21238, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581350483812212, 44.8315969352152, N'Arceau vélo', 21240, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.562111962439596, 44.8258614035807, N'Arceau moto', 21255, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.561666439670853, 44.8220141633135, N'Arceau moto', 21256, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.552842008549998, 44.8607479589444, N'Arceau moto', 21258, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581628395550697, 44.8381586850598, N'Arceau moto', 21290, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.602364695544114, 44.8341571160682, N'Arceau vélo', 21310, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577500168663284, 44.8254434926079, N'Arceau vélo', 21311, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544229350699179, 44.8306730849563, N'Arceau vélo', 21312, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.571971083945924, 44.8534646611696, N'Arceau vélo', 21394, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572446484310468, 44.8537034969498, N'Arceau vélo', 21395, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569013906256527, 44.852023362523, N'Arceau vélo', 21399, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569636190797357, 44.8523461520989, N'Arceau vélo', 21400, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580538496589948, 44.8355847860722, N'Arceau vélo', 20810, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.579672529186252, 44.8329520375067, N'Arceau vélo', 20811, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580595487418515, 44.8349740464572, N'Arceau vélo', 20812, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.580164807328364, 44.8336948489413, N'Arceau vélo', 20813, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.574988900936721, 44.8436458281209, N'Arceau vélo', 20814, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.58749152093708, 44.8412061876794, N'Arceau vélo', 20815, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.569924658294648, 44.8391543233284, N'Arceau vélo', 20816, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573016784626121, 44.8398380913782, N'Arceau vélo', 20817, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568331139041792, 44.8381684724257, N'Arceau vélo', 20818, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570575627872461, 44.8392087558619, N'Arceau vélo', 20819, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.577506401715977, 44.8394007581133, N'Arceau vélo', 20820, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.581898895750766, 44.8367467109947, N'Arceau vélo', 20823, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.570675920920101, 44.8372951861817, N'Arceau vélo', 20822, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.573085478898923, 44.8423519692591, N'Arceau vélo', 20824, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.586589931484066, 44.8362406932594, N'Arceau vélo', 20825, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.587110812148129, 44.8364141647031, N'Arceau vélo', 20826, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.592275145423055, 44.852982758577, N'Arceau vélo', 20827, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.592171004457866, 44.8530470302929, N'Arceau vélo', 20828, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.572945850771162, 44.8430035805277, N'Arceau vélo', 20829, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.582584579794979, 44.8404233943851, N'Arceau moto', 21150, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.558785824033073, 44.8410648466664, N'Arceau vélo', 21330, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.548268675793122, 44.8472084102457, N'Arceau vélo', 21331, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.54782624350707, 44.847473433681, N'Arceau vélo', 21332, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545684252043338, 44.8487710248296, N'Arceau vélo', 21333, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.545350825994526, 44.8489785044458, N'Arceau vélo', 21334, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544680437260988, 44.8497537961061, N'Arceau vélo', 21335, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.544513048348643, 44.8498548531016, N'Arceau vélo', 21336, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.568157740135486, 44.8332697642555, N'Arceau vélo', 21337, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575747285446385, 44.8416114051172, N'Arceau vélo', 21338, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.575962463008967, 44.841653196995, N'Arceau vélo', 21339, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Quartier, Num_Categorie) VALUES(-0.584880983785539, 44.8522284198091, N'Arceau vélo', 21496, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

