#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>
#include "utils/csv.h"
#include "utils/sql.h"

using namespace std;

void traiterVille(string ligne, fstream &fichierSql)
{

    string gauche, droite;
    coupeEnDeux(ligne, ';', gauche, droite);
    fichierSql << "INSERT INTO"
           << "VILLES(Num_Ville, Nom_Villes) "
           << "VALUES(" << gauche << "," << droite << ")"
           << endl << endl;

}


void traiterVilles(string nomVillesCsv, string nomVillesSql) {

    ifstream villesCsv;
    string ligne;

    // ouverture fichier csv en lecture
    villesCsv.open(nomVillesCsv.c_str(), ios::in);
    if (villesCsv.fail()) {
        cerr << "ouverture du fichier " << nomVillesCsv
             << " impossible." << endl;
        exit(EXIT_FAILURE);
    }

    // ouverture fichier sql en ecriture
    fstream villesSql;
    villesSql.open(nomVillesSql.c_str(), ios::out);
    if (villesSql.fail()) {
      cerr << "ouverture du fichier " << nomVillesSql
           << " impossible." << endl;
      exit(EXIT_FAILURE);
    }

    // suppression des anciennes villes
    villesSql << "DELETE FROM VILLES" << endl << endl;

    // lecture des lignes du fichier des villes
    bool premiereLigne = true;
    while (getline(villesCsv, ligne)) {
        if (premiereLigne) { // on ne traite pas la ligne des titres
            premiereLigne = false;
            continue;
        }
        traiterVille(ligne, villesSql);
    }
    villesCsv.close();
    villesSql.close();
}

void analyserLigneDeCommande(int argc, char *argv[],
                             string &cheminCsv, string &cheminSql){

    if( argc != 3 ) {
      cout << "Erreur : nombre d'arguments incorrect." << endl;
      cout << "Usage : " << argv[0]
           << " configVilles.csv villes.sql" << endl;
      exit(-1);
    }
    cheminCsv = argv[1];
    cheminSql = argv[2];
}

int main(int argc, char *argv[])
{
    string cheminSql, cheminCsv;
    analyserLigneDeCommande(argc, argv, cheminCsv, cheminSql);
    traiterVilles(cheminCsv, cheminSql);
}
