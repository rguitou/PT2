/* Ce programme convertit des points extraits de la base de données, stockés
 * dans un fichier CSV, vers un format JSON utilisable pour les cartes Google.
 */
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>
#include "utils/chaines.h"
#include "utils/csv.h"
#include "utils/json.h"
#include "utils/sql.h"
using namespace std;

/*
 * Exporte tous les points d'une catégorie vers un fichier Json.
 */
void exportJsonPointsCategorie(string nomFichierCsv, string nomFichierJson,
							   string cheminIcones) {
    ifstream fichierCsv;
    fstream fichierJson;
    string ligne;

    // ouverture fichier csv en lecture
    fichierCsv.open(nomFichierCsv.c_str(), ios::in);
    if (fichierCsv.fail()) {
        cerr << "ouverture du fichier " << nomFichierCsv << " impossible."
			 << endl;
        exit(EXIT_FAILURE);
    }

    // ouverture fichier json en ecriture
    fichierJson.open(nomFichierJson.c_str(), ios::out);
    if (fichierJson.fail()) {
        cerr << "ouverture du fichier " << nomFichierJson << " impossible."
			 << endl;
        exit(EXIT_FAILURE);
    }
	jsonInsererDebut(fichierJson);

    // lecture des lignes du fichier csv des points extraits
	bool premiereLigne = true;
    while (getline(fichierCsv, ligne)) {
        int nbCol;
        TabLigne colonnes;
        coupeEnPlusieurs(ligne, CSV_SEP, nbCol, colonnes);
        if (nbCol<JSON_NB_COLONNES) {
            cerr << "Une ligne du fichier " << nomFichierCsv;
            cerr << " ne contient pas assez de colonnes : " << endl;
			cerr << ligne << endl;
        }
		if (!premiereLigne) {
		  fichierJson << "," << endl;
		} else {
		  premiereLigne = false;
		}
        jsonInsererPoint(fichierJson,
						 colonnes[JSON_COL_X], colonnes[JSON_COL_Y],
						 supprimeEspacesDroite(colonnes[JSON_COL_TEXT]),
						 cheminIcones +
						 supprimeEspacesDroite(colonnes[JSON_COL_ICONE]));
    }
	jsonInsererFin(fichierJson);
    fichierJson.close();
    fichierCsv.close();
}


/*
 * Ouverture du fichier config.csv, et pour chacune de ses lignes,
 * traitement de la categorie correspondante.
 */
void traiterFichierConfig(string configCsv, string cheminCsv,
													string cheminJson, string cheminSqlExtraction,
													string cheminIcones) {

    ifstream configCsvFic;
    string ligne;


    // ouverture fichier config.csv en lecture
    configCsvFic.open(configCsv.c_str(), ios::in);
    if (configCsvFic.fail()) {
        cerr << "ouverture du fichier " << configCsv
			 << " impossible." << endl;
        exit(EXIT_FAILURE);
    }

    // lecture des lignes du fichier config.csv
	bool premiereLigne = true;
    while (getline(configCsvFic, ligne)) {
	    if (premiereLigne) { // on ne traite pas la ligne des titres
		    premiereLigne = false;
		    continue;
	    }
		infoCategorie categorie;
		bool ok = lireLigneConfigCsv(ligne, configCsv, categorie);
		if (!ok) {
		  return;
		}
		string nomFichierCsv  = cheminCsv  + categorie.id + ".sql.csv";
		string nomFichierJson = cheminJson + categorie.id + ".json";
		// export de tous les points de la catégorie
		exportJsonPointsCategorie(nomFichierCsv, nomFichierJson,
								  cheminIcones);
    }
    configCsvFic.close();
}

/*
 * Analyse de la ligne de commande.
 */
void analyserLigneDeCommande(int argc, char *argv[], string &configCsv,
							string &cheminCsv, string &cheminJson,
							string &cheminSqlExtraction, string &cheminIcones) {

    if( argc != 6 ) {
	  cout << "Erreur : nombre d'arguments incorrect." << endl;
	  cout << "Usage : " << argv[0]
		   << " config.csv quartiers.csv cheminCsv cheminJson cheminRequetesExtraction cheminIcones"
		   << endl;
	  exit(-1);
	}
	configCsv = argv[1];
	cheminCsv = argv[2];
	cheminJson = argv[3];
	cheminSqlExtraction = argv[4];
	cheminIcones = argv[5];
}

/*
 * Procédure principale.
 */
int main( int argc, char* argv[] )
{
  string configCsv, cheminCsv, cheminJson, cheminSqlExtraction;
  string cheminIcones;
  analyserLigneDeCommande(argc, argv, configCsv, cheminCsv,
						  cheminJson, cheminSqlExtraction, cheminIcones);
  traiterFichierConfig(configCsv, cheminCsv, cheminJson,
					   cheminSqlExtraction, cheminIcones);
  return EXIT_SUCCESS;
}
