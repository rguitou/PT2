/*
 * Programme générant les requêtes SQL d'extraction des points de la base,
 * et par catégorie.
 */
#include <cstdlib>
#include <iostream>
#include <string>
#include "utils/csv.h"
#include "utils/sql.h"
using namespace std;

/* Extraction des informations d'une catégorie, et de ses points :
 * on génère les requêtes SQL SELECT correspondantes.
 */
void extraireCategorie(infoCategorie categorie, string cheminSql) {
    fstream fichierSql;
	string nomFichierSql = cheminSql + categorie.id + ".sql";

	// ouverture fichier sql en ecriture
    fichierSql.open(nomFichierSql.c_str(), ios::out);
    if (fichierSql.fail()) {
        cerr << "ouverture du fichier " << nomFichierSql << " impossible."
			 << endl;
        exit(EXIT_FAILURE);
    }

	// extraire les points pour cette catégorie
	sqlExtrairePointsCategorie(categorie, fichierSql);

    fichierSql.close();
}

/*
 * Ouverture du fichier config.csv, et pour chacune de ses lignes,
 * traitement de la categorie correspondante.
 */
void traiterFichierConfig(string configCsv, string cheminSql) {

    ifstream configCsvFic;
    string ligne;

    // ouverture fichier csv en lecture
    configCsvFic.open(configCsv.c_str(), ios::in);
    if (configCsvFic.fail()) {
        cerr << "ouverture du fichier " << configCsv
			 << " impossible." << endl;
        exit(EXIT_FAILURE);
    }

    // lecture des lignes du fichier de configuration
	bool premiereLigne = true;
    while (getline(configCsvFic, ligne)) {
	    if (premiereLigne) { // on ne traite pas la ligne des titres
		    premiereLigne = false;
		    continue;
	    }
		infoCategorie categorie;
		bool ok = lireLigneConfigCsv(ligne, configCsv, categorie);
		if (!ok) {
		  return;
		}
		extraireCategorie(categorie, cheminSql);
    }
    configCsvFic.close();
}

/*
 * Analyse de la ligne de commande.
 */
void analyserLigneDeCommande(int argc, char *argv[], string &configCsv,
							 string &cheminSql) {

    if( argc != 3 ) {
	  cout << "Erreur : nombre d'arguments incorrect." << endl;
	  cout << "Usage : " << argv[0]
		   << " config.csv cheminSql" << endl;
	  exit(-1);
	}
	configCsv = argv[1];
	cheminSql = argv[2];
}

/*
 * Procédure principale.
 */
int main( int argc, char* argv[] )
{
  string configCsv, cheminSql;
  analyserLigneDeCommande(argc, argv, configCsv, cheminSql);
  traiterFichierConfig(configCsv, cheminSql);
  return EXIT_SUCCESS;
}
