#ifndef HTML_H
#define HTML_H

#include <string>
#include "csv.h"

std::string chargerJsonCategories(std::string nomMap, std::string cheminJson,
								  infoCategorie categorie);

std::string listePagesCategories(infoCategorie categorie);

std::string pointCategorie(std::string x, std::string y, std::string texte);

#endif // HTML_H
