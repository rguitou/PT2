#ifndef SQL_H
#define SQL_H

#include <fstream>
#include "csv.h"

std::string virgulePoint(std::string s);

std::string echapperQuote(std::string s);

void sqlInsererPoint(std::fstream &fichierSql, std::string nomCategorie,
                     std::string x, std::string y, std::string txt,
                     std::string cle);

void sqlSupprimerPointsCategorie(std::fstream &fichierSql,
								 std::string nomCategorie);

void sqlSupprimerCategorie(std::fstream &fichierSql, std::string nomCategorie);

void sqlInsererCategorie(std::fstream &fichierSql, std::string nomCategorie,
						 std::string cheminIcone, std::string date);

void sqlExtrairePointsCategorie(infoCategorie categorie,
								std::fstream &fichierSql);

#endif // SQL_H
