/*
 * Fonctions utiles pour le traitement des fichiers CSV.
 */
#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include "csv.h"
#include "chaines.h"

using namespace std;

/* Coupe une chaine en deux morceaux séparés par un caractère donné.
 * Renvoie vrai ssi le caractère a été trouvé.
 */
bool coupeEnDeux(string chaine, char sep, string &gauche, string &droite) {
  size_t trouve = chaine.find(sep);
  if (trouve != string::npos) {
    gauche = chaine.substr(0, trouve);
    droite = chaine.substr(trouve+1);
  }
  return (trouve!=string::npos);
}

/* Coupe une chaine en plusieurs morceaux séparés par un caractère donné.
 * Le nombre de morceaux maximal est LONGUEUR_MAX_LIGNE_CSV.
 */
void coupeEnPlusieurs(string chaine, char sep,
                      int &nbMorceaux, TabLigne &morceaux) {
  string gauche, droite;
  nbMorceaux = 0;
  while (coupeEnDeux(chaine, sep, gauche, droite) && nbMorceaux<MAX_LIGNE)
  {
    morceaux[nbMorceaux] = gauche;
    chaine = droite;
    nbMorceaux++;
  }
  if (nbMorceaux<MAX_LIGNE) {
      morceaux[nbMorceaux] = chaine;
      nbMorceaux++;
  }
}

/* Renvoie le plus grand indice de colonne parmi ceux de la catégorie. */
int indiceColonneMax(infoCategorie categorie) {
  int maxIndice = max(categorie.colX, categorie.colY);
  maxIndice = max(maxIndice, categorie.colTxt);
  maxIndice = max(maxIndice, categorie.colCle);
  return maxIndice;
}

/* Lit une ligne du fichier config.csv et renseigne les infos de la catégorie
 * lue.
 * Renvoie vrai ssi la lecture s'est bien déroulée.
 */
bool lireLigneConfigCsv(string ligne, string configCsv,
						infoCategorie &categorie) {
  int nbCol;
  TabLigne colonnes;
  coupeEnPlusieurs(ligne, CSV_SEP, nbCol, colonnes);
  if (nbCol!=NB_COL_CONFIG_CSV) {
	cerr << "Une ligne du fichier " << configCsv;
	cerr << " ne contient pas le bon nombre de colonnes : " << endl;
	cerr << ligne << endl;
	cerr << "Fichier ignoré." << endl;
	return false;
  }
  categorie.id = colonnes[0];
  categorie.nom = colonnes[1];
  categorie.date = colonnes[2];
  categorie.colX = atoi(colonnes[3].c_str())-1;
  categorie.colY = atoi(colonnes[4].c_str())-1;
  categorie.colTxt = atoi(colonnes[5].c_str())-1;
  categorie.colCle = atoi(colonnes[6].c_str())-1;
  categorie.cheminIcone = colonnes[8];
  return true;
}
