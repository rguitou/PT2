/*
 * Quelques fonctions utiles pour la génération de code HTML.
 */
#include "chaines.h"
#include "html.h"

using namespace std;

/*
 * Contenu d'un mot-clé CHARGER_JSON_CATEGORIES pour une catégorie donnée.
 */
string chargerJsonCategories(string nomMap, string cheminJson,
							 infoCategorie categorie) {
  return "        " + nomMap + ".data.loadGeoJson('"
	+ cheminJson + categorie.id + ".json');\n";
}

/*
 * Contenu d'un mot-clé LISTE_PAGES_CATEGORIES pour une catégorie donnée,
 * sur la page index.html.
 */
string listePagesCategories(infoCategorie categorie) {
  return "<li><img src=\"images/icones/" + categorie.cheminIcone
  + "\"/> <a href=\"categories-" + categorie.id + ".html\">"
  + categorie.nom + "</a>\n";
}

/*
 * Contenu d'une ligne décrivant un point, sur la page d'une catégorie.
 */
string pointCategorie(string x, string y, string texte) {
  return "      <tr><td>" + x + "</td><td>" + y + "</td><td>" + texte
	+ "</td></tr>\n";
}
