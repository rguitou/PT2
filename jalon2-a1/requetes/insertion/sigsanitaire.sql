DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Toilettes publiques')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Toilettes publiques'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Toilettes publiques', N'toilets.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.574467312151664, 44.8438837199486, N'TOILETTES MIXTES PMR', 5801, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.584577183300566, 44.8611674592142, N'Urinoir Pierre Trébod', 112, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.572138740169289, 44.8423716922678, N'TOILETTES MIXTES PMR', 5802, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.612880665162825, 44.8246288668863, N'Urinoir Alfred Smith', 114, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.546770710046757, 44.8415949968253, N'Urinoir Benauge', 116, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.549668817580285, 44.8705977268006, N'Urinoir Buscaillet', 117, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.578579667861026, 44.8349354916407, N'Urinoir République', 118, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.572441821689467, 44.847136163724, N'CHALET DE NECESSITE AVEC DOUCHES', 119, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.548211299705866, 44.87699109038, N'Urinoir Philippe Lebon', 120, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.586087834620069, 44.8391943310741, N'Urinoir Lateulade', 124, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.545747531899173, 44.849346248441, N'Urinoir Avenue Thiers', 129, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.54438084445224, 44.8750695649701, N'Urinoir Albert Brandenburg', 133, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.563484428073626, 44.8133891925777, N'Urinoir Boulevard Albert 1er', 135, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.571894569744631, 44.8314301072816, N'Urinoir Paul Broca', 136, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.564321470670066, 44.8267129011554, N'Urinoir Dormoy', 138, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.577383972080092, 44.8229896978552, N'Urinoir Simiot', 140, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.550259965161317, 44.8430567057489, N'Urinoir Calixte Camelle', 141, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.601448460338731, 44.83155637711, N'Urinoir Edouard Laroque', 144, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.562917322479597, 44.829530512184, N'Urinoir André Meunier', 145, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.588363598189398, 44.830789557383, N'Urinoir Arlac', 146, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.607806284750287, 44.835185610133, N'Urinoir Frantz Schrader', 179, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.545190734088545, 44.8429220037914, N'Urinoir Ferdinand Palau', 183, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.582568237883878, 44.8386928250834, N'TOILETTES MIXTES PMR', 5781, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.56727848446677, 44.8387910517145, N'TOILETTES MIXTES PMR', 5782, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.6214603566178, 44.8570871025109, N'Sanitaire automatique', 5783, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.599287298022792, 44.8535319679415, N'TOILETTES MIXTES PMR', 5784, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.56594780098854, 44.904904108173, N'TOILETTES MIXTES PMR', 5785, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.582290857486657, 44.8605917147978, N'TOILETTES MIXTES PMR', 5786, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.616170914392001, 44.8516061819138, N'SANITAIRE AUTOMATIQUE', 5787, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.610951424066316, 44.8331408015695, N'TOILETTES MIXTES PMR', 5788, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.60333974270816, 44.8295117974338, N'TOILETTES MIXTES PMR', 5789, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.594328151025758, 44.8330130187272, N'TOILETTES MIXTES PMR', 5790, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.573314789766353, 44.8446463221507, N'TOILETTES MIXTES PMR', 5791, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.572454186136095, 44.8352494948124, N'TOILETTES MIXTES PMR', 5792, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.57562356883447, 44.8411420095674, N'TOILETTES MIXTES PMR', 5811, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.567017406588822, 44.8526618303676, N'TOILETTES MIXTES PMR', 5812, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.569853946277985, 44.8490497773707, N'TOILETTES MIXTES PMR', 5813, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.574045258693568, 44.8519742996232, N'TOILETTES MIXTES PMR', 5814, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.57033289016278, 44.846846166257, N'TOILETTES MIXTES PMR', 5815, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.574961625738932, 44.8469010201312, N'TOILETTES MIXTES PMR', 5816, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.55118585241094, 44.8277019032597, N'TOILETTES MIXTES PMR', 5862, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.577106102676729, 44.8332744380251, N'TOILETTES MIXTES PMR', 5818, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.580138546512449, 44.8348205141139, N'TOILETTES MIXTES PMR', 5819, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.573131616843663, 44.8312773849294, N'TOILETTES MIXTES PMR', 5820, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.566810186772343, 44.8305549317135, N'TOILETTES MIXTES PMR', 5821, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.562329653949059, 44.8296875809056, N'TOILETTES MIXTES PMR', 5861, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.550565569664043, 44.8274116598213, N'TOILETTES MIXTES PMR', 5863, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.554615102591232, 44.8243397850155, N'TOILETTES MIXTES PMR', 5864, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.563160532496793, 44.8223735462417, N'TOILETTES MIXTES PMR', 5865, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.572011114618454, 44.8203017604242, N'TOILETTES MIXTES PMR', 5866, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.576707209126519, 44.8265046239696, N'TOILETTES MIXTES PMR', 5867, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.575364148979582, 44.8321829138356, N'TOILETTES MIXTES PMR', 5868, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.562930340486213, 44.8348916982116, N'TOILETTES MIXTES PMR', 5832, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.565009886418641, 44.8343995830601, N'TOILETTES MIXTES PMR', 5831, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.561166641420188, 44.8336657953068, N'TOILETTES MIXTES PMR', 5833, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.558533509304403, 44.8320266626823, N'TOILETTES MIXTES PMR', 5834, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.551315305466965, 44.8241647655615, N'TOILETTES MIXTES PMR', 5835, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.557660463023604, 44.8261417606178, N'TOILETTES MIXTES PMR', 5836, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.559629408676257, 44.8410481005709, N'TOILETTES MIXTES PMR', 5837, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.560001058926109, 44.8398258029106, N'TOILETTES MIXTES PMR', 5838, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.549898050931639, 44.8428114584717, N'TOILETTES MIXTES PMR', 5839, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.565072869268665, 44.845339445957, N'TOILETTES MIXTES PMR', 5840, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.56130163984137, 44.8500481634925, N'TOILETTES MIXTES PMR', 5841, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.578258477685363, 44.8477844235551, N'TOILETTES MIXTES PMR', 5842, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.580998810630376, 44.8500024060006, N'TOILETTES MIXTES PMR', 5843, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.585326967352431, 44.8432468185118, N'TOILETTES MIXTES PMR', 5844, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.599238334808374, 44.8242721021416, N'TOILETTES MIXTES PMR', 5851, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

