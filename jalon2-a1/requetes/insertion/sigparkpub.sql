DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Parkings publics')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Parkings publics'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Parkings publics', N'parking.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.569250881479594, 44.8556448958119, N'vandebrande', 5128, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.614855868755689, 44.8547710128833, N'masson', 5094, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.556653091877665, 44.8442785427267, N'pugs bma', 5096, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.597304397714099, 44.8319727415306, N'ornano', 5097, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.550512772493089, 44.83986389454, N'faure', 5099, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.539444353376732, 44.8535351164913, N'pont st emilion bretelle', 5121, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.55364854689683, 44.8292176137206, N'Paludate', 5101, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.56644932576399, 44.8566926911888, N'poyenne', 5087, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.582623773394466, 44.8592471436853, N'cc europe', 5141, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.555488445353758, 44.8299149980802, N'pont st jean', 5088, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.552795204860593, 44.845190302815, N'rff thiers', 5123, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.544353164577132, 44.8489131933715, N'Galin', 5136, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.560942267130757, 44.840221170098, N'voie sur berge', 5139, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.601912115926412, 44.8195276302054, N'Tauzin', 5135, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.566667946114359, 44.8561431099037, N'gymnase chartrons', 5085, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.575506697569883, 44.8281094931402, N'Leberthon', 5125, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.601931473229441, 44.833725734695, N'daunes doumerc', 5120, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.57179623568148, 44.8514744218252, N'Notre Dame', 5109, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.547815044379679, 44.8484860011197, N'Euromaster', 5118, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.594659546931392, 44.8463873017352, N'Marc Nouaux', 5119, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.568537359667829, 44.8552461436477, N'barreyre', 5115, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.603197093345847, 44.8484761174755, N'grand lebrun', 5117, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.592661972414699, 44.8538202851482, N'nicolas beaujon', 5116, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.575100454789213, 44.8561758361092, N'counord', 5110, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.550945521070261, 44.8448652846859, N'Bonnefin', 5111, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.599788128431395, 44.8191635474594, N'medoquine', 5090, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.544764982598287, 44.8491770001434, N'Galin', 5093, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.545367637191964, 44.8249194859431, N'Brienne', 5133, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.558993186438901, 44.8386852121266, N'voie sur berge', 5131, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.548253825339322, 44.8664060536278, N'achard', 5092, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.548573992311179, 44.8490093004633, N'mayaudon', 5086, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.598372711103963, 44.8483353476145, N'charles de gaulle crama', 5091, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.577734387753918, 44.8251731812584, N'Hopital des Enfants', 5103, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.577575240363581, 44.8431125265322, N'Grands Hommes', 5151, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.556043254270543, 44.8455083585104, N'ex salle de la rotonde/hortense', 5152, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.538760565097572, 44.8535524141326, N'pont st emilion dessous', 5138, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.579901412938348, 44.8423236831122, N'Auditorium', 20171, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.567741222966319, 44.8304053932397, N'Capucins', 5150, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.572372527017383, 44.8352616971991, N'Victor Hugo', 5124, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.568466956070667, 44.8376611155064, N'Alsace-Lorraine', 5149, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.572100503802415, 44.8470473384591, N'Allées de Chartres', 5153, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.574680218424688, 44.8279031463063, N'argonne', 5143, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.555508266074428, 44.8438362639098, N'abadie', 5127, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata, Num_Categorie) VALUES(-0.538719455010557, 44.8528173901711, N'pont st emilion sud', 5122, (SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parkings publics'))

