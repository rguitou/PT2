/*
 * Quelques fonctions utiles pour la génération de code HTML.
 */
#include "chaines.h"
#include "html.h"

using namespace std;

/*
 * Contenu d'un mot-clé CHARGER_JSON_CATEGORIES pour une catégorie donnée.
 */
string chargerJsonCategories(string nomMap, string cheminJson,
							 infoCategorie categorie) {
  return "        " + nomMap + ".data.loadGeoJson('"
	+ cheminJson + categorie.id + ".json');\n";
}

/*
 * Contenu d'un mot-clé LISTE_PAGES_CATEGORIES pour une catégorie donnée,
 * sur la page index.html.
 */
string listePagesCategories(infoCategorie categorie) {
  return "<li><img src=\"images/icones/" + categorie.cheminIcone
  + "\"/> <a href=\"categories-" + categorie.id + ".html\">"
  + categorie.nom + "</a>\n";
}

string listePagesVilles(infoVilles ville) {
  string idVille = int2string(ville.id);
  return "<li> <a href=\"villes-" + idVille + ".html\">"
  + ville.nom + "</a>\n";
}

string prendreNomVilles(infoVilles ville) {
	return "<h2>" + ville.nom + "</h2>";
}
/*
 * Contenu d'une ligne décrivant un point, sur la page d'une catégorie.
 */
string pointCategorie(string x, string y, string texte) {
  return "      <tr><td>" + x + "</td><td>" + y + "</td><td>" + texte
	+ "</td></tr>\n";
}

/*
 * Renvoie le nom d'une carte (map) pour une ville donnée.
 */
string nomDivMap(const infoVilles &ville) {
  return "map-" + int2string(ville.id);
}

/*
 * Contenu d'un mot-clé SECTIONS_VILLES de villes.html,
 * pour une ville donnée.
 */
string sectionville(const infoVilles &ville) {
  string section = "\n<h2>" + ville.nom + "</h2>\n";
  section += "<div id=\"" + nomDivMap(ville)
	+ "\" class=\"map-canvas\"></div>\n";
  return section;
}

/*
 * Nom de la variable javascript associée à la map d'une ville.
 */
string nomVarMap(infoVilles ville) {
  return "map_" + int2string(ville.id);
}

/*
 * Contenu d'un mot-clé DECLARER_MAPS_VILLES_CATEGORIES de villes.html,
 * pour une ville donnée.
 */
string declarerMapville(const infoVilles &ville) {
  return "      var " + nomVarMap(ville) + ";\n";
}

/*
 * Contenu d'un mot-clé CHARGER_JSON_VILLES_CATEGORIES de villes.html,
 * pour une ville donnée.
 */ 
string chargerJsonvilleCategories(const infoVilles &ville,
									 const vector<infoCategorie> &categories) {
  string chargerJson = "        " + nomVarMap(ville)
	+ " = new google.maps.Map(document.getElementById('"
    + nomDivMap(ville) + "'), mapOptions);\n";
  for (infoCategorie categorie : categories) {
    if (categorie.colVilles != -1) {
      chargerJson += "        " + nomVarMap(ville)
		+ ".data.loadGeoJson('json/"
        + int2string(ville.id) + "-" + categorie.id + ".json');\n";
	}
  }
  return chargerJson;
}

/*
 * Contenu d'un mot-clé SETSTYLE_MAPS_villes de villes.html,
 * pour une ville donnée.
 */ 
string setStyleMapVille(const infoVilles &ville) {
  string indent = "        ";
  string setStyleMap = indent +
    nomVarMap(ville) + ".data.setStyle(setColorStyleFn);\n";
  setStyleMap += indent + nomVarMap(ville)
	+ ".data.addListener('mouseover', function(event) {\n";
  setStyleMap += indent
	+ "  infowindow.setContent(event.feature.getProperty('description'));\n";
  setStyleMap += indent + "  infowindow.setPosition(event.latLng);\n";
  setStyleMap += indent +
	"  infowindow.setOptions({pixelOffset: new google.maps.Size(0,-34)});\n";
  setStyleMap += indent + "  infowindow.open(" + nomVarMap(ville) + ");\n";
  setStyleMap += indent + "});\n";
  setStyleMap += indent + "google.maps.event.addListener("
    + nomVarMap(ville) + ", 'click', function() {\n";
  setStyleMap += indent
	+ "  infowindow.close(); // un clic sur la carte ferme les infowindows\n";
  setStyleMap += indent + "});\n";
  return setStyleMap;
}
