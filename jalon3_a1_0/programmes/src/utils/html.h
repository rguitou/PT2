#ifndef HTML_H
#define HTML_H

#include <string>
#include "csv.h"

std::string chargerJsonCategories(std::string nomMap, std::string cheminJson,
								  infoCategorie categorie);

std::string listePagesCategories(infoCategorie categorie);

std::string listePagesVilles(infoVilles ville);

std::string prendreNomVilles(infoVilles ville);

std::string pointCategorie(std::string x, std::string y, std::string texte);

std::string sectionville(const infoVilles &ville);

std::string declarerMapville(const infoVilles &ville);

std::string chargerJsonvilleCategories(const infoVilles &ville,
										  const std::vector<infoCategorie> &categories);

std::string setStyleMapVille(const infoVilles &ville);
#endif // HTML_H
