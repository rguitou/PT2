DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Equipements socioculturels de proximité')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Equipements socioculturels de proximité'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Equipements socioculturels de proximité', N'theater.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6142441507, 1.47465189762, N'CENTRE D''ANIMATION SOUPETARD', 63, "{""type"": ""Point"", ""coordinates"": [1.474651897622263, 43,61424415069264]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6106686913, 1.44146279301, N'MAISON DE QUARTIER ESCOUSSIERES ARNAUD-BERNARD', 3, "{""type"": ""Point"", ""coordinates"": [1.441462793011977, 43,61066869126709]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5871492541, 1.41657501108, N'MAISON DE QUARTIER FONTAINE-LESTANG', 59, "{""type"": ""Point"", ""coordinates"": [1.416575011077641, 43,58714925405944]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5856184022, 1.46593002825, N'MJC PONT DES DEMOISELLES', 0063B, "{""type"": ""Point"", ""coordinates"": [1.465930028252787, 43,58561840217492]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6168524032, 1.40835736016, N'ESPACE JOB', 105, "{""type"": ""Point"", ""coordinates"": [1.408357360161728, 43,616852403168835]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6265640697, 1.46820948725, N'MAISON DE QUARTIER AMOUROUX', 70, "{""type"": ""Point"", ""coordinates"": [1.468209487252963, 43,62656406973787]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6190285178, 1.40100204141, N'MJC ANCELY', 7, "{""type"": ""Point"", ""coordinates"": [1.401002041411882, 43,61902851782482]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5773850259, 1.43865928815, N'MJC EMPALOT', 30, "{""type"": ""Point"", ""coordinates"": [1.438659288147084, 43,57738502590217]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5894738831, 1.47974179746, N'SALLE DES FETES DE LIMAYRAC', 22, "{""type"": ""Point"", ""coordinates"": [1.47974179746311, 43,58947388313155]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.574060423, 1.48511547654, N'CENTRE D''ANIMATION MONTAUDRAN', 3, "{""type"": ""Point"", ""coordinates"": [1.485115476540925, 43,57406042298154]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5961643226, 1.4273901705, N'ESPACE ROGUET - MJC ROGUET', 9, "{""type"": ""Point"", ""coordinates"": [1.427390170500713, 43,59616432258308]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5504020121, 1.45050090606, N'MAISON DE QUARTIER DE POUVOURVILLE', 4, "{""type"": ""Point"", ""coordinates"": [1.450500906064249, 43,5504020120729]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6337730244, 1.43579091023, N'MAISON DE QUARTIER LA VACHE', 6, "{""type"": ""Point"", ""coordinates"": [1.43579091023181, 43,63377302441804]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5732488684, 1.41916418783, N'MJC PREVERT', 292, "{""type"": ""Point"", ""coordinates"": [1.419164187826892, 43,5732488684222]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5641001958, 1.41375556072, N'SALLE LAFOURGUETTE', 28, "{""type"": ""Point"", ""coordinates"": [1.413755560717885, 43,56410019583647]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5837161253, 1.48345175619, N'CENTRE D''ANIMATION LA TERRASSE', 15, "{""type"": ""Point"", ""coordinates"": [1.48345175619005, 43,583716125336764]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6004897787, 1.47405413173, N'MAISON DE QUARTIER ACHIARY', 42, "{""type"": ""Point"", ""coordinates"": [1.474054131731374, 43,60048977872385]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.615074259, 1.39694238593, N'MAISON DE QUARTIER DES ARENES ROMAINES', 107, "{""type"": ""Point"", ""coordinates"": [1.396942385934326, 43,61507425902846]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5704228483, 1.40285653083, N'ATELIER B', 3, "{""type"": ""Point"", ""coordinates"": [1.402856530825981, 43,57042284825562]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5616730565, 1.37899412833, N'CENTRE D''ANIMATION SAINT SIMON', 10, "{""type"": ""Point"", ""coordinates"": [1.378994128328354, 43,561673056454865]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6491252107, 1.45075763876, N'MAISON DE QUARTIER DU GRAND SELVE', TOUT, "{""type"": ""Point"", ""coordinates"": [1.450757638763393, 43,64912521069981]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6467793767, 1.43299037399, N'MAISON DE QUARTIER LALANDE', 1, "{""type"": ""Point"", ""coordinates"": [1.43299037398508, 43,646779376744114]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5882179919, 1.38516320066, N'MAISON DE QUARTIER RUE JEAN D''ALEMBERT', 30, "{""type"": ""Point"", ""coordinates"": [1.385163200658241, 43,58821799187351]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5787967156, 1.44654842749, N'MAISON DES ASSOCIATIONS NIEL', 81, "{""type"": ""Point"", ""coordinates"": [1.44654842749425, 43,578796715641154]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.634709744, 1.46607143611, N'MJC CROIX DAURADE', 141, "{""type"": ""Point"", ""coordinates"": [1.466071436114223, 43,634709744032016]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5748702098, 1.46394643231, N'MAISON DE QUARTIER RANGUEIL', 19, "{""type"": ""Point"", ""coordinates"": [1.463946432305958, 43,57487020982781]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5798864775, 1.40821873348, N'CENTRE D''ANIMATION DE BAGATELLE', 11, "{""type"": ""Point"", ""coordinates"": [1.408218733478232, 43,57988647747551]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6392163308, 1.44290267571, N'CENTRE D''ANIMATION DES CHAMOIS', 11, "{""type"": ""Point"", ""coordinates"": [1.442902675711552, 43,63921633075359]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6482273907, 1.43289118512, N'CENTRE D''ANIMATION LALANDE', 239, "{""type"": ""Point"", ""coordinates"": [1.432891185117108, 43,64822739066429]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5704228483, 1.40285653083, N'CENTRE D''ANIMATION REYNERIE', 3, "{""type"": ""Point"", ""coordinates"": [1.402856530825981, 43,57042284825562]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6023638146, 1.46736738344, N'CERCLE LAIQUE JEAN CHAUBET', 7, "{""type"": ""Point"", ""coordinates"": [1.467367383439308, 43,60236381460414]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6364933118, 1.46276675958, N'MAISON DE QUARTIER CROIX DAURADE', 7, "{""type"": ""Point"", ""coordinates"": [1.462766759581141, 43,63649331181133]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6003781331, 1.48404120918, N'MAISON DE QUARTIER DE L''HERS', 56, "{""type"": ""Point"", ""coordinates"": [1.484041209179636, 43,60037813309684]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5789529585, 1.38377359832, N'MAISON DE QUARTIER DES PRADETTES', TOUT, "{""type"": ""Point"", ""coordinates"": [1.383773598322587, 43,5789529584834]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6144594887, 1.44888696025, N'MAISON DE QUARTIER MATABIAU', 7, "{""type"": ""Point"", ""coordinates"": [1.448886960250047, 43,61445948873793]}",(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements socioculturels de proximité'))

