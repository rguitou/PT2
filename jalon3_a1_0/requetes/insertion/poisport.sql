DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Lieux de sports et loisirs')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Lieux de sports et loisirs'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Lieux de sports et loisirs', N'tennis.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572510250919828, 44.8356247139743, N'Palais des sports', 1058, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565228842375302, 44.8991483516204, N'Vélodrome du Lac', 663, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.564212890487399, 44.8495509435277, N'Port Bastide', 664, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.579207833807522, 44.8126432609108, N'Stade Suzon', 665, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.617733049677768, 44.8705736302139, N'Stade Sainte Germaine', 666, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.604995731906728, 44.8402006751453, N'Salle d''escrime Guy Laupies', 667, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.558825386658389, 44.8381332601763, N'Ponton Benauge', 669, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.562885494412952, 44.82711702435, N'Gymnase Barbey', 670, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.550513724698587, 44.8738632107121, N'Stade Charles Martin', 671, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.58279547512431, 44.8574474888051, N'Stade Grand Parc III', 672, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.578050826297592, 44.8782685179843, N'Plage du Lac', 673, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571179150881065, 44.8330948445545, N'Gymnase Montaigne', 674, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.545137758456445, 44.8426171726457, N'Gymnase Jean Dauguet', 676, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.613115755433654, 44.829313230855, N'Stade Maître Jean', 677, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.622237276417572, 44.8573018862327, N'Piscine Stéhélin', 678, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.585472517074729, 44.8489257610508, N'Gymnase Malleret', 679, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.547146995342712, 44.8389259855432, N'Stade Tregey', 681, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.570636708789208, 44.8667501731575, N'Stade Alfred Daney', 683, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569212677975406, 44.8732600864609, N'City Stade Les Aubiers', 684, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567145870225419, 44.8556003562104, N'Gymnase des Chartrons', 686, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.606505910855077, 44.8420442167075, N'Stade Bel Air', 687, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.598243019156834, 44.8292065328448, N'Stade Chaban Delmas', 688, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.590893501625303, 44.8401353131646, N'Piscine Judaïque', 690, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56582173464136, 44.8989568250583, N'Stadium', 691, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581253917421449, 44.8803406744634, N'Centre de Voile', 692, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.540232486079121, 44.8477824492504, N'Stade Galin', 695, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552792502324479, 44.9023177068685, N'Piste d''accélération moto', 696, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.561101175742673, 44.8475489221135, N'City stade Reignier', 697, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56615132354965, 44.8307041436105, N'Salle Envol d''Aquitaine', 698, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.549478174899475, 44.8182149746207, N'City Stade Carle Vernet', 699, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.561585573031, 44.9092252844276, N'Golf de Bordeaux-Lac', 700, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.66903244693494, 44.8775239598023, N'Centre international de football', 702, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567184023902624, 44.8339166804827, N'Gymnase de La flèche', 703, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.546753957924305, 44.8790263534812, N'Piscine Georges Tissot', 704, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.58039908665732, 44.8954535048303, N'Camping de Bordeaux-Lac', 707, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.611102109833577, 44.8316657575248, N'Gymnase des Peupliers', 708, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.622110007648959, 44.8563045479113, N'Stade Stéhélin', 709, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.568584069959395, 44.8720026547735, N'Terrain de bi-cross de Bordeaux-Lac', 710, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.59858197664607, 44.8275272092912, N'Stade Municipal Chaban Delmas (annexe)', 711, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577565875882837, 44.8337635587072, N'Gymnase Coqs Rouges', 712, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.586569743182503, 44.889904238183, N'Centre nautique de Bordeaux lac', 713, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.588137770509466, 44.8371615008049, N'Bowling Mériadeck', 715, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573530855404772, 44.8228526329869, N'Gymnase Nelson Paillou', 716, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.54115170049989, 44.8789316912187, N'City Stade Port de la Lune', 717, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.579930826007511, 44.8573587480223, N'Piscine du Grand Parc', 718, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.564183798914197, 44.8484531333788, N'City Stade Parc des Angéliques', 719, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.55142723074177, 44.8223353720552, N'City Stade Belcier', 720, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.590982911237879, 44.8472376374177, N'Gymnase Wustenberg', 721, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.604741720554191, 44.843205670457, N'Villa Primrose', 722, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.587989597909013, 44.8350491760499, N'Patinoire', 726, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.548887103977743, 44.8704270510828, N'Gymnase Buscaillet', 637, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.547726944755416, 44.8628439247483, N'Halte Nautique', 638, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.625882257105875, 44.8495749066662, N'Gymnase ASPTT', 639, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.601846325116252, 44.8204322700135, N'City Stade Le Tauzin', 640, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.615965414026424, 44.8511769221244, N'Gymnase La Pergola', 641, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.545724347403076, 44.8452768532014, N'City Stade Benauge', 642, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.575502765919592, 44.8172692202088, N'Stade Brun', 643, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571688357430244, 44.8985619724212, N'Antennes Sportives', 644, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.590312686568708, 44.8393880489661, N'Stade Chauffour', 645, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56686681722442, 44.8390031722564, N'Ponton d''honneur de Bordeaux', 646, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571994603772505, 44.8556046026623, N'Salle Gouffrand', 647, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.578405496440872, 44.8582695453786, N'Stade Grand Parc I', 648, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.560768118075001, 44.8603514278249, N'Gymnase Dupaty', 650, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563280829939238, 44.8552651013699, N'Ponton des Chartrons', 651, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.582172235751719, 44.8617043140666, N'Stade Grand Parc II', 653, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552754358612722, 44.8393780620273, N'Stade Promis', 654, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.540031162008047, 44.8471294196081, N'Piscine Galin', 655, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571432406191968, 44.8529703534435, N'Gymnase l''Aiglon', 656, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.550984521706759, 44.8461991557357, N'Gymnase Thiers', 657, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563646094074487, 44.8421995306056, N'Ponton Yves Parlier', 658, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.625904519121315, 44.8430622460628, N'Stade André Maginot', 659, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573675770165408, 44.8751558959627, N'City Stade Le Lauzun', 660, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.632265586071185, 44.8538392857537, N'Stade Monséjour', 661, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56565680552764, 44.8539088788621, N'Skate Parc', 737, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.610776988095544, 44.83196856031, N'Maison des 5 sens', 966, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565675679720587, 44.8457215649623, N'Embarcadère Montesquieu', 1024, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.562467970266504, 44.8411766689405, N'Ponton Burdigala', 1025, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565196560464287, 44.8538462573048, N'Embarcadères Albert Londres, La Fayette et Thomas Jefferson ', 1029, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.626726750618714, 44.8488965654995, N'Complexe sportif Virginia (gymnase Virginia et stade Henri Lequesne)', 977, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.570892735293164, 44.8982050822315, N'Plaine des sports Colette Besson', 980, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.623288696912586, 44.8574741524382, N'Skatepark Caudéran', 988, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563184485183986, 44.8976657503638, N'Nouveau Stade', 992, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.549264442008576, 44.8617328292234, N'PONTON DE LA CITE DU VIN', 1054, sports loisirs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

