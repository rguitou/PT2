DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Toilettes publiques')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Toilettes publiques'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Toilettes publiques', N'toilets.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2967550262871, -1.39247410410928, N'Cellier', SP_NM_12_001, Cellier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2349401485852, -1.50276859414071, N'Vieux Doulon', SP_NM_13_002, Vieux Doulon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2343938752821, -1.53285244350403, N'Marrière', SP_C_13_033, Marrière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2274986071001, -1.50877933912959, N'Grand Blottereau', SP_C_13_002, Grand Blottereau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2962282602878, -1.49139177009538, N'Place du marché', SP_C_05_001, Place du marché,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2965300571601, -1.4934256482368, N'Place Le Corvec', SP_NM_05_001, Place Le Corvec,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2625645513395, -1.53076311416444, N'Beaujoire (maison de la Rose)', SP_C_13_003, Beaujoire (maison de la Rose),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2618953206344, -1.53460443859609, N'Beaujoire (vallon)', SP_C_13_004, Beaujoire (vallon),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2880421820774, -1.52274532606656, N'Chantrerie', SP_C_13_005, Chantrerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2353479918952, -1.53555299703359, N'Plessis Tison', SP_C_13_006, Plessis Tison,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2992124794165, -1.55005782168864, N'Eglise', SP_NM_08_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2625792328604, -1.6727998481161, N'Gendarmerie', SP_NM_22_001, Gendarmerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2705688535784, -1.62319175906487, N'Jeanne dArc', SP_C_14_001, Jeanne dArc,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2532769221593, -1.57648352562549, N'Bout des Pavés', SP_NM_14_001, Bout des Pavés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2526364561662, -1.59631296898079, N'Petit Chantilly', SP_C_14_002, Petit Chantilly,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2434041846086, -1.56934660915782, N'Vallée du Cens (Grotte)', SP_C_13_007, Vallée du Cens (Grotte),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2535618522742, -1.56035224228016, N'Bourgeonnière', SP_NM_13_004, Bourgeonnière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2424383596741, -1.56701446155703, N'Vallée du Cens (camping)', SP_C_13_008, Vallée du Cens (camping),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2143452022477, -1.68235350185828, N'Place des cités', SP_NM_06_001, Place des cités,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2156318890403, -1.72479448617054, N'Epinettes', SP_NM_06_002, Epinettes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2111589065519, -1.72749549896159, N'Charles De Gaulle', SP_NM_06_004, Charles De Gaulle,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.193248886718, -1.64709926489558, N'Joseph Tahet', SP_NM_07_001, Joseph Tahet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1977903497235, -1.67376706998005, N'Quai dIndre', SP_C_07_001, Quai dIndre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2219280219297, -1.63927863771579, N'Marcel Paul', SP_NM_17_001, Marcel Paul,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2115704139931, -1.65359198849273, N'Lieutenant Mouillie', SP_NM_17_002, Lieutenant Mouillie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2183976427514, -1.60806749778069, N'Branchoire', SP_C_17_002, Branchoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2135900046552, -1.58689325701635, N'Zola', SP_NM_13_005, Zola,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2067635604654, -1.58792608975112, N'Mairie Chantenay', SP_NM_13_006, Mairie Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1994734703664, -1.5927835731453, N'Civelles', SP_C_13_009, Civelles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2019266280916, -1.57892821431067, N'Garennes', SP_NM_13_007, Garennes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2092763473293, -1.57123120436148, N'René Bouhier', SP_NM_13_008, René Bouhier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2146595270628, -1.57477040504501, N'Canclaux', SP_NM_13_009, Canclaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2238186770592, -1.59808821663716, N'Dervallières', SP_NM_13_010, Dervallières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1994883164344, -1.58917683647408, N'Macé', SP_NM_13_011, Macé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2002988655197, -1.58176945128172, N'Maurice Schwob', SP_C_13_010, Maurice Schwob,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2110607447429, -1.56082664381794, N'Petite Hollande', SP_NM_13_029, Petite Hollande,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2160002423212, -1.56364515123737, N'Square Lebé', SP_NM_13_030, Square Lebé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.219262661263, -1.5587931829209, N'Cassegrain', SP_NM_13_031, Cassegrain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2287894722674, -1.54310426691244, N'Enfants Nantais', SP_NM_13_032, Enfants Nantais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2258589857588, -1.52280834557691, N'Gabriel Trarieux', SP_NM_13_033, Gabriel Trarieux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2128175153783, -1.55254762147452, N'Baco', SP_NM_13_030, Baco,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2152499245917, -1.56133000110399, N'Calvaire', SP_NM_13_031, Calvaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2170975563696, -1.55622963364689, N'Armand Brossard', SP_NM_13_032, Armand Brossard,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2172579438702, -1.55948009692146, N'Bretagne', SP_NM_13_033, Bretagne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2205170959908, -1.54456688020931, N'Jardin des plantes Nord', SP_C_13_011, Jardin des plantes Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2110964960782, -1.56376366502621, N'Cambronne', SP_C_13_012, Cambronne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2127907688621, -1.5647992137733, N'Louis Bureau', SP_C_13_013, Louis Bureau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2269939173847, -1.52710793103935, N'Noë Mitrie', SP_C_13_014, Noë Mitrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2020491752722, -1.54655828098108, N'Vertais', SP_C_13_015, Vertais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2069237282868, -1.53311347965413, N'Jardin des cinq sens', SP_C_13_016, Jardin des cinq sens,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2062333816928, -1.56481030281344, N'Parc des chantiers- Bar de la Branche', SP_NM_13_016, Parc des chantiers- Bar de la Branche,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2052746937977, -1.56466112957655, N'Parc des chantiers - Les Nefs', SP_NM_13_017, Parc des chantiers - Les Nefs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2051557694397, -1.56737509578747, N'Parc des chantiers - Station Prouvé', SP_NM_13_018, Parc des chantiers - Station Prouvé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2188274242079, -1.54098073924425, N'Jardin des plantes Sud', SP_C_13_017, Jardin des plantes Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2288298201608, -1.56619350364038, N'Emile Fritsch', SP_NM_13_019, Emile Fritsch,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.224487913552, -1.57020023210618, N'Cimetière Pelleterie', SP_NM_13_020, Cimetière Pelleterie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2303488158409, -1.58592716957707, N'Louis Feuillade', SP_NM_13_021, Louis Feuillade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2251122614612, -1.56319871188901, N'Capucins', SP_NM_13_022, Capucins,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2274787850822, -1.59138915917936, N'Piscine Dervallières', SP_NM_13_023, Piscine Dervallières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2204112289093, -1.56185137461257, N'Viarme', SP_NM_13_024, Viarme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2327954941687, -1.57674424164196, N'Américains', SP_NM_13_025, Américains,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2224040424727, -1.58197343101717, N'Jardin d''enfants de Procé', SP_C_13_018, Jardin d'enfants de Procé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2235568442375, -1.58330065600502, N'Procé Vallon', SP_C_13_019, Procé Vallon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.223136236766, -1.58136598775862, N'Procé Chateau', SP_C_13_020, Procé Chateau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2411686325569, -1.57710550365362, N'Gaudinière Haut', SP_C_13_021, Gaudinière Haut,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.225270478295, -1.55371716601873, N'Ile de Versailles', SP_C_13_022, Ile de Versailles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2486596406107, -1.52239020890825, N'La Halluchère', SP_NM_13_026, La Halluchère,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2951988547925, -1.39286533092106, N'Eglise', SP_C_12_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.269500798695, -1.51780773830689, N'Tonelliers', SP_NM_13_027, Tonelliers,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2211674028578, -1.5514622432977, N'Parking Tournefort', SP_NM_13_029, Parking Tournefort,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2171778591698, -1.55438473971654, N'Amiral Halgan', SP_C_13_023, Amiral Halgan,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2103577905182, -1.52336922882544, N'Crapa', SP_C_13_024, Crapa,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1908975024982, -1.5297221084211, N'Crapaudine', SP_C_13_025, Crapaudine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2177146718177, -1.56248778376616, N'Faustin Helie', SP_C_13_026, Faustin Helie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2465428179967, -1.57825267295222, N'Gaudinière Bas', SP_C_13_027, Gaudinière Bas,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2266524293868, -1.51227510942117, N'Potager tropical', SP_C_13_028, Potager tropical,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2226964729384, -1.52768342121573, N'Jean Baptiste Barre', SP_C_13_029, Jean Baptiste Barre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2043291674751, -1.59354973264737, N'Jean Le Gigant', SP_C_13_030, Jean Le Gigant,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2006574903376, -1.5839366442963, N'Oblates', SP_C_13_031, Oblates,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2252713684107, -1.55369078770185, N'Ile Versailles Capitainerie', SP_C_13_032, Ile Versailles Capitainerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.196458125064, -1.54114446382891, N'Pirmil', SP_NM_13_028, Pirmil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2094437080825, -1.61282701891088, N'Denis Forestier', SP_C_17_003, Denis Forestier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1413567879381, -1.68881374199635, N'Marché', SP_C_02_001, Marché,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1445322159992, -1.69329004055767, N'Eglise', SP_NM_02_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1349131478556, -1.73293335372255, N'Eglise', SP_NM_19_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.189950589369, -1.68370684991348, N'Eglise', SP_NM_09_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1878812533845, -1.68337966299872, N'Similien Guérin', SP_NM_09_002, Similien Guérin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1957187825113, -1.72496219728354, N'Mairie', SP_NM_18_001, Mairie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2061867666696, -1.78556903413688, N'Canal', SP_NM_10_001, Canal,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2030285566745, -1.7551368848215, N'LHerminier', SP_NM_10_002, LHerminier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2012451258297, -1.75513273852254, N'Halles', SP_C_10_001, Halles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.169606662585, -1.72236087412763, N'Mairie', SP_C_04_001, Mairie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1686295224922, -1.72043759541334, N'Eglise', SP_C_04_002, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1807661934522, -1.58854385914105, N'Freinet', SP_NM_03_001, Freinet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1790655037078, -1.62375737387148, N'Eglise', SP_NM_03_002, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1769998188308, -1.59307617084829, N'Neustrie', SP_NM_03_003, Neustrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1842123665492, -1.65444662050528, N'Roche Ballue', SP_C_03_001, Roche Ballue,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1853105827462, -1.5870802206819, N'Leclerc', SP_NM_03_004, Leclerc,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1677644264054, -1.62288200246808, N'Les jardins familiaux', SP_C_03_003, Les jardins familiaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.177349653857, -1.62937242933986, N'Champ Toury', SP_C_03_002, Champ Toury,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1942159314792, -1.5287989913016, N'Joliot curie', SP_NM_13_001, Joliot curie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1846939937215, -1.54759831529334, N'Eglise', SP_C_15_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1848696885941, -1.562394809477, N'Pays de Retz', SP_C_15_002, Pays de Retz,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1891001039323, -1.55155146434883, N'08-mai', SP_C_15_003, 08-mai,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1835502124466, -1.55262697722405, N'Balinière', SP_C_15_004, Balinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1636165228539, -1.53177824945554, N'La Robinière', SP_C_15_005, La Robinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1905078077965, -1.5650322849317, N'Cimetière St Pierre', SP_C_15_006, Cimetière St Pierre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1847104484583, -1.54352670741723, N'Cimetière St Paul', SP_C_15_007, Cimetière St Paul,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1830442310594, -1.53188936779172, N'Parc de la Morinière', SP_C_15_008, Parc de la Morinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1915184921957, -1.56952629261983, N'Hôtel de Ville', SP_C_15_009, Hôtel de Ville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1877183941908, -1.53754770038832, N'Parc Chêne Gala', SP_C_15_010, Parc Chêne Gala,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1647178098502, -1.53204691980543, N'Stade Robinière', SP_C_15_011, Stade Robinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1717283803808, -1.55831922900723, N'Cimetière Classerie', SP_C_15_012, Cimetière Classerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1817949613664, -1.57022297073413, N'Stade Léo lagrange', SP_C_15_013, Stade Léo lagrange,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074361214992, -1.50514404023303, N'Poste', SP_NM_20_001, Poste,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2084138753702, -1.50364140031247, N'Eglise', SP_NM_20_002, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1935509230788, -1.51773095275049, N'Liberté', SP_NM_20_003, Liberté,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2150160899479, -1.46531932254415, N'Eglise', SP_NM_01_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2173721110801, -1.46597579162715, N'Salle Zodiaque', SP_C_01_001, Salle Zodiaque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.148315904221, -1.53045143123094, N'Face Mairie', SP_NM_11_001, Face Mairie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1652324526394, -1.4839848939869, N'Garnier', SP_NM_24_001, Garnier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1582216246082, -1.47240648550308, N'Pierre Percée', SP_C_24_001, Pierre Percée,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1709208087693, -1.51088513669327, N'Cale Beautour', SP_C_24_002, Cale Beautour,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1678551158433, -1.47992234389478, N'Capitainerie', SP_C_24_003, Capitainerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1638439165467, -1.47206679265198, N'Eglise', SP_NM_24_002, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1493884345102, -1.46825355710177, N'Portillon', SP_NM_24_003, Portillon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2492980554648, -1.48672336617472, N'11-nov', SP_NM_21_001, 11-nov,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2498794405798, -1.48385499949354, N'Marché', SP_NM_21_002, Marché,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2443251085474, -1.46086918880616, N'Ile Clémentine', SP_C_21_001, Ile Clémentine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2445237680788, -1.47450553357271, N'Plessis', SP_C_21_002, Plessis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2673084420546, -1.44085570593544, N'Eglise', SP_NM_23_001, Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2662951813781, -1.43824383903739, N'Cimetière communautaire', SP_NM_23_002, Cimetière communautaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Toilettes publiques'))

