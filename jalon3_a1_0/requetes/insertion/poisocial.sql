DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Etablissements sociaux')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Etablissements sociaux'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Etablissements sociaux', N'group-2.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552446916858348, 44.841616135252, N'Club senior Nuits', 355, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563922243249341, 44.8262833163889, N'Club senior Vilaris', 357, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566379326890896, 44.8318343366927, N'Club senior Saumenude', 358, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.586992686525911, 44.844345465612, N'Club Senior Albert Barraud', 361, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.548064723004695, 44.869210547654, N'Club senior Achard', 363, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.551901439370459, 44.825010818353, N'Club senior Son Tay', 364, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.546232065068248, 44.8726555173856, N'Club senior Lumineuse', 365, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.612676063711337, 44.8241740332456, N'Club senior Alfred Smith', 369, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.551383072949761, 44.8447646204558, N'Club senior Bonnefin', 370, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56292706080889, 44.8628448544629, N'Club senior Chantecrit', 371, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563814967426476, 44.8261943417269, N'Centre médico-scolaire Vilaris', 372, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565048471224225, 44.8218811130216, N'Club senior Buchou', 373, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.601157473427647, 44.8209652944258, N'Maison de quartier Tauzin', 374, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.6198313088522, 44.8497725027188, N'Club senior Gelé de Francony', 375, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572760812252972, 44.8564080694238, N'Club senior Jardin Public', 378, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.590516240713219, 44.8312235810088, N'Club senior Manon Cormier', 379, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.611671910130433, 44.8326748520544, N'Club senior Saint Augustin', 380, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571136067988135, 44.8733107276326, N'Club senior Aubiers', 381, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.568821233432003, 44.8381385503822, N'Club senior Alsace Lorraine', 383, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.614039382107227, 44.8458281614345, N'Club senior Armand Faulat', 384, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.57748148521039, 44.8213719996631, N'Club senior Dubourdieu', 385, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.616808046547178, 44.8498137207421, N'Maison de quartier AGJA', 386, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569373931118597, 44.814425922094, N'Club senior Albert Premier', 387, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.575772473332079, 44.8332143532937, N'Club senior Magendie', 388, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.599640039313539, 44.8236460854157, N'Club senior Quintin', 389, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.564658331345514, 44.8241088975532, N'Club senior Billaudel', 391, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.541085320581932, 44.8507735438305, N'Club senior Reinette', 394, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.55855008270822, 44.8455202723676, N'Club senior Queyries', 395, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576671498960112, 44.8394409319163, N'Centre local d''information et de coordination (CLIC)', 397, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567961534533861, 44.8335189642716, N'Centre médico-social Saint-Michel', 398, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563847743534304, 44.8250679561616, N'Maison de quartier Union Saint Jean', 402, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.549290365234222, 44.8439273144806, N'Centre médico-social Bastide', 403, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.599315893428256, 44.8488363289682, N'Centre médico-scolaire Bel-Air', 404, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.610692375895897, 44.8317425319074, N'Maison de quartier Saint Augustin', 965, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.55249803127328, 44.8417311952662, N'Centre médico-scolaire Nuits', 407, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.591154228172865, 44.8387690237122, N'Maison de quartier Union Saint Bruno', 408, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572498557225671, 44.855555912368, N'Centre médico-scolaire Gouffrand', 409, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.564792405365424, 44.8584327636791, N'Maison de quartier US Chartrons', 410, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.583246769506484, 44.8456683126775, N'Centre médico-social Bordeaux Centre', 414, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.562898108581132, 44.845495901338, N'Maison de quartier de La Bastide', 415, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.559296291934806, 44.8287208111121, N'Centre médico-social Saint-Jean', 416, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572766583542513, 44.8731707850223, N'Centre médico-social Bordeaux-Lac', 417, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.619661904458151, 44.8554407430415, N'Centre médico-social Caudéran', 423, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.57665847597867, 44.8539294693791, N'Maison de quartier Chantecler', 424, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576671470650194, 44.8552077743152, N'Centre médico-social Grand Parc', 426, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.57091009968541, 44.8794056552081, N'Maison polyvalente Sarah Bernhardt', 951, santé social,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements sociaux'))

