DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Etablissements petite enfance')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Etablissements petite enfance'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Etablissements petite enfance', N'daycare.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.613455391671063, 44.846333450757, N'Multi accueil Armand Faulat 1', 172, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.543447701590002, 44.8795228781871, N'Multi accueil Claveau', 173, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.562757035634496, 44.8310835971023, N'Le Jardin de l''Eau Vive', 175, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.575184079991564, 44.8334543745651, N'Multi accueil Magendie', 176, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.609184789028589, 44.8238496375736, N'Multi accueil Carreire', 177, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581357989965014, 44.8412947664553, N'Multi accueil Pitchoun Gambetta', 178, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.605044102677771, 44.8499784370585, N'Multi accueil Pitchoun Caudéran', 179, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.597625407422127, 44.8315273908437, N'Multi accueil Ornano', 180, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576382185307366, 44.8331180443766, N'LAEP APEEF -  La Maison des enfants', 182, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576506205406451, 44.8331283367483, N'Multi accueil APEEF Maison des enfants', 183, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552134329415273, 44.8415421836205, N'Multi accueil La maison soleil', 184, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563568819937014, 44.8299103691226, N'Multi accueil des Douves 2', 185, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.589183257871198, 44.8374032329169, N'La parentèle', 186, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.583477809196919, 44.8589260886981, N'Multi accueil Grand Parc 2', 222, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.586867450948258, 44.8444152648642, N'Relais Assistance Maternelle Bordeaux centre (RAM)', 225, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573263561362849, 44.8747610659111, N'Multi accueil Arc en ciel', 226, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573483717789341, 44.8641663722355, N'Multi accueil Haussmann', 227, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566661075573347, 44.8335786365293, N'Multi accueil Gaspard Philippe', 228, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56131739057073, 44.8450132621432, N'Relais Assistante Maternelle Bordeaux Bastide (RAM)', 229, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.602141313129211, 44.8335107800426, N'Multi accueil Coucou', 230, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.593738481581731, 44.8299691148406, N'Multi accueil Manon Cormier', 231, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.635514742954322, 44.8601564583436, N'Multi accueil La Coccinelle', 232, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.585127111957575, 44.8310693941893, N'Multi accueil - L''escale des bambins', 233, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569677331412132, 44.8639715708016, N'Multi accueil Nuage bleu', 234, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.613462140764023, 44.8462162184756, N'Multi accueil Armand Faulat 2', 235, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573045097250744, 44.8744424022175, N'Crèche familiale Bordeaux Nord', 236, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.570311761332685, 44.8395649512801, N'Multi accueil Argentiers', 238, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.553665122476894, 44.8246116053298, N'Multi accueil Canaillous Lafitteau', 187, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.61349966116373, 44.8464583863268, N'Crèche familiale Caudéran', 189, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571581993668337, 44.8205514852391, N'Multi accueil Malbec-Nansouty', 190, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.586818683333194, 44.8444525157007, N'Multi accueil Albert Barraud', 191, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.543666576233495, 44.8461565181819, N'Multi accueil Benauge 1', 192, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.556578590320932, 44.8149489425187, N'Multi accueil Canaillous-Brascassat', 193, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566965236468354, 44.8215949987944, N'Multi accueil Babilou Mirassou 1', 194, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.605055451161187, 44.8426484100666, N'Multi accueil Cité administrative', 195, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.583774011113766, 44.8590266651569, N'Multi accueil Grand Parc 1', 196, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565873752972249, 44.8623675690834, N'Multi accueil des Chartrons 1', 197, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577146700398578, 44.8369183590054, N'Multi accueil Les petits bouchons', 198, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.584990654949867, 44.8227887151511, N'Multi accueil George V', 199, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574892327907944, 44.8606031076607, N'Relais Assistante Maternelle Bordeaux maritime (RAM)', 200, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.570474291108388, 44.8563451645699, N'Multi accueil Barreyre 1', 201, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.562757035634496, 44.8310835971023, N'Multi accueil - APEEF - le Jardin de l''eau vive', 202, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.591100070907305, 44.8386983582926, N'Multi accueil Bordeaux Saint Bruno', 203, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572186363719207, 44.8558969270766, N'Multi accueil La souris verte', 204, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.608255136014903, 44.8325358138727, N'Multi accueil Saint Augustin', 205, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552229700721131, 44.8419359951469, N'APEEF - Association petite Enfance, Enfance et Famille', 206, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.587588708097428, 44.832249888676, N'Crèche familiale Bordeaux centre', 207, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.553919380514301, 44.8425781111829, N'Multi accueil Pitchoun Bastide - Centre petite enfance', 208, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.58362272926674, 44.8590394885562, N'Crèche familiale Grand Parc', 209, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.563258825655542, 44.8297830767913, N'Multi accueil des Douves 1', 211, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.613653573190814, 44.8462473539588, N'Antenne du Relais Assistante Maternelle', 212, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.584749233548152, 44.8381181158876, N'Multi accueil Babilou - ile aux oiseaux', 213, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.628582156862608, 44.8443527930635, N'Multi accueil Quai des Bambins', 214, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566965236468354, 44.8215949987944, N'Multi accueil Babilou Mirassou 2', 215, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552120801215176, 44.8415245127238, N'LAEP APEEF - La Maison Soleil', 216, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.588958224263167, 44.8462353389024, N'Multi accueil Ptit Bout''chou', 217, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.583089003073932, 44.8252499853775, N'Multi accueil Villa Pia', 218, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.586815767667372, 44.8522504985567, N'Multi accueil Pitchoun les 4 saisons', 219, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.543673907517593, 44.8460482833387, N'Multi accueil Benauge 2', 220, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.59439674249826, 44.8503402392997, N'Multi accueil les calins d''orme', 973, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.593525160188627, 44.8423349396576, N'Micro crèche Alema George Mendel', 928, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.556367766802609, 44.8460002975067, N'Multi accueil Le jardin d''Hortense', 929, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569703556407657, 44.8375129491792, N'Multi accueil Sainte Colombe 1', 930, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.55070032614895, 44.8225096339734, N'Multi accueil Canaillous Armagnac', 932, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.575499313453596, 44.8258670818047, N'Multi accueil Alema les Sablières', 933, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566148238091259, 44.8621639635694, N'Multi accueil Les chartrons 2', 934, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.549443249540262, 44.8180371550079, N'Multi accueil Carle Vernet 2', 935, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.616063544365905, 44.8414222465629, N'Multi accueil Lucilann', 959, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.593889936783258, 44.8231817773015, N'Multi accueil Pain d''épices', 955, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.628827094469124, 44.8648456754203, N'Micro crèche Lucilann', 956, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.551024244030592, 44.8233044375224, N'La maison de Nolan', 957, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.561022932129432, 44.8448736041949, N'Ludothèque Interlude Bastide', 958, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574121565737777, 44.8484611272117, N'Multi accueil MSA Brin d''éveil', 960, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.57231197927043, 44.8806988701545, N'Multi accueil La Berge du lac 1', 970, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569920784601189, 44.8375228405222, N'Multi accueil Sainte Colombe 2', 971, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572088091749046, 44.8806318925711, N'Multi accueil La berge du lac 2', 969, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.570415035910711, 44.856301521407, N'Multi accueil Barreyre 2', 964, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.549588947997624, 44.8180200885227, N'Multi accueil Carle Vernet 1', 972, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574892606357527, 44.8366889471013, N'Multi accueil Bordeaux Alema Paul Bert', 974, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.578996171887966, 44.8241034892354, N'Multi accueil Jean Marquaux', 975, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.60998653255945, 44.8412007099433, N'Multi accueil Détrois - D3 La maison bleue', 997, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.601185617372688, 44.8224083739081, N'Multi accueil Charles Perrens', 998, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574273080556969, 44.8745469897574, N'LAEP - L''Oasis', 1042, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574652050704232, 44.8606267039463, N'LAEP - A petit pas - APPE-AGEP', 1043, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.575156812633515, 44.8602124631893, N'Centre Papillon', 1044, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.615157177380349, 44.8554541259071, N'LAEP APEEF', 1045, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.560861795698446, 44.8437606041623, N'Multi accueil - Tourni''quai', 1062, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.588491760878992, 44.8538079732665, N'Micro creche Zazzen Rivière', 1063, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56809984169365, 44.8371313453755, N'Multi accueil Cadet Rousselle', 1064, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.560861795698446, 44.8437606041623, N'La Bastide  Micro crèche Bilbo''Quai', 1065, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.562714839419228, 44.8624194804001, N'Ludothèque Interlude Chantecrit', 979, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.570929271942746, 44.8793786346908, N'Ludothèque Interlude du lac', 981, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.593845442628924, 44.8462031638805, N'Les enfants d''Osiris', 982, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.58953220155924, 44.846425019479, N'Multi accueil Mille couleurs', 987, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.59700693780716, 44.8289887608561, N'Multi accueil Docteur Christiane Larralde', 1021, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.588643223511675, 44.8333312486276, N'Multi accueil Éden Art ', 1088, petite enfance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements petite enfance'))

