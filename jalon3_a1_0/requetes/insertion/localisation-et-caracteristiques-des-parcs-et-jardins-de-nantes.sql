DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Parcs et jardins')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Parcs et jardins'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Parcs et jardins', N'tree.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2276, -1.50934, N'Parc du grand-blottereau', 170, Parc du grand-blottereau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2023, -1.55138, N'Square benoni goulin', 173, Square benoni goulin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2053, -1.53153, N'Square rue jules sebilleau', 174, Square rue jules sebilleau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2123, -1.5685, N'Parc say', 176, Parc say,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2078, -1.59202, N'Square de la marseillaise', 177, Square de la marseillaise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2081, -1.60983, N'Square des marthyrs irlandais', 178, Square des marthyrs irlandais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2049, -1.61028, N'Square du bois de la musse', 179, Square du bois de la musse,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2015, -1.59801, N'Parc de la boucardiere', 180, Parc de la boucardiere,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2351, -1.57141, N'Square gaston michel', 182, Square gaston michel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2545, -1.57074, N'Jeux de boules proust bergson', 183, Jeux de boules proust bergson,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2476, -1.56688, N'Parc du petit port', 187, Parc du petit port,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2114, -1.52053, N'Parc de beaulieu', 211, Parc de beaulieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2111, -1.56282, N'Square gabriel chéreau', 347, Square gabriel chéreau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2008, -1.59307, N'Square des courtils', 382, Square des courtils,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2709, -1.58538, N'Cimetiere paysager', 433, Cimetiere paysager,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2183, -1.54513, N'Square saint françois', 507, Square saint françois,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2659, -1.51974, N'Jardin des quatre jeudis', 1074, Jardin des quatre jeudis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.267, -1.519, N'Jardin du nadir', 1075, Jardin du nadir,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2689, -1.51677, N'Jardin du zenith', 1076, Jardin du zenith,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2669, -1.52127, N'Jardin des farfadets', 1077, Jardin des farfadets,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2875, -1.52446, N'Parc de la chantrerie', 1078, Parc de la chantrerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.221554, -1.533176, N'Parc de la moutonnerie', 1391, Parc de la moutonnerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2569, -1.52301, N'Abords du stade de la beaujoire', 1182, Abords du stade de la beaujoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1829, -1.51965, N'Square du vieux parc de sèvre', 1228, Square du vieux parc de sèvre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2059, -1.56715, N'Le parc des chantiers', 1237, Le parc des chantiers,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2452, -1.57952, N'Parc de la gaudiniere', 1324, Parc de la gaudiniere,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2544, -1.52001, N'Square de la halvèque', 1330, Square de la halvèque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.215, -1.54468, N'Square jean heurtin', 1342, Square jean heurtin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1909, -1.53078, N'Parc potager de la crapaudine', 1389, Parc potager de la crapaudine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2086, -1.5274, N'Place de la guirouée', 1471, Place de la guirouée,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2662, -1.52304, N'Square barbara', 1475, Square barbara,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2121, -1.54823, N'Square du lait de mai', 1479, Square du lait de mai,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2463, -1.51608, N'Square des maraiches', 1532, Square des maraiches,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2306, -1.58528, N'Square louis feuillade', 1536, Square louis feuillade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2077, -1.55964, N'Jardin mabon', 1580, Jardin mabon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2381, -1.58255, N'Jardin des nectars', 1588, Jardin des nectars,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2057, -1.54489, N'Le jardin des fonderies', 1817, Le jardin des fonderies,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2578, -1.55923, N'Aire de jeux des renards', 214, Aire de jeux des renards,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2453, -1.54166, N'Espace de jeux de belle ile', 1521, Espace de jeux de belle ile,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2149, -1.53688, N'Parc pablo picasso', 1846, Parc pablo picasso,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.218, -1.55036, N'Square psalette', 519, Square psalette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2163, -1.54916, N'Douves du château des ducs', 518, Douves du château des ducs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2288, -1.56601, N'Square saint pasquier', 1507, Square saint pasquier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1969, -1.5448, N'Jardin confluent', 29, Jardin confluent,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2311, -1.55332, N'Square félix thomas', 1599, Square félix thomas,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2355, -1.52541, N'Parc potager croissant', 1297, Parc potager croissant,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2038, -1.54795, N'Place wattignies', 1060, Place wattignies,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.245, -1.52471, N'Square pilotière', 1638, Square pilotière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2631, -1.57095, N'Parc potager amande', 1622, Parc potager amande,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2164, -1.52407, N'Square hongrie', 1725, Square hongrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2125, -1.59051, N'Parc potager fournillère', 1091, Parc potager fournillère,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2078, -1.58038, N'Square zac pilleux nord', 1642, Square zac pilleux nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2003965, -1.5850374, N'Parc des oblates', 1937, Parc des oblates,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2248, -1.59412, N'Parc des dervallières', 34, Parc des dervallières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2222, -1.58153, N'Jardin d''enfants de procé', 35, Jardin d'enfants de procé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2243, -1.58156, N'Parc de proce', 36, Parc de proce,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2175, -1.55443, N'Square amiral halgand', 115, Square amiral halgand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2198, -1.54292, N'Jardin des plantes', 116, Jardin des plantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212, -1.56308, N'Cours cambrone', 117, Cours cambrone,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2129, -1.56489, N'Square louis bureau', 118, Square louis bureau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.226, -1.55438, N'Square ile de versailles', 120, Square ile de versailles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2221, -1.55188, N'Square maquis de saffré', 121, Square maquis de saffré,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212, -1.55775, N'Square jean-batiste daviais', 123, Square jean-batiste daviais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2075, -1.53367, N'Jardin des cinq sens', 124, Jardin des cinq sens,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2027, -1.60834, N'Square charles housset', 126, Square charles housset,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2179, -1.56347, N'Square faustin hélie', 127, Square faustin hélie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2629, -1.53242, N'Parc de la beaujoire', 128, Parc de la beaujoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.235, -1.53608, N'Parc du plessis tison', 130, Parc du plessis tison,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2011, -1.58072, N'Square maurice schwob', 131, Square maurice schwob,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2142, -1.55037, N'Square elisa mercoeur', 134, Square elisa mercoeur,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2039, -1.53609, N'Square marcel launay', 135, Square marcel launay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2042, -1.59294, N'Square jean le gigant', 136, Square jean le gigant,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.195, -1.61559, N'Square toussaint louverture', 138, Square toussaint louverture,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2253, -1.56276, N'Parc des capucins', 140, Parc des capucins,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2277, -1.52675, N'Parc de la noe mitrie', 147, Parc de la noe mitrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2226, -1.52753, N'Square jean-baptiste barre', 148, Square jean-baptiste barre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2254, -1.52259, N'Square jules bréchoir', 149, Square jules bréchoir,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2182, -1.51796, N'Parc de malakoff', 150, Parc de malakoff,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2052, -1.54996, N'Square gustave roch', 152, Square gustave roch,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2081, -1.52521, N'Place basse mar', 153, Place basse mar,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2024, -1.54659, N'Square vertais', 154, Square vertais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2122, -1.58708, N'Square du prinquiau', 155, Square du prinquiau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2144, -1.57444, N'Square canclaux', 157, Square canclaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2093, -1.57075, N'Square des combattants d''afrique du nord', 158, Square des combattants d'afrique du nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2144, -1.56766, N'Square de l''edit de nantes', 159, Square de l'edit de nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2022, -1.57732, N'Square marcel moisan', 162, Square marcel moisan,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2162, -1.56342, N'Square pascal lebee', 163, Square pascal lebee,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2192, -1.56382, N'Square etienne destranges', 165, Square etienne destranges,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2452, -1.52404, N'Centre social de la pilotière', 167, Centre social de la pilotière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2431, -1.52293, N'Square augustin fresnel', 168, Square augustin fresnel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2268, -1.52255, N'Parc de broussais', 169, Parc de broussais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Parcs et jardins'))

