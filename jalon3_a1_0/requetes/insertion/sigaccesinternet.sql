DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Points d''accès à Internet')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Points d''accès à Internet'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Points d''accès à Internet', N'radio-station-2.png', '06/05/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.615162608139202, 44.8521232663924, N'Annexe Caudéran', 1008, 1008,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574176061782873, 44.8437230928344, N'Office de tourisme - Quinconce', 1006, 1006,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574260542405202, 44.8435715822149, N'Office de tourisme - Place de la Comédie', 1007, 1007,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.555275779834489, 44.8631813311128, N'Hangar G2', 1009, 1009,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569112670370342, 44.838454308662, N'Alsace', 255, 255,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566015567785078, 44.8342984523836, N'Place Saint Michel', 271, 271,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56945597398709, 44.8500304622651, N'Ibaia', 277, 277,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.588037974686414, 44.835180548989, N'Patinoire Mériadeck', 1013, 1013,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.55827184189155, 44.8443499096176, N'Jardin Botanique 1', 259, 259,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576958961500011, 44.8398436651071, N'ATHENEE MUNICIPAL', 299, 299,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.560036618832484, 44.8312524242836, N'Conservatoire', 321, 321,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.559472483829691, 44.8325362751921, N'Esplanade Sport1', 323, 323,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.601869931416811, 44.8204892098459, N'Bibliothèque Tauzin', 246, 246,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.58667256654969, 44.8352894031204, N'Bibliothèque Mériadeck', 248, 248,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581533439273732, 44.8589432763187, N'Mairie annexe Grand Parc', 249, 249,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572938002888903, 44.8737352808529, N'Bibliothèque Lac', 250, 250,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566277015297812, 44.8535270165028, N'Skate1', 251, 251,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573949476945553, 44.8389853203685, N'Place Saint Projet', 256, 256,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567041126976586, 44.8387641391926, N'MEC Toit', 262, 262,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565040125534483, 44.8542727303211, N'H14Toit', 263, 263,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567515736723729, 44.8392620546689, N'MEC Parvis 1', 264, 264,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566037672243968, 44.853716622717, N'Skate2', 269, 269,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565794139097888, 44.8539034521056, N'Skate3', 270, 270,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.55877795930834, 44.8445693741644, N'Jardin Botanique 2', 278, 278,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.56898193867724, 44.8587996228254, N'CCAS', 283, 283,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552857185264412, 44.8415587925069, N'Mairie Annexe Bastide', 284, 284,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.5461534697722, 44.8730804059259, N'Bibliothèque Bacalan', 285, 285,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.562944756478272, 44.8454998643989, N'Atelier du Pole Giono', 286, 286,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.58012286108146, 44.8480260833129, N'Bibliothèque Jardin Public', 287, 287,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.583625651534337, 44.8567411146206, N'Bibliothèque Grand Parc', 288, 288,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.551660145769663, 44.8247464424864, N'Bibliothèque Son Tay', 289, 289,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.545965462038406, 44.8733767377706, N'Mairie Annexe Bacalan', 290, 290,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572855794265409, 44.8737831801318, N'Bibliothèque Aubiers Est', 292, 292,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573212400039588, 44.873678947015, N'Bibliothèque Aubiers Ouest', 293, 293,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.614750435146855, 44.8462189190718, N'Club senior A. Faulat', 297, 297,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.590466286287994, 44.8313735113726, N'Club senior Manon Cormier', 298, 298,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566992522704808, 44.8392105406195, N'MEC Ponton Honneur', 300, 300,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.615540888697352, 44.8521992462704, N'Mairie annexe Caudéran', 301, 301,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.584254984261177, 44.8578601494929, N'Bibliothèque Grand Parc', 302, 302,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576620048679785, 44.8395591065173, N'11 Louis de Jabrun – Point info familles', 303, 303,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572865956295554, 44.8309830857822, N'Place Victoire 2', 305, 305,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569200241364265, 44.8410140620273, N'Mirroir Eau Sud 2', 306, 306,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569200241364265, 44.8410140620273, N'Mirroir Eau Sud 1', 307, 307,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567515736723729, 44.8392620546689, N'MEC Parvis 2', 308, 308,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567433896865495, 44.8524016896823, N'Martinique', 309, 309,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569721776676866, 44.8422218729537, N'Mirroir Eau Nord 1', 310, 310,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569721776676866, 44.8422218729537, N'Mirroir Eau Nord 2', 311, 311,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571903305826729, 44.8391103676243, N'Place Camille Julian', 312, 312,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.542220092867005, 44.8440508455854, N'Bibliothèque Bastide', 313, 313,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572389982423059, 44.8307985828337, N'Place Victoire 1', 315, 315,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577503495803707, 44.8386324349361, N'Place Jean Moulin', 316, 316,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.57614629380538, 44.8379430250813, N'Pey Berland Forges', 318, 318,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.578910265324865, 44.837711742468, N'Pey Berland HDV1', 319, 319,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.574432450345082, 44.8336437547877, N'DGA', 327, 327,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.57107243117208, 44.8428045490531, N'Mission de l''europe', 328, 328,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.579334526052336, 44.8381751334157, N'Hotel de Ville', 329, 329,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.575700287391315, 44.8332183324786, N'Club Senior Magendie', 330, 330,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.564587482367892, 44.824042601314, N'Club Senior Billaudel', 331, 331,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.54612814234151, 44.8732525681827, N'Club Senior Lumineuse', 332, 332,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.594615831920461, 44.8390063085784, N'Atelier du Pole Brach', 333, 333,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566911648044833, 44.8308409214513, N'Bibliothèque St Michel', 334, 334,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.610846175573378, 44.8317669819592, N'Bibliothèque St Augustin', 335, 335,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.542122327198918, 44.8441809608696, N'Bibliothèque Bastide', 336, 336,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.611203453401391, 44.8329617080137, N'Mairie annexe St Augustin', 337, 337,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572605659172594, 44.8520594063995, N'Chartron Marche', 961, 961,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.560807267688721, 44.8307465140602, N'EBA', 962, 962,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572446978093307, 44.8524145706, N'Chartron Halle', 963, 963,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.582288048099357, 44.8599395812081, N'Grand Parc Centre Commercial', 964, 964,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.561617879610019, 44.8306800957878, N'EBA Annexe', 965, 965,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.558416750698445, 44.8321808329079, N'Quai Sport Maison Jardinier', 1010, 1010,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Points d''accès à Internet'))

