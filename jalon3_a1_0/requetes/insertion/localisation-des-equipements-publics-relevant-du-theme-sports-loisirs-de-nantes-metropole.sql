DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Lieux de sports et loisirs')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Lieux de sports et loisirs'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Lieux de sports et loisirs', N'tennis.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212756571105, -1.59872468497865, N'Piscine Durantière - Nantes', 111, Piscine Durantière - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2275397157953, -1.59218618244518, N'Piscine Dervallières - Nantes', 112, Piscine Dervallières - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2098578332065, -1.56006613543103, N'Piscine Léo Lagrange Ile Gloriette - Nantes', 114, Piscine Léo Lagrange Ile Gloriette - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2457334512595, -1.54388993682305, N'Club Léo Lagrange Nantes Aviron', 116, Club Léo Lagrange Nantes Aviron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2692656930341, -1.57977559307076, N'Golf Nantes Erdre', 118, Golf Nantes Erdre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2365563818049, -1.55129581053572, N'Base d''Aviron Universitaire', 119, Base d'Aviron Universitaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2639930545289, -1.53365378581846, N'Plaine de Jeux de la Beaujoire', 692, Plaine de Jeux de la Beaujoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2538634556883, -1.51861361543117, N'Stade La Halvêque', 694, Stade La Halvêque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.239734960728, -1.53284534905717, N'Stade Joseph Geoffroy La Saint-Pierre', 695, Stade Joseph Geoffroy La Saint-Pierre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2452728951847, -1.52669752937696, N'Stade Batignolles', 696, Stade Batignolles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2396673832905, -1.5383314439884, N'Plateau Sportif du Port Boyer', 703, Plateau Sportif du Port Boyer,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2368726881607, -1.52203929092366, N'Espace de Proximité La Grande Noue', 706, Espace de Proximité La Grande Noue,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1959161944956, -1.52647122386286, N'Espace Sportif de Proximité du Clos Toreau', 721, Espace Sportif de Proximité du Clos Toreau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1883832527323, -1.52082048184233, N'Stade de la Gilarderie', 722, Stade de la Gilarderie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1848018764143, -1.51960027094327, N'Stade des Bourdonnières', 723, Stade des Bourdonnières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.24833488268, -1.56372001054341, N'Plaine de Jeux du Petit Port', 732, Plaine de Jeux du Petit Port,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2490898325466, -1.55734123748702, N'Stade Launay Violette', 733, Stade Launay Violette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2244928701333, -1.58037969838993, N'Stade Procé', 737, Stade Procé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2219052569873, -1.5803084859143, N'Stade Annexe de Procé', 738, Stade Annexe de Procé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2022635523877, -1.61578032556758, N'Plaine de Jeux Bernardière', 739, Plaine de Jeux Bernardière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2016418613322, -1.61194218184324, N'Stade Bois de la Musse Espérance St-Yves', 740, Stade Bois de la Musse Espérance St-Yves,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.203030129374, -1.61000448546101, N'Plaine de Jeux Lycée Albert Camus', 741, Plaine de Jeux Lycée Albert Camus,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2200626895231, -1.59280872537732, N'Stade La Mellinet-Audrain', 743, Stade La Mellinet-Audrain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2292210076335, -1.58210919594525, N'Stade Pascal-Laporte', 747, Stade Pascal-Laporte,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2121674917616, -1.58463937662557, N'Gymnase du Club Athlétic Nantais', 840, Gymnase du Club Athlétic Nantais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2272167095547, -1.53111131447497, N'Gymnase Agenêts', 841, Gymnase Agenêts,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1986446498102, -1.59478761421589, N'Gymnase Chantenay', 842, Gymnase Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2482513320381, -1.51748108230335, N'Piscine Jules Verne Haluchère - Nantes', 844, Piscine Jules Verne Haluchère - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2401710295351, -1.50438306254425, N'Stade Jean Jacques Audubon', 846, Stade Jean Jacques Audubon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2614441001634, -1.54361455419714, N'Plaine de Jeux Jonelière', 848, Plaine de Jeux Jonelière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2791655233678, -1.4909378276142, N'Plaine de Jeux de la Mainguais', 849, Plaine de Jeux de la Mainguais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2479389858742, -1.53534434965587, N'Plaine de Jeux l''Eraudière', 850, Plaine de Jeux l'Eraudière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2541680513428, -1.51848979699111, N'Equipement Sportif de Proximité La Halvêque', 851, Equipement Sportif de Proximité La Halvêque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2366966931512, -1.52871415356151, N'Plaine de Jeux Marrière', 853, Plaine de Jeux Marrière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1846104468124, -1.53165351828126, N'Plaine de Jeux Sévres', 855, Plaine de Jeux Sévres,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2243971830527, -1.54071562690763, N'Plaine de jeux Gaston Turpin', 856, Plaine de jeux Gaston Turpin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.255501567701, -1.5747452777234, N'Stade Etoile du Cens', 859, Stade Etoile du Cens,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2624908425566, -1.57333713309748, N'Stade l''Amande', 860, Stade l'Amande,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.209675350334, -1.53281900853191, N'Stade Michel Lecointre Beaulieu', 863, Stade Michel Lecointre Beaulieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.242648370723, -1.5201696337809, N'Stade Pin Sec', 864, Stade Pin Sec,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.233981218756, -1.5472936544112, N'Cercle d''Aviron de Nantes', 865, Cercle d'Aviron de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2579812899213, -1.53949512370314, N'Base Nautique de la Jonelière', 866, Base Nautique de la Jonelière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2254406712455, -1.50964696317175, N'Plaine de Jeux Grand Blottereau', 1061, Plaine de Jeux Grand Blottereau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2192670171279, -1.51974622474479, N'Stade de la Roche', 1072, Stade de la Roche,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2427860997913, -1.55555103389616, N'Patinoire Olympique du Petit Port', 1073, Patinoire Olympique du Petit Port,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.260411863375, -1.56825391542092, N'Plaine de Jeux Géraudière Santos Dumont', 1079, Plaine de Jeux Géraudière Santos Dumont,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2462030149329, -1.56316761933513, N'Hippodrome du Petit Port', 1125, Hippodrome du Petit Port,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2264093976016, -1.59259941569142, N'Stade des Dervallières', 1233, Stade des Dervallières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2328638801984, -1.49998703586133, N'Stade Racing Athlétic Club Cheminots', 1239, Stade Racing Athlétic Club Cheminots,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2319442062122, -1.50037475669311, N'Gymnase RACC', 1240, Gymnase RACC,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2050515534856, -1.54100881728214, N'Stade Mangin Beaulieu', 1353, Stade Mangin Beaulieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2259382709138, -1.50893453793683, N'Circuits Rustiques d''Activités Plein Air - Blottereau', 1439, Circuits Rustiques d'Activités Plein Air - Blottereau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2532293711017, -1.55463899444773, N'Terrain de tennis Bourgeonnière', 1496, Terrain de tennis Bourgeonnière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2525033773486, -1.53821896419221, N'Base Nautique Port Durand S.N.S.M.', 1503, Base Nautique Port Durand S.N.S.M.,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2606723385847, -1.54147665589036, N'Centre Sportif José Arribas', 1540, Centre Sportif José Arribas,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2460838309883, -1.53648614689791, N'Gymnase Amicale Don Bosco', 1559, Gymnase Amicale Don Bosco,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2471057935949, -1.60978483501323, N'Centre Sportif l''Angevinière', 1830, Centre Sportif l'Angevinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2350579489681, -1.59992582425841, N'Gymnase Le Hérault', 1831, Gymnase Le Hérault,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.242707413865, -1.6008832939483, N'Gymnase Joli Mai', 1832, Gymnase Joli Mai,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2150249072251, -1.64989325221638, N'Stade Calvaires', 1835, Stade Calvaires,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2063233654695, -1.61601637936851, N'Gymnase Bernardière', 1836, Gymnase Bernardière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2150709214297, -1.61251401027904, N'Espace Multisport Ernest Renan', 1837, Espace Multisport Ernest Renan,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2154198758843, -1.61358066544455, N'Piscine Ernest Renan - St Herblain', 1838, Piscine Ernest Renan - St Herblain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2106536618271, -1.61225062129551, N'Gymnase Sensive', 1840, Gymnase Sensive,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2179570960269, -1.62222094024926, N'Centre Sportif du Vigneau', 1843, Centre Sportif du Vigneau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.210746403379, -1.65323604713288, N'Gymnase du Bourg', 1844, Gymnase du Bourg,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2077852435338, -1.65677255323662, N'Piscine Bourgonnière  - St Herblain', 1846, Piscine Bourgonnière  - St Herblain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2151682764468, -1.65111526089458, N'Gymnase Changetterie', 1847, Gymnase Changetterie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2518860610327, -1.61007776864608, N'Complexe Sportif Roger-Piccaud', 1965, Complexe Sportif Roger-Piccaud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2500435216439, -1.59134696816738, N'Stade du Verger', 1966, Stade du Verger,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2542766664819, -1.59552405590642, N'Gymnase Emile Gibier', 1967, Gymnase Emile Gibier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.246157821333, -1.59197970160573, N'Complexe de la Ferrière', 1968, Complexe de la Ferrière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2703607676064, -1.5961971384645, N'Gymnase Le Bois Raguenet', 1969, Gymnase Le Bois Raguenet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2600781293576, -1.63172618061595, N'Stade de la Bugallière', 1970, Stade de la Bugallière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2621189108854, -1.63172494291619, N'Gymnase de La Bugallière', 1971, Gymnase de La Bugallière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2732098577681, -1.62565544930331, N'Complexe Sportif de la Frebaudière', 1973, Complexe Sportif de la Frebaudière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2831792345893, -1.62970765071141, N'Stade de Gagné', 1974, Stade de Gagné,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1926587852741, -1.49992018806, N'Gymnase Ouche Quinet', 2026, Gymnase Ouche Quinet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2067361689816, -1.5017449964624, N'Gymnase du Petit Anjou', 2037, Gymnase du Petit Anjou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.19593068403, -1.4890329841085, N'Gymnase Profondine', 2038, Gymnase Profondine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2094370352284, -1.48756395474289, N'Gymnase Savarières', 2039, Gymnase Savarières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.198080590439, -1.52683675403684, N'Complexe Sportif de La Martellière', 2040, Complexe Sportif de La Martellière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1947852992256, -1.48786168522172, N'Stade de la Profondine', 2041, Stade de la Profondine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1945884331847, -1.48352237987671, N'Stade des Gripots', 2042, Stade des Gripots,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1914129715111, -1.52081100676749, N'Gymnase du Douet', 2045, Gymnase du Douet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2054570138992, -1.63378543692181, N'Ensemble Sportif  de l''Orvasserie', 2069, Ensemble Sportif  de l'Orvasserie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1670391904977, -1.62918182822166, N'Les Écuries du Clos - Village du cheval', 2086, Les Écuries du Clos - Village du cheval,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.171742960848, -1.62129629865885, N'Piscine municipale - Bouguenais', 2087, Piscine municipale - Bouguenais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2012865194815, -1.67079315636277, N'Stade Football Félix Guyot', 2111, Stade Football Félix Guyot,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131721364892, -1.49257395344305, N'Practice de golf', 2153, Practice de golf,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2111885912061, -1.50685768063897, N'Centre Equestre Ile Pinette', 2154, Centre Equestre Ile Pinette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2113704422294, -1.50056167624582, N'Stade René Massé', 2155, Stade René Massé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2112714740649, -1.50375232534838, N'Stade Maurice Thollé', 2156, Stade Maurice Thollé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1937733642716, -1.48516777776613, N'Circuits Rustiques d''Activites Plein Air des Gripots', 2158, Circuits Rustiques d'Activites Plein Air des Gripots,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2112280735376, -1.51241128658423, N'Circuits Rustiques d''Activites en Plein Air de l''Ile Forget', 2159, Circuits Rustiques d'Activites en Plein Air de l'Ile Forget,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2126800728297, -1.49576482435283, N'Stade de la Ligue Atlantique de Football', 2161, Stade de la Ligue Atlantique de Football,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1724561976818, -1.62282816376482, N'Stade de la Croix Jeannette', 2193, Stade de la Croix Jeannette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1580198405419, -1.62397991744768, N'Parcours sportif de la Ville-au-Denis', 2200, Parcours sportif de la Ville-au-Denis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1832239739524, -1.57897799892288, N'Salle du Fougan-de-Mer', 2201, Salle du Fougan-de-Mer,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1813384628233, -1.63094055940366, N'Stade de Beauvoir', 2202, Stade de Beauvoir,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1756170441002, -1.58662907238535, N'Stade des Landes', 2203, Stade des Landes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1790041615066, -1.62828964796547, N'Plateau d''évolution Cité de Beauvoir', 2204, Plateau d'évolution Cité de Beauvoir,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2409048062182, -1.61838202891687, N'Centre Sportif du Val de Chézine', 2216, Centre Sportif du Val de Chézine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.238694249991, -1.63338638581862, N'Plaine de Jeux La Bergerie', 2222, Plaine de Jeux La Bergerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1951295237465, -1.6478993182919, N'Gymnase Haute-Indre', 2243, Gymnase Haute-Indre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1969507666815, -1.67143790880919, N'Golf Miniature', 2248, Golf Miniature,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2008208726484, -1.66931281917631, N'Gymnase Basse-Indre', 2252, Gymnase Basse-Indre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2130697383576, -1.5710414266968, N'Gymnase Gigant', 2299, Gymnase Gigant,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2720906363171, -1.51830834178211, N'Complexe Sportif de Saint Joseph de Porterie', 2316, Complexe Sportif de Saint Joseph de Porterie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2308000633039, -1.52715800803948, N'Gymnase Noë Lambert', 2317, Gymnase Noë Lambert,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2682760901519, -1.69670558494988, N'Parc d''Attractions des Naudières', 2339, Parc d'Attractions des Naudières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1728202523257, -1.7255254070564, N'Stade Julien Micheau', 2411, Stade Julien Micheau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1683199340313, -1.72210655961521, N'Gymnase Brains', 2414, Gymnase Brains,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3031531081619, -1.52400721179044, N'Club nautique de Port Jean', 2426, Club nautique de Port Jean,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2843363833325, -1.48185667004875, N'Centre Sportif du Moulin Boiseau', 2431, Centre Sportif du Moulin Boiseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.295826083548, -1.4874311720501, N'Centre sportif Jean Gauvrit', 2433, Centre sportif Jean Gauvrit,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3024911797028, -1.49260361219881, N'Piscine Daniel Gilard - Carquefou', 2434, Piscine Daniel Gilard - Carquefou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3185834225673, -1.46999264876481, N'Complexe sportif de la Tournière', 2435, Complexe sportif de la Tournière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2104870142114, -1.71708554407837, N'Stade de la Frémondière', 2442, Stade de la Frémondière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2120419396265, -1.71946117169609, N'Stade Donatien et Suzanne Hauray', 2443, Stade Donatien et Suzanne Hauray,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2144055961146, -1.72000040792451, N'Centre Sportif Paul Langevin', 2444, Centre Sportif Paul Langevin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2138643756754, -1.72039299775916, N'Piscine Baptiste Lefèvre - Couëron', 2445, Piscine Baptiste Lefèvre - Couëron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2207410736832, -1.68423886292367, N'Complexe Sportif Léo-Lagrange', 2453, Complexe Sportif Léo-Lagrange,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1895824580666, -1.48606376344073, N'Bowling EuroBowl', 2464, Bowling EuroBowl,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2877803426137, -1.48894815586082, N'Golf de l''Epinay', 2465, Golf de l'Epinay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2163316906721, -1.68430936585207, N'Gymnase Pierre Moisan', 2466, Gymnase Pierre Moisan,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2145372502315, -1.72758810210756, N'Gymnase Jules Boullery', 2468, Gymnase Jules Boullery,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1823718209336, -1.5729261472647, N'Patinoire Olympique Rezé', 2469, Patinoire Olympique Rezé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2122651467985, -1.73884756734602, N'Centre Sportif René Gaudin', 2471, Centre Sportif René Gaudin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2115055172575, -1.7368906288991, N'Vélodrome Marcel-de-la-Provôté', 2474, Vélodrome Marcel-de-la-Provôté,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2853531096737, -1.42255628400108, N'Centre Equestre de la Hillière', 2477, Centre Equestre de la Hillière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2027334741685, -1.78016713157777, N'Centre Equestre Ferme de la Chauffetière', 2479, Centre Equestre Ferme de la Chauffetière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1956959966305, -1.75867046958166, N'Salle omnisports - Gymnase intercommunal', 2491, Salle omnisports - Gymnase intercommunal,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1961777320699, -1.76061423481004, N'Stade Municipal Le Pellerin', 2493, Stade Municipal Le Pellerin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2437449092645, -1.70659505805765, N'Centre Equestre de la Pirouette', 2501, Centre Equestre de la Pirouette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1498859375824, -1.51644264715241, N'Centre Equestre Poney Club', 2505, Centre Equestre Poney Club,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2988927790405, -1.55725415859161, N'Stade de Massigné', 2511, Stade de Massigné,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2680033465562, -1.54440193745847, N'Stade de la Haute Gournière', 2513, Stade de la Haute Gournière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3036283754249, -1.55960525330873, N'Stade de rugby Robert Mesnard', 2514, Stade de rugby Robert Mesnard,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2676571941241, -1.55084218661066, N'Halle des sports Bernard Corneau', 2516, Halle des sports Bernard Corneau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1827452580804, -1.6745670922931, N'Stade de football municipal La Montagne', 2528, Stade de football municipal La Montagne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.192082171731, -1.72513264303311, N'Complexe Sportif Les Genets', 2534, Complexe Sportif Les Genets,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1877658275626, -1.71022499843855, N'Stade Saint Jean de Boiseau', 2536, Stade Saint Jean de Boiseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1496505324574, -1.69458100821657, N'Centre Sportif de Bellestre', 2539, Centre Sportif de Bellestre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1551489030202, -1.68226632310647, N'Centre Sportif Les Ormeaux', 2540, Centre Sportif Les Ormeaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1340375737984, -1.73260456982872, N'Salle Omnisports', 2543, Salle Omnisports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.181166226703, -1.67548286181971, N'Stade de Football Marcel Lejay', 2544, Stade de Football Marcel Lejay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.124218701987, -1.6351141907511, N'Halle de sport La Pavelle', 2545, Halle de sport La Pavelle,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1237730354818, -1.63258914886237, N'Salle polyvalente', 2546, Salle polyvalente,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2928952007586, -1.39372197288169, N'Stade Municipal des Loquets', 2551, Stade Municipal des Loquets,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2933658841445, -1.39001752893399, N'Gymnase Armand Jolaine', 2554, Gymnase Armand Jolaine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2489007475172, -1.48292958826837, N'Gymnase Marc Jaffret', 2562, Gymnase Marc Jaffret,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1538072247004, -1.4702859062032, N'Hippodrome du Portillon', 2564, Hippodrome du Portillon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1648119268734, -1.4665017603452, N'Complexe Sportif Sèvre et Maine', 2565, Complexe Sportif Sèvre et Maine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1658084208778, -1.47239612768862, N'Stade de Vertou Centre', 2566, Stade de Vertou Centre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.164908839673, -1.46996117459646, N'Piscine municipale - Vertou', 2567, Piscine municipale - Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1847635199967, -1.49234523360176, N'Stade Vincent et Robert Girard', 2571, Stade Vincent et Robert Girard,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1591236342807, -1.48558414099214, N'Stade Loreau', 2573, Stade Loreau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2585382482751, -1.49047870675377, N'Gymnase  Eric Tabarly', 2574, Gymnase  Eric Tabarly,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2451678793443, -1.46219113950204, N'Centre Sportif Philippe Porcher', 2576, Centre Sportif Philippe Porcher,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2557045303056, -1.45549029320852, N'Centre Equestre (CERVAL)', 2580, Centre Equestre (CERVAL),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2536698421485, -1.48139885324741, N'Stade municipal', 2581, Stade municipal,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2453944638194, -1.47940132531115, N'Complexe sportif du Patisseau', 2596, Complexe sportif du Patisseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2589533862309, -1.49054823768047, N'Plateau sportif de la Reinetière', 2602, Plateau sportif de la Reinetière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2646141078844, -1.54003681983312, N'Base Nautique de Port Barbe', 2607, Base Nautique de Port Barbe,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2937603816921, -1.5370732372526, N'Base nautique de l''ANCRE - La Grimaudière', 2608, Base nautique de l'ANCRE - La Grimaudière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2972906511825, -1.53828369820079, N'Centre Equestre de la Gascherie', 2613, Centre Equestre de la Gascherie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2768331381298, -1.57123346471296, N'Centre Equestre de la Noue Verrière', 2623, Centre Equestre de la Noue Verrière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3413445174179, -1.58079609859183, N'Centre Equestre de la Vaillantière', 2624, Centre Equestre de la Vaillantière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1839678788112, -1.65323044926348, N'Site naturel de loisirs de la Roche Ballue', 2625, Site naturel de loisirs de la Roche Ballue,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1619242000662, -1.47786508318741, N'Base Nautique Canöé Kayak Vertou', 2633, Base Nautique Canöé Kayak Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2179367333265, -1.46394887294369, N'Gymnase de Goulaine', 2643, Gymnase de Goulaine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2178316177451, -1.46875691160205, N'Stade Henri Michel', 2644, Stade Henri Michel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2169161908085, -1.46801264502142, N'Gymnase Henri Michel', 2645, Gymnase Henri Michel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2028562340209, -1.46796962085231, N'Stade de Rugby des Rouleaux', 2647, Stade de Rugby des Rouleaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1839561684273, -1.68208699209713, N'Gymnase Saint Exupéry', 2648, Gymnase Saint Exupéry,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1460980882278, -1.52204843919714, N'Complexe ancien stade Les Sorinières', 2649, Complexe ancien stade Les Sorinières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2069828317016, -1.79331782394207, N'Club Nautique de la Martinière', 2650, Club Nautique de la Martinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2615705329282, -1.45369470250542, N'Parc des Sports', 2651, Parc des Sports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2458174722199, -1.46132231714737, N'Stade Philippe Porcher', 2652, Stade Philippe Porcher,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1636639470401, -1.53122003430701, N'Complexe sportif de la Robinière', 2653, Complexe sportif de la Robinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1818649955412, -1.56942931910424, N'Stade Léo Lagrange', 2654, Stade Léo Lagrange,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1830205869091, -1.56875728473706, N'Piscine municipale Victor-Jara - Rezé', 2655, Piscine municipale Victor-Jara - Rezé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1781407722569, -1.55872927787927, N'Gymnase Lysiane et Michel Liberge', 2656, Gymnase Lysiane et Michel Liberge,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1856949495429, -1.54762429913796, N'Gymnase Cercle Saint Paul', 2657, Gymnase Cercle Saint Paul,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.185424945757, -1.55761005300458, N'Gymnase Château Nord', 2658, Gymnase Château Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1805107974701, -1.55943408915102, N'Gymnase Lucien Cavalin', 2659, Gymnase Lucien Cavalin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.183632569294, -1.56911450733204, N'Gymnase Cités Unies COSEC', 2660, Gymnase Cités Unies COSEC,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1737926961696, -1.54665858864919, N'Gymnase du Chêne Creux', 2661, Gymnase du Chêne Creux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1784225868172, -1.54863252978591, N'Gymnase E. Cretual', 2662, Gymnase E. Cretual,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1867459132979, -1.55069399007275, N'Gymnase Foyer AEPR', 2663, Gymnase Foyer AEPR,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1832770053528, -1.56021694457925, N'Gymnase Jean Perrin', 2664, Gymnase Jean Perrin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1858505169181, -1.54307423533422, N'Gymnase Julien Douillard', 2665, Gymnase Julien Douillard,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1762262917148, -1.53696348110384, N'Gymnase Ouche Dinier', 2666, Gymnase Ouche Dinier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.190039740392, -1.57064522845569, N'Gymnase Yvonne et Alexandre Plancher', 2667, Gymnase Yvonne et Alexandre Plancher,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1841109487559, -1.5495168410679, N'Gymnase Roger Salengro', 2668, Gymnase Roger Salengro,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1649147456071, -1.46711916996356, N'Gymnase Jean-Pierre Morel', 2669, Gymnase Jean-Pierre Morel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1643410306174, -1.46729103893042, N'Gymnase 2', 2670, Gymnase 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1638293313631, -1.46555765365028, N'Stade Sèvre et Maine', 2671, Stade Sèvre et Maine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1515913046786, -1.51806450579143, N'Roller-Skatepark - Les Sorinières', 2672, Roller-Skatepark - Les Sorinières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1837025528381, -1.49772048555354, N'Gymnase Raymond Durand', 2673, Gymnase Raymond Durand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1825752547099, -1.49883622580853, N'Stade Raymond Durand', 2674, Stade Raymond Durand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1563946320429, -1.44592048939953, N'Piscine des Thébaudières - Vertou', 2675, Piscine des Thébaudières - Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1632873138492, -1.46722162122092, N'Salle Sèvre et Maine', 2676, Salle Sèvre et Maine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.265941420438, -1.67651649196514, N'Complexe Sportif', 2687, Complexe Sportif,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.170732458997, -1.6220346010185, N'Gymnase Joël Dubois', 3087, Gymnase Joël Dubois,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1484770051065, -1.5231167400241, N'Gymnase Joachim Marnier', 3088, Gymnase Joachim Marnier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.257656005473, -1.54281104096611, N'Mini Auto Club Nantais', 3120, Mini Auto Club Nantais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2196220557256, -1.51303380175172, N'Skatepark Le Hangar - Nantes', 3155, Skatepark Le Hangar - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2231767322878, -1.54988607605579, N'Gymnase Sully', 3165, Gymnase Sully,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1627492950353, -1.52966462186016, N'Courts de Tennis Robinière', 3239, Courts de Tennis Robinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1651527054851, -1.53045499402347, N'Boulodrome Robinière', 3241, Boulodrome Robinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1806455552376, -1.57189318372349, N'Skatepark - Rezé', 3260, Skatepark - Rezé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1811938949034, -1.5679119813604, N'Rezé Tennis Club - Halles de la Trocardière', 3262, Rezé Tennis Club - Halles de la Trocardière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1602373880865, -1.56997068365413, N'Espace Sport Canin Bouguenais', 3290, Espace Sport Canin Bouguenais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1832208384498, -1.57953925266013, N'Plateau sportif du Fougan-de-Mer', 3298, Plateau sportif du Fougan-de-Mer,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1729765475935, -1.60547145313346, N'Salle des Bélians', 3305, Salle des Bélians,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1711869849478, -1.62049028927955, N'Terrain stabilisé de la Croix Jeannette', 3306, Terrain stabilisé de la Croix Jeannette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1700686549648, -1.61889568219984, N'Terrains de Tennis de la Gagnerie', 3307, Terrains de Tennis de la Gagnerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1703680152882, -1.61804709117255, N'Terrain de tir à l''arc de la Gagnerie', 3308, Terrain de tir à l'arc de la Gagnerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1766393374081, -1.59180917746448, N'Centre de tennis Arthur Ashe', 3309, Centre de tennis Arthur Ashe,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1762295671634, -1.59302228346367, N'Gymnase de la Baronnais', 3310, Gymnase de la Baronnais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.175882322274, -1.59219906767451, N'Gymnase de la Neustrie', 3311, Gymnase de la Neustrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1757333241693, -1.5927673773589, N'Salle des Mousquetaires', 3312, Salle des Mousquetaires,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1753910180697, -1.59112844820114, N'Plateau d''évolution de la Neustrie', 3313, Plateau d'évolution de la Neustrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1749884728192, -1.59181038072302, N'Stade Pierre de Coubertin', 3314, Stade Pierre de Coubertin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1652856313396, -1.47325814337337, N'Salle de Tennis de Table Vertou', 3330, Salle de Tennis de Table Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1658302013054, -1.47981291545655, N'Terrain de Pétanque du Parc de la Sèvre', 3331, Terrain de Pétanque du Parc de la Sèvre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1833399515573, -1.49907317912865, N'Terrain de Tir à l''Arc Vertou', 3336, Terrain de Tir à l'Arc Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2093886560433, -1.51694761919064, N'Piste de Bi-cross Ile Forget', 3361, Piste de Bi-cross Ile Forget,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1925737586485, -1.50199448455118, N'Tennis de l''Ouche Catin', 3362, Tennis de l'Ouche Catin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.209361707034, -1.49951203283728, N'Complexe Sportif Saint Sébastien', 3363, Complexe Sportif Saint Sébastien,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2921609724699, -1.49016586168623, N'Skatepark Jouandière - Carquefou', 3371, Skatepark Jouandière - Carquefou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2834760541553, -1.48272657049156, N'Gymnase Alella', 3373, Gymnase Alella,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2816599536798, -1.4819778113677, N'Salle du Tremplin', 3374, Salle du Tremplin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2174956015047, -1.46414834529638, N'Salle de Tennis', 3379, Salle de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1981369987963, -1.45211641003807, N'Gymnase La Herdrie', 3381, Gymnase La Herdrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1970606984537, -1.4518438765204, N'Stade La Herdrie', 3382, Stade La Herdrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2969808407456, -1.55836528167593, N'Gymnase de Mazaire', 3387, Gymnase de Mazaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3062805421427, -1.55849488739076, N'Complexe sportif de La Coutancière', 3391, Complexe sportif de La Coutancière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2897153767933, -1.53853292487801, N'Complexe sportif Buisson de la Grolle', 3392, Complexe sportif Buisson de la Grolle,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1543009732966, -1.68227255927785, N'Piste d''athlétisme', 3406, Piste d'athlétisme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2195633834591, -1.68346552493436, N'Salle Léo Lagrange', 3412, Salle Léo Lagrange,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2140301476733, -1.72127239006127, N'Gymnase Paul Langevin', 3414, Gymnase Paul Langevin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2123101868461, -1.70294117334378, N'Piste Bi-cross Couëron', 3417, Piste Bi-cross Couëron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1973163153296, -1.67199390697825, N'Boulodrome Indre', 3423, Boulodrome Indre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2935906250654, -1.39044844263005, N'Salle de sport', 3427, Salle de sport,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2594508036441, -1.63451043548246, N'Centre Stévin', 3435, Centre Stévin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2571645470284, -1.58156438456381, N'Tennis Orvault', 3441, Tennis Orvault,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2632318683259, -1.59694048733103, N'Plateau Multisports', 3442, Plateau Multisports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2529330508998, -1.61156164440896, N'Tennis Roger Picaud', 3443, Tennis Roger Picaud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.251124918662, -1.61012134345654, N'Piscine Roger Picaud - Orvault', 3444, Piscine Roger Picaud - Orvault,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1827492524075, -1.67655751897517, N'Complexe Sports Loisirs Francis Lespinet', 3449, Complexe Sports Loisirs Francis Lespinet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1961900301957, -1.76158045857492, N'Courts de Tennis', 3458, Courts de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1960744912035, -1.75932744212281, N'Terrain de pétanque', 3459, Terrain de pétanque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1207193963842, -1.63772247513102, N'Tennis Saint Aignan de Grand Lieu', 3461, Tennis Saint Aignan de Grand Lieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1242267697203, -1.63166120014361, N'Stade Jean Bertin', 3465, Stade Jean Bertin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1334304977231, -1.73297467088365, N'Plaine de Jeux de la Rive', 3482, Plaine de Jeux de la Rive,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2582512018069, -1.49018590950255, N'Gymnase Marcel Le Bonniec', 3489, Gymnase Marcel Le Bonniec,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1475885964023, -1.52505259448483, N'Gymnase Jules Léauté', 3495, Gymnase Jules Léauté,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1482185616221, -1.52296741885726, N'Salle de Judo Les Sorinières', 3497, Salle de Judo Les Sorinières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1500101560296, -1.51904656373622, N'Complexe de loisirs Louis Barta', 3501, Complexe de loisirs Louis Barta,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1980987641279, -1.64003404395561, N'Motocross Christian Bageot', 3529, Motocross Christian Bageot,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2157731371012, -1.62048942265318, N'Tir à l''Arc Saint Herblain', 3531, Tir à l'Arc Saint Herblain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1596886310787, -1.52154017923406, N'Salle de Tennis de Table du Haut Vigneau', 3578, Salle de Tennis de Table du Haut Vigneau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1498081053604, -1.69313170407814, N'Stade Georges Tougeron', 3593, Stade Georges Tougeron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.24636074149, -1.61054783037182, N'Espace Multisport Sillon', 3607, Espace Multisport Sillon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2163926638424, -1.5317100587433, N'Piscine Petite Amazonie - Nantes', 3634, Piscine Petite Amazonie - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2123074407887, -1.55351435609301, N'Skatepark Ricordeau - Nantes', 3660, Skatepark Ricordeau - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.213201100097, -1.54869427347157, N'Terrain Multisport Monteil', 3662, Terrain Multisport Monteil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2309716832261, -1.52528828389541, N'Boulodrome Noë Lambert', 3683, Boulodrome Noë Lambert,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1762811485736, -1.46628233029004, N'Complexe sportif des Echalonnières', 3788, Complexe sportif des Echalonnières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1759243946878, -1.46967483894057, N'Plateau sportif Les Echalonnières', 3789, Plateau sportif Les Echalonnières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1767514191249, -1.47063261253475, N'Salle omnisports des Echalonnières', 3790, Salle omnisports des Echalonnières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2775056951158, -1.71206542059429, N'Centre Equestre La Gourmette', 3798, Centre Equestre La Gourmette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2122960037366, -1.55134942285908, N'Gymnase CEPA', 3818, Gymnase CEPA,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2006448286548, -1.59266498071941, N'Espace Spotif des Courtils', 3897, Espace Spotif des Courtils,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2077181434763, -1.61048376514402, N'Espace Sportif des Martyrs Irlandais', 3898, Espace Sportif des Martyrs Irlandais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2046109159618, -1.60070048031775, N'Espace Sportif Terrain A. Fournier', 3901, Espace Sportif Terrain A. Fournier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2073848506105, -1.60361198834223, N'Espace Sportif Terrain du Petit Verger', 3902, Espace Sportif Terrain du Petit Verger,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1998915071053, -1.60892584696039, N'Espace Sportif ZAC Montplaisir', 3904, Espace Sportif ZAC Montplaisir,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2213069311405, -1.58227742124368, N'Stade Raymond Baumard - Centre Hebertiste', 3905, Stade Raymond Baumard - Centre Hebertiste,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2247463435712, -1.59247267037624, N'Espace Sportif Meissonier', 3906, Espace Sportif Meissonier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2103400239408, -1.60302864200755, N'Salle de Musculation du Jamet - Bellevue', 3907, Salle de Musculation du Jamet - Bellevue,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2162064931775, -1.52077937985436, N'Espace Sportif Malakoff', 3910, Espace Sportif Malakoff,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2279541332966, -1.52634402218328, N'Espace Sportif Noe Mitrie', 3911, Espace Sportif Noe Mitrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2209116115039, -1.53364946392769, N'Espace Sportif Moutonnerie', 3912, Espace Sportif Moutonnerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2052549515569, -1.53163551400006, N'Terrain multisports Sébilleau', 3917, Terrain multisports Sébilleau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1929657069178, -1.53691739665354, N'Gymnase Bonne Garde', 27, Gymnase Bonne Garde,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2064593469832, -1.55149274758042, N'Gymnase Emile Morice', 28, Gymnase Emile Morice,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2038636257076, -1.57752951962898, N'Gymnase Gravaud', 29, Gymnase Gravaud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2048690280705, -1.54281139595074, N'Centre Sportif Mangin Beaulieu', 30, Centre Sportif Mangin Beaulieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1910659920083, -1.52936590210597, N'Gymnase Gobelets Ripossière', 32, Gymnase Gobelets Ripossière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2282590704611, -1.51222501260675, N'Gymnase Raphaël Lebel', 34, Gymnase Raphaël Lebel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2372383103632, -1.51029286158558, N'Gymnase Doulon', 36, Gymnase Doulon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2351002409264, -1.52250611892111, N'Centre Sportif Croissant', 37, Centre Sportif Croissant,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2146953649976, -1.52857641995758, N'Gymnase Malakoff 4', 41, Gymnase Malakoff 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2035712201959, -1.61090292783313, N'Gymnase Albert Camus', 42, Gymnase Albert Camus,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2537355401747, -1.51723434629303, N'Gymnase La Halvêque', 44, Gymnase La Halvêque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.241523594468, -1.52150628321785, N'Gymnase Urbain Le Verrier', 45, Gymnase Urbain Le Verrier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2425205913952, -1.52946788379463, N'Gymnase des Marsauderies', 46, Gymnase des Marsauderies,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2069254452754, -1.59051123448175, N'Gymnase Chantenaysienne', 48, Gymnase Chantenaysienne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2082910721207, -1.60741124990384, N'Gymnase Jean Zay Bellevue', 50, Gymnase Jean Zay Bellevue,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2104278349921, -1.60388252601688, N'Gymnase Jamet', 51, Gymnase Jamet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2141254689289, -1.60509557209857, N'Complexe Sportif de la Durantière', 52, Complexe Sportif de la Durantière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2305298815958, -1.59366789931119, N'Gymnase Pierre de Coubertin', 53, Gymnase Pierre de Coubertin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2261929641064, -1.59573765025371, N'Gymnase Jean Ogé', 56, Gymnase Jean Ogé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2259611738148, -1.59182606948918, N'Complexe Sportif Dervallières', 57, Complexe Sportif Dervallières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2409417884044, -1.57776513067268, N'Gymnase Longchamp', 58, Gymnase Longchamp,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2637856588362, -1.57642391541624, N'Gymnase Bout des Landes', 60, Gymnase Bout des Landes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2613899375134, -1.56758509177524, N'Gymnase Géraudière', 61, Gymnase Géraudière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2546887975864, -1.56286476233857, N'Gymnase Le Baut', 62, Gymnase Le Baut,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2522657385138, -1.55512499401024, N'Gymnase  Barboire', 63, Gymnase  Barboire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2407760976174, -1.53826191612352, N'Gymnase du Port Boyer', 64, Gymnase du Port Boyer,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2095250968194, -1.53677206941937, N'Palais des Sports Beaulieu', 65, Palais des Sports Beaulieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2236630243329, -1.54107241363304, N'Gymnase Gaston Turpin', 67, Gymnase Gaston Turpin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2327235683155, -1.54372092947929, N'Gymnase du Coudray', 71, Gymnase du Coudray,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.20999754084, -1.55964752075255, N'Gymnase Léo Lagrange', 72, Gymnase Léo Lagrange,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2243419211878, -1.56398900131605, N'Gymnase Joêl Paon', 76, Gymnase Joêl Paon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2239660590438, -1.56288962195842, N'Gymnase La Similienne', 77, Gymnase La Similienne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.223444046917, -1.55798634041585, N'Gymnase V. Hugo', 79, Gymnase V. Hugo,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2164042716215, -1.56139305853245, N'Gymnase Armand Coidelle', 80, Gymnase Armand Coidelle,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2157271429871, -1.56706186061684, N'Gymnase Sévigné', 83, Gymnase Sévigné,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2100485073366, -1.5734859854521, N'Gymnase Leloup Bouhier', 84, Gymnase Leloup Bouhier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2272303205278, -1.57598246882318, N'Gymnase Gaston Serpette', 85, Gymnase Gaston Serpette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2291425674547, -1.58358465931778, N'Gymnase du Breil Malville', 86, Gymnase du Breil Malville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2318280425404, -1.5800023215603, N'Centre Sportif et Culturel La Laëtitia', 88, Centre Sportif et Culturel La Laëtitia,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.202717675415, -1.58033399312007, N'Gymnase de la Mellinet', 92, Gymnase de la Mellinet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2140537074209, -1.579919327956, N'Gymnase ASPTT Appert', 93, Gymnase ASPTT Appert,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2282851429191, -1.52010465635145, N'Gymnase ASTA', 94, Gymnase ASTA,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.235391342113, -1.57972152389204, N'Gymnase ASPTT de Longchamp', 95, Gymnase ASPTT de Longchamp,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.230280169119, -1.54328640921046, N'Salle Saint Donatien', 97, Salle Saint Donatien,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2128111349278, -1.53864710450033, N'Stade Marcel Saupin', 99, Stade Marcel Saupin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2560188243375, -1.52470397906934, N'Stade de la Beaujoire Louis Fonteneau', 104, Stade de la Beaujoire Louis Fonteneau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2353670612924, -1.51004339094618, N'Plaine de Jeux Colinière', 106, Plaine de Jeux Colinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2691044373119, -1.58770491033286, N'Plaine de Jeux Basses Landes', 107, Plaine de Jeux Basses Landes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2138786486921, -1.60046869676866, N'Vélodrome Stade Petit Breton', 108, Vélodrome Stade Petit Breton,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2117738156213, -1.60037031182162, N'Plaine de Jeux Durantière', 109, Plaine de Jeux Durantière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2428434218458, -1.55446089756306, N'Piscine Petit Port - Nantes', 110, Piscine Petit Port - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2314200917769, -1.58148771094116, N'Boulodrome du Breil', 3918, Boulodrome du Breil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2416435521858, -1.5727031517998, N'Skatepark Schuman - Nantes', 3920, Skatepark Schuman - Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2367088534381, -1.55129617973765, N'Base Nautique de l''UNA', 3921, Base Nautique de l'UNA,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2570977823384, -1.540807058322, N'Stand de Tir des Jamonières', 3923, Stand de Tir des Jamonières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2595297012164, -1.57826013212345, N'Plateau Sportif Winnipeg', 3924, Plateau Sportif Winnipeg,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2533733262975, -1.56724591076149, N'Plateau Sportif André Chénier', 3925, Plateau Sportif André Chénier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.258481052182, -1.56767475232835, N'Plateau Sportif de la Géraudière', 3927, Plateau Sportif de la Géraudière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2516895143255, -1.55531376070076, N'UFR STAPS', 3930, UFR STAPS,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2662235402302, -1.52053450198753, N'Equipement Sportif des Quatre Jeudis', 3932, Equipement Sportif des Quatre Jeudis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2680687130896, -1.51982819492898, N'Espace Sportif Louis Pergaud', 3934, Espace Sportif Louis Pergaud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2682287243193, -1.5285551638079, N'Espace Sportif Ecole du Linot', 3935, Espace Sportif Ecole du Linot,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2354161995634, -1.51229825260666, N'Plaine de Jeux du Lycée de la Colinière', 3937, Plaine de Jeux du Lycée de la Colinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2368185757269, -1.52784818256959, N'Halle de Tennis de la Marrière', 3938, Halle de Tennis de la Marrière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2341578249064, -1.53252726238317, N'Equipement Sportif Marché de la Marrière', 3939, Equipement Sportif Marché de la Marrière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.239935244659, -1.5051183545599, N'Stade René Chevalier', 3940, Stade René Chevalier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2426956453976, -1.52266565989614, N'Equipement Sportif du Square A. Fresnel', 3941, Equipement Sportif du Square A. Fresnel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2300547381758, -1.52586312329866, N'Complexe sportif de la Noë Lambert', 3942, Complexe sportif de la Noë Lambert,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2406744968973, -1.52102580886387, N'Salle de Boxe Bottière Nantes Est', 3943, Salle de Boxe Bottière Nantes Est,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2474595193242, -1.51227991311785, N'Equipement Sportif de la Haluchère', 3944, Equipement Sportif de la Haluchère,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2598818439842, -1.51301291030959, N'Boulodrome du Bèle', 3953, Boulodrome du Bèle,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3018248360051, -1.50361701293574, N'Bowling de l''Erdre', 3957, Bowling de l'Erdre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2091102616296, -1.53037444889631, N'Terrain multisports Millerand', 3970, Terrain multisports Millerand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3039615378976, -1.50241580552544, N'Espace Sportif Le Souchais', 4039, Espace Sportif Le Souchais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2258162797341, -1.59817455093445, N'Espace Sportif Louis Le Nain', 4044, Espace Sportif Louis Le Nain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.237434063416, -1.51413692175225, N'Gymnase Bottière Chênaie', 4102, Gymnase Bottière Chênaie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2488734221852, -1.55557682568905, N'Stadium Métropolitain Pierre Quinon', 4113, Stadium Métropolitain Pierre Quinon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1902520445376, -1.55287080054853, N'Gymnase Port-au-Blé', 4114, Gymnase Port-au-Blé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1654797011878, -1.54438743967666, N'Gymnase Arthur-Dugast', 4132, Gymnase Arthur-Dugast,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2166995009956, -1.64176082507584, N'Espace Multisport Solvardière', 4151, Espace Multisport Solvardière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2104096584847, -1.61158809549252, N'Espace Multisport Censive', 4152, Espace Multisport Censive,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.215451784303, -1.60905162935041, N'Espace Multisport Crémetterie', 4153, Espace Multisport Crémetterie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1345345540375, -1.73271134771113, N'Tennis', 4161, Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2009617615033, -1.66875796124264, N'Court de Tennis', 4166, Court de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1960700438542, -1.66910329129323, N'Cercle Nautique d''Indre', 4168, Cercle Nautique d'Indre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1717279732956, -1.71740328001877, N'Skatepark - Brains', 4171, Skatepark - Brains,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1715714181572, -1.71741348723713, N'Aire de jeux multisports Brains', 4172, Aire de jeux multisports Brains,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1480518339684, -1.52263459405555, N'Karaté', 4176, Karaté,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1574985364247, -1.52301319853266, N'Aire multisports', 4179, Aire multisports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1488585941618, -1.53387347807476, N'Aire multisports', 4180, Aire multisports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1448969479154, -1.54402353337293, N'Centre equestre Poney Club', 4188, Centre equestre Poney Club,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1895837223825, -1.47341691893707, N'Centre aquatique So.Pool', 4192, Centre aquatique So.Pool,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1498408419618, -1.69634341849071, N'Salle omnisport de Bellestre', 4193, Salle omnisport de Bellestre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1506353405133, -1.69262060484085, N'Court de Tennis', 4194, Court de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1546780883818, -1.68029644310577, N'Skatepark - Bouaye', 4196, Skatepark - Bouaye,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.260932282872, -1.45322268077049, N'Salle de sport de la Belle Etoile', 4215, Salle de sport de la Belle Etoile,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2625548892178, -1.45519991438981, N'Salle de Tennis du Guette Loup', 4216, Salle de Tennis du Guette Loup,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2631952890256, -1.45608305074378, N'Salle Omnisport', 4217, Salle Omnisport,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2185329342019, -1.46781574720677, N'Court de Tennis', 4221, Court de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.218460390894, -1.46645497803417, N'Stades de Football', 4222, Stades de Football,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2167943071233, -1.46748951112505, N'Dojo Goulainais', 4223, Dojo Goulainais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2166199319565, -1.46807115962903, N'Boulodrome', 4224, Boulodrome,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2178756582812, -1.46573542845903, N'Skatepark - Basse-Goulaine', 4226, Skatepark - Basse-Goulaine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2176312634001, -1.46578072150551, N'Terrain multisport', 4227, Terrain multisport,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1203301600343, -1.63771789549553, N'Boulodrome', 4230, Boulodrome,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1211603599007, -1.6372830000767, N'Skatepark - St-Aignan', 4231, Skatepark - St-Aignan,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1210002452753, -1.63719841649588, N'Terrain de Basket', 4232, Terrain de Basket,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1862942742528, -1.70998017391629, N'Tir à l''Arc', 4233, Tir à l'Arc,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.186997597229, -1.68685033383099, N'Skatepark - La Montagne', 4239, Skatepark - La Montagne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1632053229194, -1.46618353240723, N'Skatepark - Vertou', 4244, Skatepark - Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1639364142099, -1.46673883456256, N'Terrain de Tennis', 4245, Terrain de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1832575358085, -1.49828335754745, N'Terrain de Tennis', 4246, Terrain de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1785373270275, -1.46318253916213, N'Entente Tennis Vertou', 4247, Entente Tennis Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1652390708765, -1.46713546740473, N'Salle spécifique de gymnastique', 4249, Salle spécifique de gymnastique,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1376168981039, -1.46445035062312, N'Terrain de Pétanque des Reigners', 4252, Terrain de Pétanque des Reigners,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.137740018999, -1.46506122696991, N'Skatepark - Vertou', 4253, Skatepark - Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2662087671741, -1.67944111191122, N'Salle Antarès', 4257, Salle Antarès,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2669270963193, -1.67886425849015, N'Salle de Tennis Centaure', 4258, Salle de Tennis Centaure,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2658821247271, -1.67811198422129, N'Salle de Pétanque Electra', 4259, Salle de Pétanque Electra,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2666474812869, -1.67923508048179, N'Salle de Tennis Cassiopée', 4260, Salle de Tennis Cassiopée,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.266164291383, -1.67787222373831, N'Salle Omnisport Delta', 4261, Salle Omnisport Delta,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2660375779804, -1.67874386609388, N'Salle Omnisport Bellatrix', 4262, Salle Omnisport Bellatrix,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2665123104871, -1.67841905846459, N'Terrains de tennis', 4263, Terrains de tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2670126159199, -1.67792360967436, N'Terrains de pétanque', 4264, Terrains de pétanque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2662524329902, -1.67980908258144, N'Skatepark - Sautron', 4265, Skatepark - Sautron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2445883438036, -1.70438066734281, N'Tennis Country Club', 4281, Tennis Country Club,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2460107847443, -1.70455122124794, N'Centre équestre Poney-Club', 4282, Centre équestre Poney-Club,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2193163005765, -1.68393059268241, N'Boulodrome', 4285, Boulodrome,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2117152639444, -1.73813547120881, N'Tennis', 4286, Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2124692994787, -1.73688201873656, N'Stade René Gaudin', 4287, Stade René Gaudin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212189124209, -1.73831246720018, N'Skatepark Romain Douillard - Couëron', 4288, Skatepark Romain Douillard - Couëron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2140442390179, -1.68346476489689, N'City Stade', 4289, City Stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2855739091227, -1.4830005017521, N'Gymnase Eersel', 4291, Gymnase Eersel,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2814486517006, -1.48138210872558, N'Piste de bicross', 4292, Piste de bicross,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2822277108609, -1.48128079092291, N'Terrain de Pétanque', 4293, Terrain de Pétanque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2844351822684, -1.48341346932704, N'Tennis', 4294, Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3182432264806, -1.47070687781425, N'Stand de Tir', 4295, Stand de Tir,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3014057422845, -1.52641524899551, N'Sport Nautique de l''Ouest', 4297, Sport Nautique de l'Ouest,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3029252594488, -1.49269654987027, N'Color''ado', 4299, Color'ado,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2171288108494, -1.53530393844997, N'Gymnase Pré-Gauchet', 4307, Gymnase Pré-Gauchet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.27434615242, -1.62505307890939, N'Boulodrome', 4334, Boulodrome,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2744022434315, -1.62560460051723, N'Tennis Frébaudière', 4335, Tennis Frébaudière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2744176896823, -1.62611737840092, N'Skatepark - Orvault', 4336, Skatepark - Orvault,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2818757473023, -1.62959729620603, N'Bike-Park', 4337, Bike-Park,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2540770172418, -1.5983966005253, N'Boulodrome', 4338, Boulodrome,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2960789946463, -1.55578638712424, N'Skatepark - La Chapelle-sur-Erdre', 4354, Skatepark - La Chapelle-sur-Erdre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2981980314539, -1.55717877351555, N'Piste d''athlétisme', 4356, Piste d'athlétisme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2991404935532, -1.55900172096383, N'Boulodrome de Massigné', 4357, Boulodrome de Massigné,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2672520042777, -1.55164077434728, N'Courts de Tennis du Gesvres', 4359, Courts de Tennis du Gesvres,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2669945793363, -1.55129377312182, N'Terrain multisports de Gesvres', 4360, Terrain multisports de Gesvres,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2668362406531, -1.5510211970947, N'Plateau sportif Gesvres', 4361, Plateau sportif Gesvres,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2647855333542, -1.54243680963377, N'Poney-club de Port-Barbe', 4363, Poney-club de Port-Barbe,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2635553088015, -1.54072241592381, N'Swing-Golf de Port-Barbe', 4364, Swing-Golf de Port-Barbe,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2963662110068, -1.55784797877258, N'Halle des Sports de Mazaire', 4370, Halle des Sports de Mazaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2966402742367, -1.55853957466216, N'Salle de Gymnastique Jacques Canzillon', 4371, Salle de Gymnastique Jacques Canzillon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1710656961929, -1.62176824826453, N'Salles Duruy et Petipa', 4379, Salles Duruy et Petipa,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1713225991909, -1.62186477405135, N'Salle des Samouraïs', 4380, Salle des Samouraïs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.174852247097, -1.59066517061956, N'Skatepark - Bouguenais', 4382, Skatepark - Bouguenais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1791113975866, -1.62643250012879, N'Terrain de boules de Beauvoir', 4384, Terrain de boules de Beauvoir,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1667816209015, -1.62536528312159, N'Terrain multisports de la Pierre Blanche', 4385, Terrain multisports de la Pierre Blanche,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1778083499968, -1.6298593988431, N'Skatepark - Bouguenais', 4392, Skatepark - Bouguenais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1942539117923, -1.76211029913759, N'Espace René Cassin - salle Gilles de la Bourdonnaye', 4393, Espace René Cassin - salle Gilles de la Bourdonnaye,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1941796772969, -1.76175348369782, N'Espace René Cassin - salle Cécile Nowak', 4394, Espace René Cassin - salle Cécile Nowak,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1959542284627, -1.75847864387058, N'Terrain de basket', 4395, Terrain de basket,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1958049825886, -1.75819683229684, N'Terrain Multisports', 4396, Terrain Multisports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.200727477551, -1.75902876489526, N'Amicale Laïque du Pellerin Tir Sportif', 4397, Amicale Laïque du Pellerin Tir Sportif,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2019454287438, -1.75010863427358, N'Skatepark - Le Pellerin', 4398, Skatepark - Le Pellerin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2103948934432, -1.51306868442501, N'Terrain multisports', 4471, Terrain multisports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2934626531311, -1.39092544393487, N'Courts de Tennis', 4472, Courts de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2072019047294, -1.65639620862974, N'Centre Sportif de la Bourgonnière', 4494, Centre Sportif de la Bourgonnière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2301054930646, -1.58103296936768, N'Tennis', 4495, Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2304282772397, -1.58390074500619, N'City Stade', 4505, City Stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2296861899789, -1.5997194294491, N'Poney Club Salantine Houssay', 4517, Poney Club Salantine Houssay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2313320131578, -1.60556742386325, N'Boulodrome du Tillay', 4519, Boulodrome du Tillay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.235501355851, -1.60102340982181, N'Tennis Club La Gagnerie', 4522, Tennis Club La Gagnerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2561017302004, -1.51933111538115, N'City Stade', 4538, City Stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2170918722201, -1.52393174051278, N'City Stade', 4539, City Stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2686413253353, -1.51874002158811, N'City Stade', 4602, City Stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1829183341098, -1.67626248447447, N'Terrains de Tennis', 4778, Terrains de Tennis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2075452017814, -1.6463037572667, N'Plateau sportif de la Pelousière', 4784, Plateau sportif de la Pelousière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2667273761625, -1.6781403826469, N'Terrain Multisports', 4841, Terrain Multisports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2685005782232, -1.67138885832522, N'Circuits Rustiques d''Activites en Plein Air de Sautron', 4842, Circuits Rustiques d'Activites en Plein Air de Sautron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1646547117135, -1.53187121182908, N'Stade de la Robinière', 4866, Stade de la Robinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1853567946203, -1.55836846869043, N'Plateau multisports Château-Nord - City Stade', 4867, Plateau multisports Château-Nord - City Stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1815630236812, -1.55834625867828, N'Plateau multisports Château-Sud - City Stade', 4868, Plateau multisports Château-Sud - City Stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2117695662414, -1.49573346582352, N'Circuit d''orientation des îles de Loire', 4869, Circuit d'orientation des îles de Loire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2072356911334, -1.48711816708137, N'SkateParc - St-Sébastien', 4870, SkateParc - St-Sébastien,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1835328575081, -1.6777921490509, N'Piste de bicross', 4872, Piste de bicross,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1846447570553, -1.56287465513668, N'Terrain de pétanque', 4873, Terrain de pétanque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1348568930063, -1.67339596547012, N'Aire de jeux de l''Etier', 4876, Aire de jeux de l'Etier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1925774888357, -1.72662264628622, N'Anneau de Patin', 4887, Anneau de Patin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1456016401181, -1.52259368869916, N'Salle de danse Les Sorinières', 4897, Salle de danse Les Sorinières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2051503697534, -1.63246080038716, N'Stade Constant Pasquier', 4898, Stade Constant Pasquier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2136698307305, -1.72145169752496, N'DOJO Jean-Claude Le Quintrec', 4904, DOJO Jean-Claude Le Quintrec,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1952812640352, -1.54009352643386, N'ASC Bonne Garde - Salle Jean-Yves Leroy', 4905, ASC Bonne Garde - Salle Jean-Yves Leroy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1799156682239, -1.57176724499931, N'Salle sportive métropolitaine de Rezé', 4907, Salle sportive métropolitaine de Rezé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.180452828575, -1.57055699634047, N'City stade', 4908, City stade,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.205190372709, -1.49913445923456, N'Gymnase du centre-ville', 4910, Gymnase du centre-ville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1966702616341, -1.69835572298409, N'Piste de Bicross', 4933, Piste de Bicross,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.175939771178, -1.53770571892471, N'Plateau multisports Ouche-Dinier', 4934, Plateau multisports Ouche-Dinier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1904034053416, -1.55362978152897, N'Plateau multisports Port-au-Blé', 4935, Plateau multisports Port-au-Blé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1899071178308, -1.5710394681802, N'Plateau multisports Rezé-centre', 4936, Plateau multisports Rezé-centre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1857850930193, -1.5427302178965, N'Plateau multisports Julien Douillard', 4937, Plateau multisports Julien Douillard,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1793379437137, -1.55015206775593, N'Plateau multisports Petite-Lande', 4938, Plateau multisports Petite-Lande,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1840229062523, -1.54886930543818, N'Plateau multisports Roger Salengro', 4939, Plateau multisports Roger Salengro,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.173633354958, -1.54639170892458, N'Plateau multisports Chêne-Creux', 4940, Plateau multisports Chêne-Creux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1844282076047, -1.55814660204147, N'Plateau multisports Square Beaumarchais', 4941, Plateau multisports Square Beaumarchais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1654416152378, -1.54171072417792, N'Plateau multisports Ragon', 4942, Plateau multisports Ragon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.193981092214, -1.5841575079895, N'Plateau multisports Trentemoult', 4943, Plateau multisports Trentemoult,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1769649348868, -1.46986490619792, N'Dojo', 4949, Dojo,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1634959712178, -1.47675154133617, N'SkatePark du Loiry', 4950, SkatePark du Loiry,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1806831058717, -1.50650983201743, N'Skatepark Beautour', 4951, Skatepark Beautour,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1873870640373, -1.44807956430045, N'Centre équestre du Bois Brûlé', 4967, Centre équestre du Bois Brûlé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2806176309888, -1.63060913019945, N'Chemin du château de la Tour', 4986, Chemin du château de la Tour,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Lieux de sports et loisirs'))

