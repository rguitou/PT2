DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Equipements publics « Mobilite »')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Equipements publics « Mobilite »'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Equipements publics « Mobilite »', N'tree.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2108708271003, -1.5623381759623, N'Parking Médiathèque', 134, Parking Médiathèque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1972022454039, -1.59435499316903, N'Gare de Nantes Chantenay', 261, Gare de Nantes Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2166890401729, -1.54498433411572, N'Parking Gare Nord', 280, Parking Gare Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2174911129885, -1.54205106482655, N'Gare de Nantes -accès nord', 281, Gare de Nantes -accès nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2279684766427, -1.5516367391695, N'Gare Fluviale Bateaux Nantais', 282, Gare Fluviale Bateaux Nantais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2166626199643, -1.55400463227177, N'Parking Decré-Bouffay', 284, Parking Decré-Bouffay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2143102105068, -1.56257181621741, N'Parking Graslin', 288, Parking Graslin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2178428948398, -1.55825001902681, N'Parking Tour Bretagne', 299, Parking Tour Bretagne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2127402501474, -1.55276077185068, N'Gare Routière Baco Eurolines', 663, Gare Routière Baco Eurolines,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131202858024, -1.55771321969051, N'Parking Commerce', 677, Parking Commerce,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.216448854427, -1.54155846026517, N'Gare de Nantes -accès sud', 1042, Gare de Nantes -accès sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2170936048579, -1.56293641808053, N'Parking Aristide Briand', 1043, Parking Aristide Briand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2129016709023, -1.54397127716684, N'Parking Cité des Congrès', 1044, Parking Cité des Congrès,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2161496359544, -1.54103682846398, N'Parking Gare Sud 1', 1045, Parking Gare Sud 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2202559810468, -1.55835729990237, N'Parking Talensac', 1046, Parking Talensac,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2517352538688, -1.55290310437924, N'P+R tramway Recteur Schmitt 1', 1341, P+R tramway Recteur Schmitt 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2054007823477, -1.57398139134178, N'Capitainerie du Port Atlantique Nantes Saint Nazaire', 1581, Capitainerie du Port Atlantique Nantes Saint Nazaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2055111148953, -1.57471135472788, N'Port Autonome de Nantes Saint Nazaire', 1583, Port Autonome de Nantes Saint Nazaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2252035773148, -1.55372459801084, N'Port de l''Erdre', 1624, Port de l'Erdre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2125286072856, -1.54089899952201, N'Service Maritime et Navigation Ecluse Saint Félix', 1658, Service Maritime et Navigation Ecluse Saint Félix,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2040832390443, -1.66031284128023, N'Gare de Basse-Indre / Saint-Herblain', 1805, Gare de Basse-Indre / Saint-Herblain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1575191481928, -1.60178206983725, N'Aéroport Nantes Atlantique', 2109, Aéroport Nantes Atlantique,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1974591309304, -1.67361043471387, N'Bacs de Loire entre Basse-Indre et Indret', 2374, Bacs de Loire entre Basse-Indre et Indret,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1380452446435, -1.68110358820185, N'Gare de Bouaye', 2375, Gare de Bouaye,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2219441875147, -1.72359939991892, N'Gare de Couëron', 2452, Gare de Couëron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2059462443778, -1.75212411426107, N'Bacs de Loire entre Couëron et Le Pellerin', 2497, Bacs de Loire entre Couëron et Le Pellerin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.193331595479, -1.54939210542755, N'Gare de Rezé Pont-Rousseau', 2517, Gare de Rezé Pont-Rousseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2651201184653, -1.44096386071447, N'Gare de Thouaré-sur-Loire', 2518, Gare de Thouaré-sur-Loire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2930081697889, -1.38930347898703, N'Gare de Mauves-sur-Loire', 2519, Gare de Mauves-sur-Loire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1861434794643, -1.47803194855925, N'Gare de Vertou', 2520, Gare de Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1955772998858, -1.57837788591499, N'Port de Trentemoult', 2532, Port de Trentemoult,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2091256884984, -1.72995922379383, N'Port de Couëron', 2533, Port de Couëron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2186961365515, -1.62163336178234, N'P+R tramway Frachon', 3188, P+R tramway Frachon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2625020936155, -1.58451198079141, N'P+R tramway Le Cardo', 3189, P+R tramway Le Cardo,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.22170620087, -1.63948414305523, N'P+R tramway François Mitterrand', 3190, P+R tramway François Mitterrand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2520277017386, -1.55247038464754, N'P+R tramway Recteur Schmitt 2', 3191, P+R tramway Recteur Schmitt 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2594951760586, -1.52668095430807, N'P+R tramway La Beaujoire', 3192, P+R tramway La Beaujoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1900337520995, -1.55143945054574, N'P+R tramway 8 Mai', 3194, P+R tramway 8 Mai,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1832927458839, -1.57109558940531, N'P+R tramway Trocardière', 3195, P+R tramway Trocardière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2639509260101, -1.57499790750312, N'P+R tramway René Cassin', 3196, P+R tramway René Cassin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1972132932035, -1.54144839889655, N'P+R tramway Pirmil', 3197, P+R tramway Pirmil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1960989467374, -1.5410477469437, N'P+R tramway Goudy', 3198, P+R tramway Goudy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1906228420922, -1.49618578979617, N'P+R TER Frêne Rond', 3200, P+R TER Frêne Rond,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1864023763705, -1.47916185458301, N'P+R TER Gare de Vertou Nord', 3201, P+R TER Gare de Vertou Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1856830197378, -1.4785719721858, N'P+R TER Gare de Vertou Sud', 3202, P+R TER Gare de Vertou Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2405326620207, -1.59344052396589, N'P+R tramway Plaisance', 3227, P+R tramway Plaisance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1906163481095, -1.4958313646506, N'Gare de St-Sébastien Frêne Rond', 3353, Gare de St-Sébastien Frêne Rond,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2111796553233, -1.55843872950464, N'Parking Gloriette 1', 3549, Parking Gloriette 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2110570860299, -1.55198311230427, N'Parking Hôtel Dieu', 3550, Parking Hôtel Dieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131509800273, -1.55160425745185, N'Parking Baco-LU 2 côté CHU', 3551, Parking Baco-LU 2 côté CHU,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2082290545652, -1.51299644307533, N'P+R TER Gare des Pas Enchantés', 3552, P+R TER Gare des Pas Enchantés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2070087740385, -1.51111306351385, N'Gare de St-Sébastien Pas Enchantés', 3553, Gare de St-Sébastien Pas Enchantés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2169899609793, -1.54785044968527, N'Parking Château', 3554, Parking Château,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2149153406257, -1.539256311287, N'Parking Gare Sud 3', 3555, Parking Gare Sud 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2217771934083, -1.72363750160792, N'P+R TER Gare de Couëron', 3556, P+R TER Gare de Couëron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.176672451111, -1.59339969648243, N'P+R tramway Neustrie 2', 3558, P+R tramway Neustrie 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1952525112789, -1.57778393206355, N'P+R navibus Trentemoult-Roquios', 3560, P+R navibus Trentemoult-Roquios,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2041579741319, -1.66103809646199, N'P+R TER Gare de Basse-Indre Sud', 3561, P+R TER Gare de Basse-Indre Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1754532342509, -1.59309648041537, N'P+R tramway Neustrie 1', 3562, P+R tramway Neustrie 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2647119908848, -1.4413893536783, N'P+R TER de Thouaré-sur_Loire', 3564, P+R TER de Thouaré-sur_Loire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2932583339329, -1.388904886686, N'P+R TER Gare de Mauves-sur-Loire', 3565, P+R TER Gare de Mauves-sur-Loire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1383223686803, -1.68257543659362, N'P+R TER Gare de Bouaye', 3566, P+R TER Gare de Bouaye,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1919649895095, -1.58068768637466, N'P+R navibus Trentemoult-Dépôt', 3582, P+R navibus Trentemoult-Dépôt,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2478235218625, -1.60283874722482, N'P+R tramway Morlière Paquelais', 3583, P+R tramway Morlière Paquelais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2467766477065, -1.60304946676259, N'P+R tramway Orvault Morlière', 3584, P+R tramway Orvault Morlière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2384135213971, -1.5873437488368, N'P+R tramway Beauséjour', 3586, P+R tramway Beauséjour,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2142340366163, -1.54134337584288, N'Parking Gare Sud 2', 3589, Parking Gare Sud 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2152021232956, -1.54267513884661, N'Parking Gare Sud 2 limité 1h', 3590, Parking Gare Sud 2 limité 1h,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1801752959869, -1.50516017764927, N'P+R busway Porte de Vertou', 3627, P+R busway Porte de Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2001390392692, -1.53333362324729, N'P+R busway Gréneraie', 3628, P+R busway Gréneraie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1859742120476, -1.52133747737003, N'P+R busway Bourdonnières 1', 3629, P+R busway Bourdonnières 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1894575221521, -1.51151075562911, N'P+R busway Chapeau Verni', 3630, P+R busway Chapeau Verni,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1925010229049, -1.54828253376638, N'P+R TER Gare de Pont-Rousseau', 3648, P+R TER Gare de Pont-Rousseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2489752466115, -1.59820022515513, N'P+R tramway Bignon', 3649, P+R tramway Bignon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2223604161017, -1.55769369256355, N'Parking Bellamy', 3650, Parking Bellamy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2093619715999, -1.55725023399242, N'Parking CHU 1', 3651, Parking CHU 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2208133510721, -1.55105616832611, N'Parking Cathédrale', 3652, Parking Cathédrale,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1851690235263, -1.51067420037066, N'P+R busway Maraîchers', 3655, P+R busway Maraîchers,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2202763442702, -1.55407090754939, N'1 Station Abonnement Préfecture', 3685, 1 Station Abonnement Préfecture,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2185640015645, -1.55354426853678, N'2 Station Accueil Hôtel de Ville', 3686, 2 Station Accueil Hôtel de Ville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.216532854728, -1.55161756498412, N'3 Station Accueil Strasbourg', 3687, 3 Station Accueil Strasbourg,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2166468712003, -1.55442584710721, N'4 Station Accueil Moulin', 3688, 4 Station Accueil Moulin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2188380915037, -1.55677730034727, N'5 Station Accueil Brossard', 3689, 5 Station Accueil Brossard,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.217604415335, -1.55695366059999, N'6 Station Abonnement Place du Cirque', 3690, 6 Station Abonnement Place du Cirque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2154096299385, -1.5561003799956, N'7 Station Accueil Barillerie', 3691, 7 Station Accueil Barillerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2162104462631, -1.55713447812337, N'8 Station Abonnement Boucherie', 3692, 8 Station Abonnement Boucherie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2158349069183, -1.559060428973, N'9 Station Accueil Guépin', 3693, 9 Station Accueil Guépin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2155886269499, -1.56071869453248, N'11 Station Accueil Calvaire', 3695, 11 Station Accueil Calvaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2151581344299, -1.56396356682832, N'12 Station Abonnement et Bonus Place Delorme', 3696, 12 Station Abonnement et Bonus Place Delorme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2167962109988, -1.55918288617231, N'13 Station Abonnement et Bonus Bretagne Sud', 3697, 13 Station Abonnement et Bonus Bretagne Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2171962876433, -1.56300550348212, N'15 Station Accueil Place Aristide Briand', 3699, 15 Station Accueil Place Aristide Briand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2190280325433, -1.56349473337675, N'16 Station Accueil Place Edouard Normand', 3700, 16 Station Accueil Place Edouard Normand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.219225442242, -1.56158876841816, N'17 Station Accueil Sainte Elisabeth', 3701, 17 Station Accueil Sainte Elisabeth,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2214408780072, -1.56332698114374, N'18 Station Abonnement et Bonus Place Viarme', 3702, 18 Station Abonnement et Bonus Place Viarme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.219835952652, -1.55892890964265, N'19 Station Accueil Saint Similien', 3703, 19 Station Accueil Saint Similien,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2229117954064, -1.55724358606304, N'20 Station Abonnement Bellamy', 3704, 20 Station Abonnement Bellamy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.221154818394, -1.55649477179845, N'21 Station Accueil Marché de Talensac Sud', 3705, 21 Station Accueil Marché de Talensac Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2203790474815, -1.55558297927192, N'22 Station Accueil Moquechien', 3706, 22 Station Accueil Moquechien,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2271569726, -1.54242264341268, N'23 Station Accueil Buat', 3707, 23 Station Accueil Buat,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2245244088152, -1.553736963561, N'24 Station Accueil Versailles', 3708, 24 Station Accueil Versailles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2236126488755, -1.53010845738847, N'25 Station Abonnement Dalby', 3709, 25 Station Abonnement Dalby,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2179401852254, -1.56926720618524, N'27 Station Accueil Guist''hau Nord', 3710, 27 Station Accueil Guist'hau Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2168394453051, -1.56702335731262, N'26 Station Abonnement et Bonus Guist''hau Sud', 3711, 26 Station Abonnement et Bonus Guist'hau Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2144590279511, -1.56761075012927, N'28 Station Accueil Place de l''Edit de Nantes', 3712, 28 Station Accueil Place de l'Edit de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2134615787904, -1.55528850840637, N'29 Station Accueil Duguay Trouin', 3714, 29 Station Accueil Duguay Trouin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131450294256, -1.55738072263822, N'30 Station Abonnement Commerce', 3715, 30 Station Abonnement Commerce,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2120943808486, -1.55910527758924, N'31 Station Accueil Bourse', 3716, 31 Station Accueil Bourse,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.219342309251, -1.55027785651826, N'32 Station Accueil Place Foch', 3717, 32 Station Accueil Place Foch,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2135460456384, -1.56319280951069, N'33 Station Accueil Racine', 3718, 33 Station Accueil Racine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2109126549257, -1.5618418449525, N'34 Station Abonnement Médiathèque', 3719, 34 Station Abonnement Médiathèque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212157257031, -1.56551666094591, N'35 Station Abonnement Jean V', 3720, 35 Station Abonnement Jean V,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.210705238574, -1.5666743136651, N'36 Station Accueil Alger', 3721, 36 Station Accueil Alger,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2114728989176, -1.55688248342545, N'37 Station Accueil Feydeau', 3722, 37 Station Accueil Feydeau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2120904234566, -1.55313666650746, N'38 Station Abonnement Place Ricordeau', 3723, 38 Station Abonnement Place Ricordeau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2093654560619, -1.555248414263, N'39 Station Accueil Quai Moncousu', 3724, 39 Station Accueil Quai Moncousu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.208815874652, -1.5504304749082, N'40 Station Accueil Madeleine', 3725, 40 Station Accueil Madeleine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2083082123361, -1.56974810265731, N'41 Station Accueil Chantiers Navals', 3726, 41 Station Accueil Chantiers Navals,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2066090811753, -1.57262953237793, N'42 Station Abonnement Gare Maritime', 3727, 42 Station Abonnement Gare Maritime,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2069302605475, -1.56482580615095, N'43 Station Abonnement Machines de l''Ile', 3728, 43 Station Abonnement Machines de l'Ile,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2083631782056, -1.56124245373142, N'44 Station Accueil Palais de Justice', 3729, 44 Station Accueil Palais de Justice,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2047848192062, -1.55888284017234, N'45 Station Accueil Prairie au Duc', 3730, 45 Station Accueil Prairie au Duc,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2053380104941, -1.55520684363397, N'46 Station Accueil Place de la République', 3731, 46 Station Accueil Place de la République,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.207035364629, -1.54817985054706, N'47 Station Abonnement Martyrs Nantais', 3732, 47 Station Abonnement Martyrs Nantais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.217516769716, -1.55186817651381, N'48 Station Abonnement Verdun', 3733, 48 Station Abonnement Verdun,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2165287485861, -1.54707722079028, N'49 Station Accueil Duchesse Anne', 3734, 49 Station Accueil Duchesse Anne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2151359323236, -1.55027600234498, N'50 Station Abonnement Château', 3735, 50 Station Abonnement Château,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2143388426474, -1.54876893751898, N'52 Station Accueil Baco', 3737, 52 Station Accueil Baco,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2117494265765, -1.54995935107843, N'53 Station Accueil Olivettes', 3738, 53 Station Accueil Olivettes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131685540285, -1.54502748383111, N'54 Station Abonnement Cité Internationale des Congrès', 3739, 54 Station Abonnement Cité Internationale des Congrès,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2106839754833, -1.54424826339243, N'55 Station Accueil Magellan', 3740, 55 Station Accueil Magellan,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2068622585847, -1.54181497476041, N'56 Station Accueil Vincent Gâche', 3741, 56 Station Accueil Vincent Gâche,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2054070144366, -1.53760065399595, N'57 Station Abonnement Gaëtan Rondeau', 3742, 57 Station Abonnement Gaëtan Rondeau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2084771961567, -1.53599916068622, N'58 Station Accueil Palais des Sports', 3743, 58 Station Accueil Palais des Sports,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2139232786654, -1.53974824054074, N'59 Station Accueil Stade Saupin', 3744, 59 Station Accueil Stade Saupin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2159307421474, -1.54286596091712, N'60 Station Abonnement Gare Sud', 3745, 60 Station Abonnement Gare Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2156027850142, -1.54592421117785, N'61 Station Accueil LU Lieu Unique', 3746, 61 Station Accueil LU Lieu Unique,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2181414046142, -1.54224096840209, N'62 Station Abonnement Gare Nord', 3747, 62 Station Abonnement Gare Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2198651514153, -1.54475253537978, N'63 Station Accueil Jardins des Plantes', 3748, 63 Station Accueil Jardins des Plantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2217983948072, -1.54717172480906, N'64 Station Abonnement et Bonus Saint Clément', 3750, 64 Station Abonnement et Bonus Saint Clément,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2225988184658, -1.5516848391931, N'65 Station Accueil Cours Sully', 3751, 65 Station Accueil Cours Sully,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2254413947716, -1.54801802179078, N'66 Station Accueil Chanzy', 3752, 66 Station Accueil Chanzy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2277863381608, -1.55216613912966, N'67 Station Accueil Place Waldeck Rousseau', 3753, 67 Station Accueil Place Waldeck Rousseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2240954955051, -1.54521632095855, N'68 Station Accueil Livet', 3754, 68 Station Accueil Livet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2192276992993, -1.53586535019784, N'69 Station Accueil Manufacture', 3755, 69 Station Accueil Manufacture,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2219103419541, -1.55906934287743, N'71 Station Abonnement et Bonus Marché de Talensac nord', 3757, 71 Station Abonnement et Bonus Marché de Talensac nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2208837667212, -1.57050717785263, N'73 Station Accueil Sarradin', 3759, 73 Station Accueil Sarradin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2143716842925, -1.57500608192678, N'74 Station Accueil Canclaux', 3760, 74 Station Accueil Canclaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212365614395, -1.57139899840128, N'75 Station Accueil Lamoricière', 3761, 75 Station Accueil Lamoricière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2094420409641, -1.57058928666779, N'76 Station Accueil Place René Bouhier', 3762, 76 Station Accueil Place René Bouhier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2076169020017, -1.55755125966966, N'77 Station Accueil Ecole d''Architecture', 3763, 77 Station Accueil Ecole d'Architecture,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2084603783954, -1.54056873329161, N'78 Station Accueil De Gaulle', 3764, 78 Station Accueil De Gaulle,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2132038951461, -1.53652014877728, N'79 Station Accueil Malakoff', 3765, 79 Station Accueil Malakoff,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2459045698135, -1.61800834963946, N'P+R tramway Marcel Paul', 3776, P+R tramway Marcel Paul,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1819948350479, -1.57947203525665, N'P+R tramway Grande Ouche', 3777, P+R tramway Grande Ouche,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.186401501745, -1.52298286719509, N'P+R busway Bourdonnières 2', 3778, P+R busway Bourdonnières 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2236164437223, -1.57139236049388, N'87 Station Accueil Anatole-France', 3830, 87 Station Accueil Anatole-France,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2254822618338, -1.56752047182291, N'86 Station Accueil Hauts Pavés', 3831, 86 Station Accueil Hauts Pavés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.211489622114, -1.57712676541886, N'88 Station Accueil Mellinet', 3833, 88 Station Accueil Mellinet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074440639466, -1.57871509661417, N'89 Station Accueil Saint-Aignan', 3834, 89 Station Accueil Saint-Aignan,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2040143599928, -1.55156732506795, N'80 Station Accueil Victor Hugo', 3835, 80 Station Accueil Victor Hugo,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2009917746406, -1.54549702666218, N'81 Station Abonnement Mangin', 3836, 81 Station Abonnement Mangin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2067685846983, -1.53313636292619, N'82 Station Accueil Sébilleau', 3837, 82 Station Accueil Sébilleau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.208217819463, -1.53016141349475, N'83 Station Abonnement Millerand', 3838, 83 Station Abonnement Millerand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2107532371851, -1.52595127548116, N'84 Station Accueil Région', 3839, 84 Station Accueil Région,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2172647322169, -1.54347875084443, N'Parking Courte Durée Gare Nord', 3950, Parking Courte Durée Gare Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2059006220645, -1.56272779236372, N'Parking Les Machines', 3951, Parking Les Machines,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2265315955713, -1.56015561148945, N'85 Station Accueil et Bonus Bel Air', 3952, 85 Station Accueil et Bonus Bel Air,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2163435868477, -1.53422912101255, N'10 Station Accueil Picasso', 3973, 10 Station Accueil Picasso,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2143914699763, -1.53007250061588, N'51 Station Abonnement Tabarly', 3974, 51 Station Abonnement Tabarly,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2151033663183, -1.53793389528195, N'Parking Gare Sud 4', 3989, Parking Gare Sud 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2330917037559, -1.56601698077602, N'92 Station Abonnement Rond Point de Rennes', 3993, 92 Station Abonnement Rond Point de Rennes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2296375405847, -1.57205389018402, N'91 Station Abonnement Rond Point de Vannes', 3994, 91 Station Abonnement Rond Point de Vannes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2243215861999, -1.57658356660229, N'90 Station Abonnement Procé', 3995, 90 Station Abonnement Procé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1931492766954, -1.54900545446362, N'102 Station Abonnement Gare de Pont Rousseau', 3996, 102 Station Abonnement Gare de Pont Rousseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1966294882289, -1.54145063364339, N'101 Station Abonnement Pirmil', 3997, 101 Station Abonnement Pirmil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1958591605985, -1.53658962887906, N'99 Station Accueil Saint-Jacques', 3998, 99 Station Accueil Saint-Jacques,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1999707101929, -1.5346071537874, N'100 Station Abonnement Gréneraie', 3999, 100 Station Abonnement Gréneraie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2353448001876, -1.54947343741177, N'96 Station Abonnement Tortière', 4000, 96 Station Abonnement Tortière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2350837066755, -1.55638870204855, N'95 Station Abonnement Michelet', 4001, 95 Station Abonnement Michelet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2432626440766, -1.55638098635507, N'94 Station Abonnement Petit Port', 4002, 94 Station Abonnement Petit Port,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2463232883454, -1.5553383498054, N'93 Station Abonnement Facultés', 4003, 93 Station Abonnement Facultés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2291287626691, -1.5404024174378, N'97 Station Accueil Saint-Donatien', 4004, 97 Station Accueil Saint-Donatien,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1891485324905, -1.55093306486444, N'98 Station Abonnement Huit Mai', 4005, 98 Station Abonnement Huit Mai,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1743921106395, -1.60539271713277, N'Aire de covoiturage Piano''cktail', 4010, Aire de covoiturage Piano'cktail,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2058433058368, -1.751615681513, N'Aire de covoiturage Le Paradis', 4011, Aire de covoiturage Le Paradis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2114911474736, -1.73553587146421, N'Aire de covoiturage Le Vélodrôme', 4012, Aire de covoiturage Le Vélodrôme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1558857839796, -1.68247122952125, N'Aire de covoiturage Les Ormeaux', 4013, Aire de covoiturage Les Ormeaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1833082568459, -1.6759732987008, N'Aire de covoiturage Georges Brassens', 4014, Aire de covoiturage Georges Brassens,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1981364148521, -1.4545741556611, N'Aire de covoiturage La Herdrie', 4015, Aire de covoiturage La Herdrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2103889373405, -1.5039410584835, N'Aire de covoiturage Pas Enchantés', 4016, Aire de covoiturage Pas Enchantés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1909311959939, -1.48944982933608, N'Aire de covoiturage Les 3 Brasseurs', 4017, Aire de covoiturage Les 3 Brasseurs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2948276010662, -1.47381722266676, N'Aire de covoiturage La Charmelière', 4018, Aire de covoiturage La Charmelière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3002260614374, -1.39840430261237, N'Aire de covoiturage La Croix de Mauves', 4019, Aire de covoiturage La Croix de Mauves,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2174899073883, -1.55963354318143, N'Station Bretagne', 4020, Station Bretagne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2194053438521, -1.55722786430589, N'Station 50 Otages', 4021, Station 50 Otages,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2176490515034, -1.55407383358279, N'Station Hôtel de Ville', 4022, Station Hôtel de Ville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2172612842459, -1.56254319041665, N'Station Aristide Briand', 4023, Station Aristide Briand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2118924383634, -1.55716507497035, N'Station Commerce', 4024, Station Commerce,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2121320145481, -1.55242193688866, N'Station Baco', 4026, Station Baco,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2125030057837, -1.54620521288415, N'Station CIO', 4027, Station CIO,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2132409347, -1.54452831608963, N'Station Cité des Congrès', 4028, Station Cité des Congrès,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2149069334244, -1.54185994912442, N'Station Gare SNCF Sud', 4029, Station Gare SNCF Sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2190322362424, -1.53624925513081, N'Station Manufacture', 4030, Station Manufacture,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2344561541767, -1.53529009313813, N'Station Rond Point de Paris', 4031, Station Rond Point de Paris,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2065013480274, -1.55668647130342, N'Station Ecole d''Architecture', 4032, Station Ecole d'Architecture,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2012759690694, -1.54518063963327, N'Station Mangin', 4033, Station Mangin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2091315762997, -1.5683301930206, N'Station Chantiers Navals', 4034, Station Chantiers Navals,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2065168340528, -1.58787125667645, N'Station Mairie de Chantenay', 4035, Station Mairie de Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.214077429887, -1.58739165581353, N'Station Zola', 4036, Station Zola,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2323454380931, -1.57721870648655, N'Station Sainte Thérèse', 4037, Station Sainte Thérèse,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2165024497787, -1.54717075568781, N'Station Duchesse Anne', 4038, Station Duchesse Anne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2126755417774, -1.55276994885937, N'Gare Routière Baco Lila', 4048, Gare Routière Baco Lila,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1963923435608, -1.54267335074876, N'Gare Routière Pirmil Lila', 4049, Gare Routière Pirmil Lila,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2380795016075, -1.58686060715571, N'Gare Routière Beauséjour Lila', 4050, Gare Routière Beauséjour Lila,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2617996203739, -1.58415510201614, N'Gare Routière Cardo Lila', 4051, Gare Routière Cardo Lila,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2480211629729, -1.52143188594155, N'Gare Routière Haluchère Lila', 4052, Gare Routière Haluchère Lila,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2061177649788, -1.56886475459008, N'103 Station Mobile', 4053, 103 Station Mobile,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2141712241294, -1.5748272514646, N'Station Canclaux', 4066, Station Canclaux,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2082893195233, -1.56891560955727, N'Parking Chantiers Navals', 4072, Parking Chantiers Navals,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2102736871054, -1.56028436884679, N'Parking Gloriette 2', 4073, Parking Gloriette 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2593236730243, -1.56822710791259, N'P+R tramway Santos Dumont', 4076, P+R tramway Santos Dumont,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2286141235376, -1.56192014921017, N'72 Station Accueil Félibien Gué-Moreau', 4077, 72 Station Accueil Félibien Gué-Moreau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2744422259447, -1.54638397050348, N'Aire de covoiturage La Métaierie Rouge', 4083, Aire de covoiturage La Métaierie Rouge,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2257239248911, -1.62624641113902, N'Aire de covoiturage Norauto Atlantis', 4084, Aire de covoiturage Norauto Atlantis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2578779098455, -1.50983215080644, N'Aire de covoiturage Carrefour Beaujoire', 4099, Aire de covoiturage Carrefour Beaujoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2543632744444, -1.53313665411229, N'P+R tramway Ranzay', 4100, P+R tramway Ranzay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2484503442291, -1.52283117372235, N'P+R tram-train Haluchère 1', 4101, P+R tram-train Haluchère 1,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2143324211276, -1.54898882844897, N'Parking Baco-LU 1 côté gare', 4142, Parking Baco-LU 1 côté gare,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.226783599264, -1.55558569228364, N'Station Quai de Versailles', 4146, Station Quai de Versailles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1975346063468, -1.59463484972795, N'P+R TER / chronobus Gare de Chantenay', 4309, P+R TER / chronobus Gare de Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1773452531518, -1.59164987334643, N'P+R tramway Neustrie 3', 4310, P+R tramway Neustrie 3,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2810941460666, -1.5184745954479, N'P+R chronobus Chantrerie - Grandes Ecoles', 4311, P+R chronobus Chantrerie - Grandes Ecoles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2531977445948, -1.47497405146538, N'P+R chronobus Planchonnais', 4312, P+R chronobus Planchonnais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2661206435125, -1.43902005915263, N'P+R TER/chronobus La Noë', 4313, P+R TER/chronobus La Noë,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2732941971647, -1.44604469439019, N'P+R chronobus Pré Poulain', 4314, P+R chronobus Pré Poulain,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1367028327517, -1.72710127303152, N'Aire de covoiturage de la Mairie de Saint-Léger-Les-Vignes', 4315, Aire de covoiturage de la Mairie de Saint-Léger-Les-Vignes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2184797159027, -1.62157240555264, N'Aire de covoiturage Frachon', 4316, Aire de covoiturage Frachon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2529809902532, -1.47507408048735, N'Aire de covoiturage de la Planchonnais', 4317, Aire de covoiturage de la Planchonnais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2049704647258, -1.54433295436521, N'Parking Les Fonderies', 4319, Parking Les Fonderies,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2140752998708, -1.55255878352104, N'Parking Feydeau', 4320, Parking Feydeau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2595965577586, -1.54622476875122, N'P+R tram-train Babinière', 4340, P+R tram-train Babinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2595557429368, -1.54559364953307, N'Gare tram-train Babinière', 4341, Gare tram-train Babinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2978015412892, -1.54771562542556, N'P+R tram-train La Chapelle-sur-Erdre Centre Est', 4342, P+R tram-train La Chapelle-sur-Erdre Centre Est,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2974860226491, -1.54808020326863, N'Gare tram-train La Chapelle-Centre', 4343, Gare tram-train La Chapelle-Centre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3083888002604, -1.54104919057664, N'P+R tram-train La Chapelle-Aulnay Est', 4344, P+R tram-train La Chapelle-Aulnay Est,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3086811231164, -1.54151123189203, N'Gare tram-train La Chapelle-Aulnay', 4345, Gare tram-train La Chapelle-Aulnay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2831147498899, -1.544344098146, N'P+R tram-train La Chapelle Erdre Active', 4346, P+R tram-train La Chapelle Erdre Active,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2828497270964, -1.54452233078279, N'Gare tram-train Erdre-Active', 4347, Gare tram-train Erdre-Active,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2968761970388, -1.54876772911246, N'P+R tram-train La Chapelle-sur-Erdre Centre Ouest', 4553, P+R tram-train La Chapelle-sur-Erdre Centre Ouest,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3079307861029, -1.54367742164992, N'P+R tram-train La Chapelle-Aulnay Ouest', 4554, P+R tram-train La Chapelle-Aulnay Ouest,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2043736318374, -1.66058829388856, N'P+R TER Gare de Basse-Indre Nord', 4555, P+R TER Gare de Basse-Indre Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1470165062331, -1.51857237049049, N'P+R chronobus Papillons', 4556, P+R chronobus Papillons,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2154852089099, -1.54293730193395, N'70 Station Accueil Gare Sud 2', 4562, 70 Station Accueil Gare Sud 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.228935299112, -1.54052829088581, N'Station Saint-Donatien', 4563, Station Saint-Donatien,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074400449769, -1.54017656001374, N'Station Tripode', 4564, Station Tripode,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2244525630143, -1.57128842131348, N'Station Sourdille', 4565, Station Sourdille,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2329047361932, -1.56631991306045, N'Station Rond-Point Rennes', 4568, Station Rond-Point Rennes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1957981746053, -1.54346489622615, N'Pirmil (consigne)', 4610, Pirmil (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1925455277881, -1.54875232924884, N'Pont Rousseau (collectif sécurisé)', 4611, Pont Rousseau (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1892249669309, -1.55184420816495, N'8 Mai (collectif abrité)', 4612, 8 Mai (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1833772889033, -1.57029190539333, N'Trocardière (consigne)', 4613, Trocardière (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1817223736954, -1.57958857608825, N'Grande ouche (consigne)', 4614, Grande ouche (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1768126188673, -1.59310087276357, N'Neustrie (consigne)', 4615, Neustrie (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1996720650986, -1.53310758222232, N'Gréneraie (collectif abrité)', 4617, Gréneraie (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1997403128977, -1.53317264504497, N'Gréneraie (collectif sécurisé)', 4618, Gréneraie (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1902113576369, -1.52540142790408, N'Mauvoisins (consigne)', 4619, Mauvoisins (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1865473532912, -1.52140876908587, N'Bourdonnières (consignes)', 4620, Bourdonnières (consignes),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1806310732837, -1.50530476926916, N'Porte de Vertou (consigne)', 4622, Porte de Vertou (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2069030184953, -1.5112538842536, N'Pas Enchantés (collectif abrité)', 4623, Pas Enchantés (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2066446600389, -1.51055928386024, N'Pas enchantés abrité (collectif abrité)', 4624, Pas enchantés abrité (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1903041814266, -1.49603983660328, N'Frêne Rond (collectif sécurisé)', 4625, Frêne Rond (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1903642806595, -1.49588744088622, N'Frêne Rond (collectif abrité)', 4626, Frêne Rond (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1862775643044, -1.47798363700849, N'Gare de Vertou (collectif abrité)', 4627, Gare de Vertou (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1858485633002, -1.47801423897547, N'Gare de Vertou (collectif sécurisé)', 4628, Gare de Vertou (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2161546501301, -1.54186762001945, N'Gare Sud (collectif abrité)', 4629, Gare Sud (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2161850343243, -1.54167126990616, N'Gare Sud (collectif sécurisé)', 4630, Gare Sud (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2173064334307, -1.54344396660319, N'Gare nord (collectif abrité)', 4631, Gare nord (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.259187277673, -1.52566317911064, N'Beaujoire (consigne)', 4632, Beaujoire (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2930095075134, -1.38964150683711, N'Gare de Mauves (consigne)', 4633, Gare de Mauves (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2520617543628, -1.55248886690146, N'Recteur Schmitt (consigne)', 4634, Recteur Schmitt (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2618808709343, -1.5920230802473, N'Grand Val (consigne)', 4635, Grand Val (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2477519931616, -1.60278903485486, N'Morlière Paquelais (consigne)', 4636, Morlière Paquelais (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2456958650382, -1.61758050075146, N'Marcel Paul (collectif abrité)', 4637, Marcel Paul (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2489560952294, -1.5978903457391, N'Bignon (consigne)', 4638, Bignon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2408711985673, -1.59305566366749, N'Plaisance (collectif abrité)', 4639, Plaisance (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2179397943275, -1.6142614951031, N'Neruda (consigne)', 4640, Neruda (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2220041796063, -1.63924595395762, N'Mitterrand (consigne)', 4641, Mitterrand (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.296766258262, -1.49175134677826, N'Place du vieux cimetiere (consigne)', 4642, Place du vieux cimetiere (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1998976565815, -1.57282043522859, N'Hangar à bananes (consigne)', 4643, Hangar à bananes (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.19643496899, -1.54289738351578, N'Pirmil (collectif sécurisé)', 4644, Pirmil (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1965912137759, -1.54258773105948, N'Pirmil (collectif abrité)', 4645, Pirmil (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1850493577924, -1.5106947981432, N'Maraicher (consigne)', 4646, Maraicher (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.168887571532, -1.47124094524049, N'Vertou gare routière (consigne)', 4647, Vertou gare routière (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1782613214724, -1.54414757085278, N'3 Moulins (consigne)', 4648, 3 Moulins (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.148577159754, -1.53119246068493, N'Place de la Mairie (consigne)', 4649, Place de la Mairie (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1842394225963, -1.56267840330244, N'Diderot (consigne)', 4650, Diderot (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.265075184061, -1.4407738737368, N'Gare de Thouaré (collectif abrité)', 4651, Gare de Thouaré (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2499301821665, -1.52329496712317, N'Haluchère (collectif sécurisé)', 4652, Haluchère (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2493238043208, -1.52351733617212, N'Haluchère (collectif abrité)', 4653, Haluchère (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2530223101536, -1.53010640990383, N'Ranzay (collectif sécurisé)', 4654, Ranzay (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2591209564933, -1.54570443679286, N'Babinière (collectif sécurisé)', 4655, Babinière (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2641310470309, -1.57525334895343, N'René Cassin (consigne)', 4659, René Cassin (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2531588402103, -1.57648890806529, N'Bout des Pavés (consigne)', 4660, Bout des Pavés (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2487487442748, -1.57694786773576, N'Pont du Cens (consigne)', 4661, Pont du Cens (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2709528330289, -1.62362612028793, N'Place Eglise (consigne)', 4662, Place Eglise (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2643013045783, -1.67954132186688, N'Sautron terminus (consigne)', 4663, Sautron terminus (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2165826086764, -1.46662847481612, N'Rue du Grignon (consigne)', 4664, Rue du Grignon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2016906204319, -1.75914765630664, N'Le Pellerin terminus ligne express (consigne)', 4665, Le Pellerin terminus ligne express (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2666841537853, -1.60019286196922, N'Bois Raguenet (consigne)', 4666, Bois Raguenet (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2272623959969, -1.51864649049279, N'Mairie Doulon (consigne)', 4667, Mairie Doulon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1940516169258, -1.52885900806199, N'Clos Toreau (consigne)', 4668, Clos Toreau (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2319371786797, -1.51567406613419, N'Landreau (consigne)', 4669, Landreau (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2593409223094, -1.56817518277345, N'Santos Dumont (consigne)', 4670, Santos Dumont (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1894640076848, -1.51713731231873, N'Joliverie (consigne)', 4671, Joliverie (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1974776824377, -1.59432064444371, N'Gare de Chantenay (consigne)', 4672, Gare de Chantenay (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2093793060207, -1.59590574852204, N'Croix bonneau (sécurisé)', 4673, Croix bonneau (sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2096388936158, -1.58893567488989, N'Egalité (sécurisé)', 4674, Egalité (sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1648832031388, -1.48057265753418, N'Grammoire (consigne)', 4675, Grammoire (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1753881988944, -1.62381734883414, N'Croix Jeannette (consigne)', 4676, Croix Jeannette (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1810398624057, -1.58619217039791, N'Les Couets (consigne)', 4677, Les Couets (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2130698340315, -1.59874322411545, N'Durantière piscine (consigne)', 4679, Durantière piscine (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1884394201389, -1.68921544482741, N'La Montagne Pompiers (consigne)', 4680, La Montagne Pompiers (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1654311212652, -1.48624898868314, N'Route des Sorinières (consigne)', 4681, Route des Sorinières (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2139678449895, -1.58630587536017, N'Zola (consigne)', 4682, Zola (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2501416083484, -1.48494815620147, N'Sainte Luce De Gaulle (consigne)', 4683, Sainte Luce De Gaulle (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1768261386391, -1.59299763887125, N'Neustrie (consigne)', 4684, Neustrie (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2220008415411, -1.63953075892652, N'Mitterrand (consigne)', 4685, Mitterrand (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2930302010769, -1.38965327436549, N'Gare de Mauves (consigne)', 4686, Gare de Mauves (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.189347335877, -1.51129606262763, N'Chapeau vernis (consigne)', 4687, Chapeau vernis (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2385865683461, -1.5875971384072, N'Beausejour (consigne)', 4688, Beausejour (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2309462592658, -1.5567462861965, N'St Félix (consigne)', 4689, St Félix (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.234739677265, -1.55679331148077, N'Michelet (consigne)', 4691, Michelet (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1647711452281, -1.46937482308132, N'Piscine (consigne)', 4692, Piscine (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.165370765273, -1.54222052093205, N'Ragon (consigne)', 4693, Ragon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2532938084695, -1.47486330373227, N'Papotière (consigne)', 4694, Papotière (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2570468971606, -1.46604870833721, N'La Minais (consigne)', 4695, La Minais (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2352385611695, -1.50293546936691, N'Place du Vieux Doulon (consigne)', 4696, Place du Vieux Doulon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1976766950814, -1.5943298425406, N'Gare de Chantenay (consigne)', 4697, Gare de Chantenay (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1956612294567, -1.72505654870234, N'Rue du Prieuré (consigne)', 4698, Rue du Prieuré (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1571883814583, -1.52299322796899, N'Maillardière (consigne)', 4699, Maillardière (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1467213882786, -1.51891022584835, N'Papillon (consigne)', 4700, Papillon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2822590665995, -1.51786707198478, N'La Chantrerie (abrité)', 4702, La Chantrerie (abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2821196424734, -1.51767304799494, N'La Chantrerie (sécurisé)', 4703, La Chantrerie (sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1381998229963, -1.68130210658714, N'Gare Bouaye (abri plus sécurisé)', 4704, Gare Bouaye (abri plus sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2042349000559, -1.66014731884854, N'Gare de Basse Indre (consigne)', 4705, Gare de Basse Indre (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2062719751404, -1.57271646451042, N'Gare maritime (consigne)', 4707, Gare maritime (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2170735573853, -1.54387627842632, N'Gare nord parking (sécurisé)', 4708, Gare nord parking (sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2171385191864, -1.54391839978149, N'Gare nord parking (abrité)', 4709, Gare nord parking (abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1913349733155, -1.71207075561716, N'Saint Jean de Boiseau (consigne)', 4710, Saint Jean de Boiseau (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2379873688937, -1.51609083959552, N'Souillarderie (consigne)', 4711, Souillarderie (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2531822038242, -1.57599666234132, N'Bout des Pavés (consigne)', 4712, Bout des Pavés (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1931315247372, -1.54879737366258, N'Pont Rousseau (consigne)', 4713, Pont Rousseau (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131821023372, -1.60022961829846, N'Durantière (consigne)', 4714, Durantière (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2120549891611, -1.72697210155762, N'Coueron Even (consigne)', 4715, Coueron Even (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1952588177616, -1.57811263716694, N'Trentmoult Roquios (collectif abrité)', 4716, Trentmoult Roquios (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2188916275261, -1.6202838660535, N'Frachon (collectif abrité)', 4717, Frachon (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.217858407968, -1.61402386984358, N'Neruda (collectif abrité)', 4718, Neruda (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2042882753782, -1.66033541953291, N'Gare de Basse Indre (collectif abrité)', 4720, Gare de Basse Indre (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1381893242158, -1.68120547935829, N'Gare de Bouaye (collectif abrité)', 4721, Gare de Bouaye (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2217826034187, -1.72352737769722, N'Gare de Coueron (collectif abrité)', 4722, Gare de Coueron (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.264990123961, -1.44112177559474, N'Gare de Thouaré (collectif abrité)', 4723, Gare de Thouaré (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.185862956777, -1.47804195086816, N'Gare de Vertou (collectif abrité)', 4724, Gare de Vertou (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2066731263519, -1.51061578215125, N'Pas enchantés abrité (collectif abrité)', 4725, Pas enchantés abrité (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1956815426243, -1.54079167962344, N'Goudy (collectif abrité)', 4726, Goudy (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1972833040936, -1.59460859492635, N'Gare de Chantenay (collectif abrité)', 4727, Gare de Chantenay (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2150965053519, -1.53799063632782, N'Gare sud 4 (collectif sécurisé)', 4728, Gare sud 4 (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2623914543365, -1.58440437097679, N'Le Cardo (collectif sécurisé)', 4729, Le Cardo (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.221943737127, -1.63953854745182, N'Mitterrand (collectif sécurisé)', 4730, Mitterrand (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2136446365615, -1.54408370542533, N'Cité des Congrès (collectif sécurisé)', 4731, Cité des Congrès (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2133194962897, -1.55780980234499, N'Commerce (collectif sécurisé)', 4732, Commerce (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2166660527285, -1.55423648573523, N'Decré / Bouffay (collectif sécurisé)', 4733, Decré / Bouffay (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2210180982701, -1.55126379714879, N'Foch Cathédrale (collectif sécurisé)', 4734, Foch Cathédrale (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2145970301062, -1.56229634339863, N'Graslin (collectif sécurisé)', 4735, Graslin (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2058257915273, -1.56258620817132, N'Machines (collectif sécurisé)', 4736, Machines (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2108425003292, -1.56230756152206, N'Médiathèque (collectif sécurisé)', 4737, Médiathèque (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2199027391761, -1.55769331688577, N'Talensac (collectif sécurisé)', 4738, Talensac (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2178042795128, -1.55831595116776, N'Bretagne (collectif sécurisé)', 4739, Bretagne (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2498994338929, -1.52325028828019, N'Haluchère (collectif abrité)', 4740, Haluchère (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2492933098569, -1.52349198770571, N'Haluchère (collectif asécurisé)', 4741, Haluchère (collectif asécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2530662115111, -1.53019927103207, N'Ranzay (collectif abrité)', 4742, Ranzay (collectif abrité),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2457700226023, -1.6175244770897, N'Marcel Paul (collectif sécurisé)', 4743, Marcel Paul (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1770829434896, -1.59312665701531, N'Neustrie (appui-vélo)', 4744, Neustrie (appui-vélo),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2380680494216, -1.5865089094778, N'Beauséjour (appui-vélo)', 4745, Beauséjour (appui-vélo),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1617797634634, -1.54191715193937, N'Corbinerie (consigne)', 4746, Corbinerie (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1477807782299, -1.52097760737545, N'Rouet (consigne)', 4747, Rouet (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1694366374064, -1.72293208387035, N'Rue des prés (consigne)', 4748, Rue des prés (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2222061302278, -1.60636612247471, N'Saint Herblain (consigne)', 4749, Saint Herblain (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.213890217221, -1.5526947054328, N'Neptune (collectif sécurisé)', 4750, Neptune (collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2132213753839, -1.5579201758517, N'Locations vélos Commerce', 4751, Locations vélos Commerce,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2169551727437, -1.54411278187315, N'Locations vélos Gare Nord', 4752, Locations vélos Gare Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2490411369368, -1.52259272941917, N'Gare tram-train Haluchère-Batignolles', 4760, Gare tram-train Haluchère-Batignolles,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2502300597845, -1.52497519560003, N'P+R tram-train Haluchère 2', 4764, P+R tram-train Haluchère 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.246903862863, -1.49495327567164, N'P+R chronobus Bois des Anses', 4765, P+R chronobus Bois des Anses,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2881528637428, -1.48386529862208, N'Moulin Boiseau (consigne)', 4766, Moulin Boiseau (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3040869777935, -1.48519147433257, N'Rue de Chateaubriand (consigne)', 4767, Rue de Chateaubriand (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2483227755784, -1.55141569040084, N'Ecole centrale / Sup de co (consigne)', 4768, Ecole centrale / Sup de co (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2425404874668, -1.53388785803026, N'Combattants d''Indochine (consigne)', 4769, Combattants d'Indochine (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2448336791445, -1.53286211850843, N'Keren (consigne)', 4770, Keren (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2676809581996, -1.51630018461917, N'Embellie (consigne)', 4802, Embellie (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2621363171528, -1.58426843671376, N'Le Cardo (consigne)', 4803, Le Cardo (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2615916064894, -1.57312981216415, N'Chêne des Anglais (consigne)', 4804, Chêne des Anglais (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2142903874097, -1.60795751446787, N'Tertre  (abri collectif)', 4805, Tertre  (abri collectif),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2214435807437, -1.56299414987565, N'Viarme/Talensac (consigne)', 4806, Viarme/Talensac (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2240354178422, -1.51990689935172, N'Bd. Doulon (consigne)', 4807, Bd. Doulon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2073374595625, -1.57259271840638, N'Gare Maritime (consigne)', 4808, Gare Maritime (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2030346687676, -1.57105494638846, N'Terminus C5 (consigne)', 4809, Terminus C5 (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.20406132891, -1.56585693674417, N'Prairie au Duc (consigne)', 4810, Prairie au Duc (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2056044971589, -1.5543433441152, N'République (consigne)', 4811, République (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2046247987917, -1.5536263768254, N'Gustave Roch (consigne)', 4812, Gustave Roch (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2066557172681, -1.53325746313532, N'Conservatoire (consigne)', 4813, Conservatoire (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2095692458573, -1.52705222903656, N'Pompidou (consigne)', 4814, Pompidou (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2833166873894, -1.54479104024594, N'Gare Erdre Active (abri collectif sécurisé)', 4815, Gare Erdre Active (abri collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2972769090855, -1.54799668663311, N'Gare Chapelle Centre (abri collectif sécurisé)', 4816, Gare Chapelle Centre (abri collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3061824898911, -1.54358501752658, N'Gare Chapelle Aulnay (abri collectif sécurisé)', 4817, Gare Chapelle Aulnay (abri collectif sécurisé),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1919519945294, -1.6967031767221, N'Chat qui guette (consigne)', 4818, Chat qui guette (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2137614033697, -1.56423877233188, N'Station Ladmirault', 4819, Station Ladmirault,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2208742958692, -1.56144843610386, N'Station Viarme', 4820, Station Viarme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2202558442878, -1.51585135578417, N'P+R Chronobus Prairie de Mauves', 4914, P+R Chronobus Prairie de Mauves,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2038729344448, -1.58080233199404, N'Station Sainte Anne', 4921, Station Sainte Anne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2051025882241, -1.55509166043531, N'Station République', 4922, Station République,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2212983457253, -1.55675468556435, N'Station Talensac', 4923, Station Talensac,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2212820798647, -1.54723195160433, N'Station Saint Clement', 4924, Station Saint Clement,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2142869619048, -1.56923615633877, N'Station Gigant', 4925, Station Gigant,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074899244075, -1.57193743729686, N'Station Maille Brezé', 4926, Station Maille Brezé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2123097339951, -1.57150370623196, N'Station Lamoricière', 4927, Station Lamoricière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.21218771847, -1.55894459500407, N'Station La Bourse', 4928, Station La Bourse,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2200953735482, -1.45218875356649, N'Aire de covoiturage Ile Chaland', 4929, Aire de covoiturage Ile Chaland,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2470700767198, -1.49508474866663, N'Aire de covoiturage Bois des Anses', 4930, Aire de covoiturage Bois des Anses,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2201417786501, -1.51637273497636, N'Prairie de Mauves (consigne)', 4973, Prairie de Mauves (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2135344666285, -1.73922897811637, N'Océan (consigne)', 4974, Océan (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1245474366259, -1.61848016100407, N'Champ de Foire (consigne)', 4975, Champ de Foire (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2305164911809, -1.61695006466537, N'Hermeland (consigne)', 4977, Hermeland (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2455229801379, -1.60828187602942, N'Angevinière (consigne)', 4978, Angevinière (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2749820576904, -1.63707835336758, N'Chapelle des Anges (consigne)', 4979, Chapelle des Anges (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3061939001201, -1.56026174347757, N'Chapelle sur Erdre (consigne)', 4980, Chapelle sur Erdre (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1949288803406, -1.57789557873385, N'Quai Surcouf (consigne)', 4981, Quai Surcouf (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1844768525316, -1.55951819223863, N'François Mitterrand (consigne)', 4982, François Mitterrand (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1897325569342, -1.47222231312136, N'Piscine (consigne)', 4983, Piscine (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2153004845288, -1.4755437753354, N'Grignon (consigne)', 4984, Grignon (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2467680035039, -1.49555909322296, N'Métro (consigne)', 4985, Métro (consigne),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1630787490619, -1.62779544174512, N'Aire de covoiturage La Ville au Denis', 5021, Aire de covoiturage La Ville au Denis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.208890253019, -1.55291388704137, N'Parking CHU 2', 5022, Parking CHU 2,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2157649874154, -1.56111806795314, N'Station Calvaire', 5024, Station Calvaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2161075134917, -1.55842433069549, N'Station Arche Sèche', 5025, Station Arche Sèche,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1435536587747, -1.67983811595075, N'Aire de covoiturage Super U de Bouaye', 5029, Aire de covoiturage Super U de Bouaye,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Equipements publics « Mobilite »'))

