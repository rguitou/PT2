DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Défibrilateurs')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Défibrilateurs'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Défibrilateurs', N'tree.png	', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2187036842052, -1.55374214495589, N'Mairie centrale', 1, Mairie centrale,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2187036842052, -1.55374214495589, N'Mairie centrale', 1, Mairie centrale,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2110628582813, -1.56209573371648, N'Médiathèque Jacques Demy', 2, Médiathèque Jacques Demy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2110628582813, -1.56209573371648, N'Médiathèque Jacques Demy', 2, Médiathèque Jacques Demy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2110628582813, -1.56209573371648, N'Médiathèque Jacques Demy', 2, Médiathèque Jacques Demy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2143981351171, -1.55775173962693, N'Pharmacie de Paris', 3, Pharmacie de Paris,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131534938734, -1.56179859209062, N'Pharmacie du Théâtre', 4, Pharmacie du Théâtre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2162860836856, -1.55278646431271, N'Pharmacie du Pilori', 5, Pharmacie du Pilori,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212810135305, -1.56502534069262, N'Muséum d''Histoire Naturelle', 6, Muséum d'Histoire Naturelle,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2157155472174, -1.55874650914908, N'Pharmacie Saint Nicolas,', 7, Pharmacie Saint Nicolas,,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2187050420096, -1.55103216649778, N'Pharmacie de la Cathédrale', 8, Pharmacie de la Cathédrale,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2040015427375, -1.60604184098012, N'Pharmacie des Lauriers', 9, Pharmacie des Lauriers,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2022445356877, -1.57752413333852, N'Le Planétarium', 10, Le Planétarium,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2022445356877, -1.57752413333852, N'Le Planétarium', 10, Le Planétarium,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2022445356877, -1.57752413333852, N'Le Planétarium', 10, Le Planétarium,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2009919817805, -1.61475418961946, N'Plaine de jeux des Bernadières', 11, Plaine de jeux des Bernadières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2018596860047, -1.61124986032484, N'Stade Espérance St Yves de Nantes', 12, Stade Espérance St Yves de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2139996379326, -1.58517837696622, N'Pharmacie Zola', 13, Pharmacie Zola,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2139987639602, -1.60527130828514, N'Complexe sportif de la Durantière', 14, Complexe sportif de la Durantière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2259325509894, -1.59130209590487, N'Plaine de jeux des Dervallières', 15, Plaine de jeux des Dervallières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2212897984276, -1.55797564403486, N'Marché de Talensac', 17, Marché de Talensac,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2212897984276, -1.55797564403486, N'Marché de Talensac', 17, Marché de Talensac,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.226207282256, -1.55424803839414, N'Maison de l''Erdre', 18, Maison de l'Erdre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2142312013418, -1.52765381590718, N'Pharmacie Devenyns', 19, Pharmacie Devenyns,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2190756970979, -1.54858696084694, N'Chapelle de l''Oratoire', 20, Chapelle de l'Oratoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2045591538685, -1.54236575181672, N'Complexe sportif Mangin Beaulieu', 21, Complexe sportif Mangin Beaulieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2095211264995, -1.53676424812643, N'Palais de sport de Beaulieu', 22, Palais de sport de Beaulieu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2618198977058, -1.57395979463886, N'Médiathèque Luce Courville', 23, Médiathèque Luce Courville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2618198977058, -1.57395979463886, N'Médiathèque Luce Courville', 23, Médiathèque Luce Courville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2618198977058, -1.57395979463886, N'Médiathèque Luce Courville', 23, Médiathèque Luce Courville,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2636669467046, -1.58084277948458, N'Salle festive Nantes Nord', 24, Salle festive Nantes Nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2676545571455, -1.58727469896092, N'Plaine de jeux des Basses landes', 25, Plaine de jeux des Basses landes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2624789354554, -1.57417999383924, N'Stade de l''Amande', 26, Stade de l'Amande,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2720652181797, -1.51680168082364, N'Complexe sportif Saint Joseph de Porterie', 27, Complexe sportif Saint Joseph de Porterie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2541248409219, -1.53131689304087, N'Salle festive Nantes Erdre', 28, Salle festive Nantes Erdre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2404092840676, -1.53341982337189, N'Stade Joseph Geoffroy le vivier', 29, Stade Joseph Geoffroy le vivier,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2473417622859, -1.53537637602438, N'Plaine de jeux de l''Eraudière', 30, Plaine de jeux de l'Eraudière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2408662853878, -1.51276882457303, N'Médiathèque Floresca Guépin', 31, Médiathèque Floresca Guépin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2408662853878, -1.51276882457303, N'Médiathèque Floresca Guépin', 31, Médiathèque Floresca Guépin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2408662853878, -1.51276882457303, N'Médiathèque Floresca Guépin', 31, Médiathèque Floresca Guépin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2408662853878, -1.51276882457303, N'Médiathèque Floresca Guépin', 31, Médiathèque Floresca Guépin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2350528901241, -1.52286331267299, N'Centre  sportif du Croissant', 32, Centre  sportif du Croissant,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2402911977274, -1.51892991965329, N'Pharmacie de la Bottière', 33, Pharmacie de la Bottière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2352578989345, -1.50296330435194, N'Pharmacie Boudrot', 34, Pharmacie Boudrot,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2306386879226, -1.52746369698488, N'Plaine de jeux de la Noè Lambert', 35, Plaine de jeux de la Noè Lambert,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2396407832622, -1.50387300163076, N'Stade Jean-Jacques Audubon  Audubon', 36, Stade Jean-Jacques Audubon  Audubon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2419558163148, -1.52005502790642, N'Stade Pin Sec', 37, Stade Pin Sec,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2318803367929, -1.50026148551293, N'Stade du RACC', 38, Stade du RACC,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.185091077843, -1.53124486939568, N'Plaine de jeux de Sèvres', 39, Plaine de jeux de Sèvres,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2016088636473, -1.57741157588825, N'Musée Jules Verne', 40, Musée Jules Verne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2016088636473, -1.57741157588825, N'Musée Jules Verne', 40, Musée Jules Verne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2016088636473, -1.57741157588825, N'Musée Jules Verne', 40, Musée Jules Verne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2197423443237, -1.55845793439292, N'CCAS de Nantes', 41, CCAS de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2144470398653, -1.56269916897997, N'Espace Cosmopolis', 42, Espace Cosmopolis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2144470398653, -1.56269916897997, N'Espace Cosmopolis', 42, Espace Cosmopolis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074581336141, -1.53275560601546, N'Conservatoire de Nantes', 43, Conservatoire de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074581336141, -1.53275560601546, N'Conservatoire de Nantes', 43, Conservatoire de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074581336141, -1.53275560601546, N'Conservatoire de Nantes', 43, Conservatoire de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074581336141, -1.53275560601546, N'Conservatoire de Nantes', 43, Conservatoire de Nantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2287253694111, -1.58784444066793, N'pharmacie Quancard ( Breil )', 44, pharmacie Quancard ( Breil ),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2595478760813, -1.5668478390367, N'pharmacie Santos Dumont (Petite Sensive )', 45, pharmacie Santos Dumont (Petite Sensive ),(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2209220900046, -1.54404099810684, N'Jardin des plantes', 46, Jardin des plantes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2281789956507, -1.51241136655742, N'Parc du Grand Blottereau', 47, Parc du Grand Blottereau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.219403053717, -1.53537753320436, N'Salles municipales La Manu', 48, Salles municipales La Manu,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2234719179191, -1.59742809004464, N'Mairie Annexe des Dervalières', 49, Mairie Annexe des Dervalières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2234719179191, -1.59742809004464, N'Mairie Annexe des Dervalières', 49, Mairie Annexe des Dervalières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2234719179191, -1.59742809004464, N'Mairie Annexe des Dervalières', 49, Mairie Annexe des Dervalières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2124934982058, -1.60029595368333, N'Plaine de Jeux Durantière', 50, Plaine de Jeux Durantière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2012861675173, -1.57285496573142, N'LC CLUB Hangar à Banane', 51, LC CLUB Hangar à Banane,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2098799723372, -1.56005333982138, N'Piscine Léo Lagrange', 52, Piscine Léo Lagrange,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2482752504886, -1.51745529394017, N'Piscine Jules Verne', 53, Piscine Jules Verne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.227375903824, -1.59202950185885, N'Piscine Dervallières', 54, Piscine Dervallières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2163824454658, -1.5317220105222, N'Piscine Petite Amazonie', 55, Piscine Petite Amazonie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212771695217, -1.59874337804225, N'Piscine Durantière', 56, Piscine Durantière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Défibrilateurs'))

