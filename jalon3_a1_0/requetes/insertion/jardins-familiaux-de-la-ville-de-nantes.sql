DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Aires de jeux')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Aires de jeux'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Aires de jeux', N'themepark.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2194390174894, -1.52344149022095, N' LA ROCHE', 4013.0, CHEMIN DE LA ROCHE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2565927290361, -1.56309409770614, N' CRESSONNIERE', 1176.76, RUE DES RENARDS,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2121267611117, -1.5481177365621, N'LAIT DE MAI', 182.76, RUE EMILE PEHANT,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2290656488421, -1.58786484344719, N'SUZANNE LENGLEN', 532.4, RUE SUZANNE LENGLEN,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2281754211676, -1.58576356458799, N' BREIL/COUBERTIN', 1743.14, BD PIERRE DE COUBERTIN,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2420588343492, -1.51645836884369, N'COLLINES', 7683.06, CHEMIN DES COLLINES,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2351474602204, -1.52309170665746, N'CROISSANT', 15808.73, RUE DU CROISSANT,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1911183070505, -1.52996452304157, N'CRAPAUDINE', 23222.69, AVENUE DES GOBELETS,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1957882470328, -1.52862142777457, N'LE JARDINET', 98.4, RUE D'HENDAYE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2556186549767, -1.52052713916583, N'HALVEQUE/BEAUJOIRE', 713.68, RUE LEON SERPOLLET,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2160658560964, -1.52439613841858, N'Le GARDEN''KOFF', 83.76, SQUARE DE HONGRIE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1946771332856, -1.54264748322921, N'LES ESCARTONS', 805.92, RUE GABRIEL GOUDY,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2109178835222, -1.56292365129831, N'PAPOTAGER (Square G. CHEREAU)', 858.74, RUE DE L'HERRONNIERE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2174186557603, -1.56585543942584, N'AL LIORZH VOUTIN', 66.71, 3 RUE HARROUYS,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2457364697555, -1.49076498693019, N'BOIS DES ANSES', 9239.9, CHEMIN DES ANSES,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2007077764372, -1.58598583645468, N'LES OBLATES', 1656.88, RUE DE LA BRIANDERIE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2674981253864, -1.57651900533873, N'LES BRUYERES', 11305.78, RUE DE PONT AVEN,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2036054083011, -1.54824126685915, N'LE 16 WATT', 125.99, RUE PETITE BIESSE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2097304589474, -1.52537316012955, N'PRAIRIE D''AMONT', 168.78, ALLEE ALAIN GERBAULT,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2072259347066, -1.60610832852431, N'JAMET', 642.08, RUE DU JAMET,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2218544645677, -1.53292618840642, N' MOUTONNERIE', 3829.54, RUE FRANSCISCO FERRER,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2470833841652, -1.51248950668165, N'JARDIN PARTAGE LA HALUCHERE LE PERRAY', 75.91, 1 RUE JULES GRANDJOUAN,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2264348486334, -1.59932090627181, N'JARDIN D''USAGE DERVALLIERES', 2029.65, RUE LOUIS LE NAIN,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2280953065597, -1.59402433638019, N'CARCOUET', 492.78, Null,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.212537479885, -1.58895939752634, N'FOURNILIERE', 31604.69, RUE JULES PIEDELEU,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2189973138641, -1.60087770314206, N'CONTRIE', 17043.26, RUE DU CORPS DE GARDE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2021953533342, -1.59877123488103, N'BOUCARDIERE', 2030.87, RUE DE L'ABBAYE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2717876097935, -1.57164946642059, N'ANGLE CHAILLOU', 14469.12, CHEMIN ANGLE CHAILLOU,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.228702444088, -1.52084080364442, N'EPINETTES', 1677.66, RUE DES EPINETTES,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2293984322158, -1.59654089683538, N'CHEZINE', 3217.96, BD DU MASSACRE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2664476306773, -1.57356790329742, N'EGLANTIERS', 22967.0, RTE DE LA CHAPELLE SUR ERDRE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2540385280162, -1.52402318387499, N' BATIGNOLLES', 7283.64, BD DES BATIGNOLLES,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2205751821284, -1.50911365979517, N'TERRE PROMISE', 14600.15, CHEMIN DES BATELIERS,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2037889595427, -1.61293860413442, N'BOIS DE LA MUSSE', 12293.24, RUE DU BOIS DE LA MUSSE,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2020816053686, -1.54694666084343, N'VERTAIS', 1117.92, RUE VERTAIS,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2446375693427, -1.49939897422361, N'CHAUPIERES', 16037.68, CHEMIN DES CHAUPIERES,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2626386020172, -1.57701678864409, N'AMANDE', 4754.45, BD RENE CASSIN,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2420259121648, -1.53884312855537, N'PORT BOYER', 211.8, RUE DU PORT BOYER,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

