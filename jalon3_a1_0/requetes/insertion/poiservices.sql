DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Etablissements et lieux de services')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Etablissements et lieux de services'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Etablissements et lieux de services', N'townhouse.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.585049695541402, 44.8371585218766, N'Boite à lire Charles de Gaulle', 303, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.615155400111274, 44.8551956416444, N'Chartreuse Saint-André', 304, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576607851283129, 44.8394335307844, N'Direction de l''Education et de la Famille', 305, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.610431648956764, 44.832534389868, N'Salle municipale Saint Augustin', 306, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.579328082777599, 44.8384014894585, N'Salle du conseil municipal', 309, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.579259003121565, 44.8372686614414, N'Direction du Developpement Economique', 310, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.614530320324729, 44.8517279930712, N'Marché biologique Saint Amand', 311, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.547844413135479, 44.8233162587839, N'Marché d''Intérêt National de Brienne', 313, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569017648592563, 44.8586238603121, N'Centre Communal d''Action Sociale', 316, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.54425035861305, 44.8773058652869, N'Salle du Point du jour - Pierre Tachou', 317, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.553154204605407, 44.8419699344399, N'Mairie de quartier Bastide', 322, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.546072471224482, 44.8731635522534, N'Mairie de quartier Bordeaux maritime', 325, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576667471050264, 44.8393599928234, N'Pôle Senior', 258, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.585427373479835, 44.8427779571766, N'Marché de producteurs Saint Seurin ', 260, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.538569945864039, 44.8535266171796, N'Marché du pont Saint Emilion', 261, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.623372658370374, 44.857723071085, N'Marché des Pins Francs', 262, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.578967101232711, 44.8380143922047, N'Hôtel de Ville - Mairie de Bordeaux', 263, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.579068066239043, 44.837246462191, N'Direction de l''hygiène et de la santé', 264, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566037588447421, 44.8345307555752, N'Marché Grand déballage de brocante', 268, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565859905629018, 44.8342650811066, N'Marché Brocante', 269, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.580817035337291, 44.826631010018, N'Boite à lire Dames de la foi', 270, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.559684736921922, 44.8427130084219, N'Marché du soir', 271, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571581993668337, 44.8205514852391, N'Espace Malbec', 274, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569266102530499, 44.8503695673889, N'Marché des Quais', 278, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.573727654083752, 44.832158371451, N'La Maison du vélo', 279, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577574635672463, 44.8430920144003, N'Marché des Grands Hommes', 280, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576054148144555, 44.8379707387084, N'Direction de la voie publique', 281, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581523403135147, 44.8303951011265, N'Salle Amédée Larrieu', 282, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552826291856799, 44.8415226462949, N'Maison cantonale', 284, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.566428060381344, 44.8344939399031, N'Marché Royal', 286, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.615539437301113, 44.8522683486889, N'Mairie de quartier Caudéran', 287, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.583850275277527, 44.8445725333847, N'Objets trouvés', 288, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581695281883485, 44.8358745334539, N'Médiateur municipal', 289, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581018500059903, 44.8595105258144, N'Marché du Grand-Parc', 290, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.594864573622442, 44.8489222194278, N'Salle Bel Orme', 291, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576708971490915, 44.8394309834071, N'Direction de la Jeunesse et des Sports', 330, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.599776772003479, 44.8238625549885, N'Salle Quintin Loucheur', 332, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576473963962176, 44.8391722475163, N'Mairie de quartier Centre', 333, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581662512278139, 44.8588278248397, N'Mairie de quartier Grand Parc - Paul Doumer / Salle du Grand Parc', 335, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.552922832865945, 44.8755181833746, N'Point ambulant René Maran', 1053, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.584135504363048, 44.8419180010666, N'Boite à lire Martyrs de la Résistance', 350, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.61116565826689, 44.8330379505253, N'Mairie de quartier St Augustin - Tauzin - Alphonse Dupeux', 352, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577601923582269, 44.8230472358663, N'Boite à lire Simiot', 239, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.576755706239224, 44.837827003262, N'Marché Pey-Berland', 240, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.553711292430094, 44.8470277999255, N'Archives Bordeaux Métropole', 241, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.558928994964873, 44.8445778389579, N'Boite à lire Linné', 242, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577537713666743, 44.8428588278169, N'Marché des bouquinistes', 243, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.551785991648534, 44.8249917317115, N'Salle Son Tay', 244, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.569429254443178, 44.8500863308005, N'Marché biologique des quais', 245, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.593797650759427, 44.8330489945994, N'Marché Gaviniès', 246, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.553243151206588, 44.8414424463707, N'Boite à lire Maison cantonale', 247, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572385652441936, 44.8520110029462, N'Marché Chartrons', 248, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.546567769610619, 44.8725706816828, N'Marché de la Lumineuse', 249, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567228552086396, 44.8300543896571, N'Marché des Capucins (plein air)', 250, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.588414148371775, 44.8307882779734, N'Marché Saint Victor - Dupeux', 251, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.550010030307075, 44.8428648453426, N'Marché Calixte Camelle', 252, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577066813575453, 44.8397551348796, N'Athénée Père Joseph Wresinski', 253, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.579361463304491, 44.8372930914343, N'Direction des Achats et Marchés', 254, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565808748432821, 44.834509486202, N'Marché Neuf', 255, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.570554406407709, 44.8332376968981, N'Atelier Informatique', 256, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.571377355822913, 44.8680782164796, N'Pôle technique municipal', 927, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581336309122116, 44.8476847002843, N'Annexe mairie de quartier Grand Parc - Paul Doumer', 946, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.581940302490375, 44.8382546883646, N'Cité municipale', 991, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.577772713425404, 44.8250836011734, N'Marché Pouyanne', 995, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.610921087845461, 44.832775305603, N'Marché Saint Augustin', 1000, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567709203895385, 44.8305189115768, N'Marché des Capucins (couvert)', 1007, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.585719001589454, 44.8500575163413, N'Marché Marie Brizard', 1012, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565705739936691, 44.8588857712845, N'Marché Saint Martial', 1016, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.572469377482487, 44.8523930229474, N'Marché des bouquinistes', 1020, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.567102518521291, 44.8387480472717, N'Maison écocitoyenne', 978, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.55820346310275, 44.8270612716625, N'Mairie de quartier Bordeaux Sud', 1051, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(-0.565741241408244, 44.8338038383195, N'Annexe de la mairie de quartier Bordeaux Sud', 1052, services,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Etablissements et lieux de services'))

