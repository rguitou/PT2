DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Mobiliers urbains : Stationnement deux-roues')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Mobiliers urbains : Stationnement deux-roues'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Mobiliers urbains : Stationnement deux-roues', N'parking_bicycle-2.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.231937260431, -1.51567410784259, N'Véloparc Landreau', 4669, Véloparc Landreau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2593409385311, -1.56817516887492, N'Véloparc Santos Dumont', 4670, Véloparc Santos Dumont,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1894640822829, -1.51713736252049, N'Véloparc Joliverie', 4671, Véloparc Joliverie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1974776928454, -1.59432063221902, N'Véloparc Gare de Chantenay', 4672, Véloparc Gare de Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2093793132118, -1.59590571933794, N'Véloparc Croix Bonneau', 4673, Véloparc Croix Bonneau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2096389969219, -1.58893567189702, N'Véloparc Egalité', 4674, Véloparc Egalité,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1648832280468, -1.48057280724925, N'Véloparc Grammoire', 4675, Véloparc Grammoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1753883030413, -1.62381748548106, N'Véloparc Croix Jeannette', 4676, Véloparc Croix Jeannette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.181039990182, -1.58619215993453, N'Véloparc Les Couëts', 4677, Véloparc Les Couëts,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2130698600424, -1.5987433387711, N'Véloparc Durantière piscine', 4679, Véloparc Durantière piscine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1884394787049, -1.68921547002753, N'Véloparc La Montagne Pompiers', 4680, Véloparc La Montagne Pompiers,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2139678598976, -1.58630574491059, N'Véloparc Zola', 4682, Véloparc Zola,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1768127291306, -1.5931008293991, N'Véloparc Neustrie', 4684, Véloparc Neustrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2220008775796, -1.63953071741886, N'Véloparc Mitterrand', 4685, Véloparc Mitterrand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1893474163775, -1.51129607032802, N'Véloparc Chapeau vernis', 4687, Véloparc Chapeau vernis,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2385866318548, -1.5875971686372, N'Véloparc Beauséjour', 4688, Véloparc Beauséjour,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2309463760357, -1.5567463855788, N'Véloparc Saint Félix', 4689, Véloparc Saint Félix,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2347397901383, -1.55679328902158, N'Véloparc Michelet', 4691, Véloparc Michelet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1653708798508, -1.54222050358576, N'Véloparc Ragon', 4693, Véloparc Ragon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2532938421275, -1.4748634235678, N'Véloparc Papotière', 4694, Véloparc Papotière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2355803942804, -1.50396829907476, N'Véloparc Place du Vieux Doulon', 4696, Véloparc Place du Vieux Doulon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1976767160305, -1.59432983478804, N'Véloparc Gare de Chantenay', 4697, Véloparc Gare de Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1919584946688, -1.69665947853245, N'Véloparc Rue du Prieuré', 4698, Véloparc Rue du Prieuré,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1571884416442, -1.52299322686504, N'Véloparc Maillardière', 4699, Véloparc Maillardière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1477594251696, -1.52095767496549, N'Véloparc Papillons', 4700, Véloparc Papillons,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2822591301244, -1.5178668774436, N'Véloparc La Chantrerie', 4702, Véloparc La Chantrerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.28211961337, -1.51767306531112, N'Véloparc La Chantrerie', 4703, Véloparc La Chantrerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1381893646589, -1.68120557976484, N'Véloparc Gare Bouaye', 4704, Véloparc Gare Bouaye,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2062720037518, -1.57271641865286, N'Véloparc Gare maritime', 4707, Véloparc Gare maritime,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2173064933529, -1.54344400552645, N'Parking Gare nord', 4708, Parking Gare nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2173064933529, -1.54344400552645, N'Parking Gare nord', 4709, Parking Gare nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1913349417818, -1.71207078123501, N'Véloparc Saint Jean de Boiseau', 4710, Véloparc Saint Jean de Boiseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2379873571532, -1.51609092250636, N'Véloparc Souillarderie', 4711, Véloparc Souillarderie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2531822297812, -1.57599658083877, N'Véloparc Bout des Pavés', 4712, Véloparc Bout des Pavés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1931316450845, -1.54879735462731, N'Véloparc Pont Rousseau', 4713, Véloparc Pont Rousseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2131821420844, -1.60022968798057, N'Véloparc Durantière', 4714, Véloparc Durantière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2120550962799, -1.72697212765254, N'Véloparc Coueron Even', 4715, Véloparc Coueron Even,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1952588328813, -1.57811271276998, N'Véloparc Trentemoult Roquios', 4716, Véloparc Trentemoult Roquios,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2188442339937, -1.62147107652606, N'Véloparc Frachon', 4717, Véloparc Frachon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2179398953252, -1.61426165370435, N'Véloparc Neruda', 4718, Véloparc Neruda,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.138233013834, -1.68128801895795, N'Véloparc Gare de Bouaye', 4721, Véloparc Gare de Bouaye,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2217826543592, -1.72352735205662, N'Véloparc Gare de Coueron', 4722, Véloparc Gare de Coueron,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2650752390025, -1.44077389451817, N'Véloparc Gare de Thouaé', 4723, Véloparc Gare de Thouaé,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1858630613677, -1.47804183990209, N'Véloparc Gare de Vertou', 4724, Véloparc Gare de Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2066732361951, -1.51061580824369, N'Véloparc Pas enchantés', 4725, Véloparc Pas enchantés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1956815594464, -1.54079172323638, N'Véloparc Goudy', 4726, Véloparc Goudy,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1972833739648, -1.59460859999199, N'Véloparc Gare de Chantenay', 4727, Véloparc Gare de Chantenay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2150965513618, -1.53799068249962, N'Parking Gare sud 4', 4728, Parking Gare sud 4,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2623915691058, -1.5844044179263, N'Véloparc Le Cardo', 4729, Véloparc Le Cardo,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2220042659758, -1.63924587675425, N'Véloparc Mitterrand', 4730, Véloparc Mitterrand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2136446636446, -1.54408374199817, N'Parking Cité des Congrès', 4731, Parking Cité des Congrès,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.213319560977, -1.55780982208116, N'Parking Commerce', 4732, Parking Commerce,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2166660839753, -1.55423649455925, N'Parking Decré / Bouffay', 4733, Parking Decré / Bouffay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2210181386105, -1.55126383584909, N'Parking Foch Cathédrale', 4734, Parking Foch Cathédrale,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2145970240282, -1.56229636076085, N'Parking Graslin', 4735, Parking Graslin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2058258245926, -1.56258627690313, N'Parking les Machines', 4736, Parking les Machines,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2108424882587, -1.56230757471655, N'Parking Médiathèque', 4737, Parking Médiathèque,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2199027543669, -1.55769327865413, N'Parking Talensac', 4738, Parking Talensac,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2178043413193, -1.55831606085806, N'Parking Bretagne', 4739, Parking Bretagne,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2498994730616, -1.52325034035658, N'Véloparc Haluchère', 4740, Véloparc Haluchère,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.249293353142, -1.52349209556796, N'Véloparc Haluchère', 4741, Véloparc Haluchère,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.253066262606, -1.53019929058564, N'Véloparc Ranzay', 4742, Véloparc Ranzay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2457701105181, -1.61752450479839, N'Véloparc Marcel Paul', 4743, Véloparc Marcel Paul,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1617798231931, -1.54191716817065, N'Véloparc Corbinerie', 4746, Véloparc Corbinerie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1467359959817, -1.51893503253335, N'Véloparc Rouet', 4747, Véloparc Rouet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1694367224748, -1.72293193757284, N'Véloparc Rue des prés', 4748, Véloparc Rue des prés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2140734974024, -1.55260876888063, N'Parking Feydeau', 4750, Parking Feydeau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2677968258863, -1.51652046932564, N'Véloparc Embellie', 4802, Véloparc Embellie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2618404296482, -1.58460891525021, N'Véloparc Le Cardo', 4803, Véloparc Le Cardo,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2616163779118, -1.57362164399881, N'Véloparc Chêne des Anglais', 4804, Véloparc Chêne des Anglais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.214316930441, -1.60795992997358, N'Véloparc Tertre', 4805, Véloparc Tertre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2216185521861, -1.56321508406336, N'Véloparc Place Viarme', 4806, Véloparc Place Viarme,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2239764550617, -1.52011737388487, N'Véloparc Boulevard de Doulon', 4807, Véloparc Boulevard de Doulon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2074270791265, -1.57229237397549, N'Véloparc Gare maritime', 4808, Véloparc Gare maritime,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2030749648796, -1.57106210055004, N'Véloparc Terminus C5', 4809, Véloparc Terminus C5,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2041969586565, -1.56616893195696, N'Véloparc Nefs', 4810, Véloparc Nefs,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2054870235548, -1.55402518483397, N'Véloparc République', 4811, Véloparc République,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2046518654793, -1.55347806693278, N'Véloparc Estuaire', 4812, Véloparc Estuaire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2067154027306, -1.53330100699943, N'Véloparc Rondeau', 4813, Véloparc Rondeau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2096989089444, -1.52688306609604, N'Véloparc Pompidou', 4814, Véloparc Pompidou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2831992364226, -1.54510118861784, N'Véloparc Erdre Active', 4815, Véloparc Erdre Active,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2967086994232, -1.54644430351977, N'Véloparc La Chapelle Centre', 4816, Véloparc La Chapelle Centre,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.308156276979, -1.54057348092828, N'Véloparc La Chapelle Aulnay', 4817, Véloparc La Chapelle Aulnay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1918930115432, -1.69670853238009, N'Véloparc Chat qui Guette', 4818, Véloparc Chat qui Guette,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2424484776003, -1.53400727373513, N'Véloparc Keren', 9994, Véloparc Keren,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2448567083933, -1.53292145249854, N'Véloparc Combattants d''Indochine', 9995, Véloparc Combattants d'Indochine,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2483007918198, -1.55133822554646, N'Véloparc Ecole centrale / Sup de co', 9996, Véloparc Ecole centrale / Sup de co,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2883262304367, -1.48380414406438, N'Véloparc Moulin Boiseau', 9997, Véloparc Moulin Boiseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.3040652091942, -1.48525929968038, N'Véloparc Rue de Chateaubriand', 9998, Véloparc Rue de Chateaubriand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1957981981634, -1.54346490376234, N'Véloparc Pirmil', 4610, Véloparc Pirmil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1925455510581, -1.54875226628812, N'Véloparc Pont Rousseau', 4611, Véloparc Pont Rousseau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.189225088926, -1.5518442182848, N'Véloparc 8 Mai', 4612, Véloparc 8 Mai,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1833774143391, -1.57029191851611, N'Véloparc Trocardière', 4613, Véloparc Trocardière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1817224604807, -1.57958862288985, N'Véloparc Grande Ouche', 4614, Véloparc Grande Ouche,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1770829665086, -1.59312668658823, N'Véloparc Neustrie', 4615, Véloparc Neustrie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1997403522979, -1.53317272421536, N'Véloparc Gréneraie', 4617, Véloparc Gréneraie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1996721215657, -1.53310743182552, N'Véloparc Gréneraie', 4618, Véloparc Gréneraie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1902113662143, -1.52540149554347, N'Véloparc Mauvoisins', 4619, Véloparc Mauvoisins,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1865465644797, -1.52134782557861, N'Véloparc Bourdonnières', 4620, Véloparc Bourdonnières,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1806310934808, -1.50530479993814, N'Véloparc Porte de Vertou', 4622, Véloparc Porte de Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2066732361951, -1.51061580824369, N'Véloparc Pas Enchantés', 4623, Véloparc Pas Enchantés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2069031127285, -1.51125384645695, N'Véloparc Pas enchantés', 4624, Véloparc Pas enchantés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1903041792141, -1.49603988548747, N'Véloparc Frêne Rond', 4625, Véloparc Frêne Rond,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.190364294955, -1.49588749652743, N'Véloparc Frêne Rond', 4626, Véloparc Frêne Rond,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1871764179109, -1.47805848962049, N'Véloparc Gare de Vertou', 4627, Véloparc Gare de Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1858486168993, -1.47801431544503, N'Véloparc Gare de Vertou', 4628, Véloparc Gare de Vertou,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2161850979805, -1.54167126556066, N'Parking Gare sud', 4629, Parking Gare sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2161546650016, -1.54186774795315, N'Parking Gare sud', 4630, Parking Gare sud,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2173064933529, -1.54344400552645, N'Parking Gare nord', 4631, Parking Gare nord,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2591873598768, -1.52566308281591, N'Véloparc Beaujoire', 4632, Véloparc Beaujoire,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2930302642636, -1.38965342888177, N'Véloparc Gare de Mauves', 4633, Véloparc Gare de Mauves,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.25206180062, -1.5524888134172, N'Véloparc Recteur Schmitt', 4634, Véloparc Recteur Schmitt,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2618809236725, -1.59202310769683, N'Véloparc Grand Val', 4635, Véloparc Grand Val,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2477520504792, -1.60278901987683, N'Véloparc Morlère Paquelais', 4636, Véloparc Morlère Paquelais,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.245695909162, -1.61758063248526, N'Véloparc Marcel Paul', 4637, Véloparc Marcel Paul,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2489561519406, -1.59789049093617, N'Véloparc Bignon', 4638, Véloparc Bignon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2623915691058, -1.5844044179263, N'Véloparc Plaisance', 4639, Véloparc Plaisance,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2179398953252, -1.61426165370435, N'Véloparc Neruda', 4640, Véloparc Neruda,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2219438356381, -1.63953863242532, N'Véloparc Mitterrand', 4641, Véloparc Mitterrand,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2967663339907, -1.49175142200904, N'Véloparc Place du vieux cimetiere', 4642, Véloparc Place du vieux cimetiere,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1998976758326, -1.57282041883639, N'Véoparc Hangar à Bananes', 4643, Véoparc Hangar à Bananes,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.196591294791, -1.54258769083711, N'Véloparc Pirmil', 4644, Véloparc Pirmil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1964350327837, -1.54289743545996, N'Véloparc Pirmil', 4645, Véloparc Pirmil,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1850494827108, -1.51069490053269, N'Véloparc Maraïchers', 4646, Véloparc Maraïchers,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1688875533168, -1.47124091965104, N'Véloparc Vertou gare routière', 4647, Véloparc Vertou gare routière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1782613949234, -1.54414760060643, N'Véloparc 3 Moulins', 4648, Véloparc 3 Moulins,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1485771482426, -1.5311923911134, N'Véloparc Place de la Mairie', 4649, Véloparc Place de la Mairie,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1842394693886, -1.56267848853425, N'Véloparc Diderot', 4650, Véloparc Diderot,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2649902250913, -1.44112172521562, N'Véloparc Gare de Thouaré', 4651, Véloparc Gare de Thouaré,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2499302800357, -1.52329504806882, N'Véloparc Haluchère', 4652, Véloparc Haluchère,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2493239290813, -1.52351731628185, N'Véloparc Haluchère', 4653, Véloparc Haluchère,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2530223711641, -1.53010645631533, N'Véloparc Ranzay', 4654, Véloparc Ranzay,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2591210062983, -1.54570457157977, N'Véloparc Babinière', 4655, Véloparc Babinière,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2641310953292, -1.57525342982189, N'Véloparc René Cassin', 4659, Véloparc René Cassin,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2624642244533, -1.57728200307714, N'Véloparc Bout des Pavés', 4660, Véloparc Bout des Pavés,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2487488717167, -1.57694781744934, N'Véloparc Pont du Cens', 4661, Véloparc Pont du Cens,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2709528785161, -1.62362612201397, N'Véloparc Place Eglise', 4662, Véloparc Place Eglise,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2643013727743, -1.67954137543637, N'Véloparc Sautron terminus', 4663, Véloparc Sautron terminus,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2165826439858, -1.46662844042804, N'Véloparc Rue du Grignon', 4664, Véloparc Rue du Grignon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2016906522285, -1.75914777165346, N'Véloparc Le Pellerin terminus ligne express', 4665, Véloparc Le Pellerin terminus ligne express,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2666841936055, -1.60019286945204, N'Véloparc Bois Raguenet', 4666, Véloparc Bois Raguenet,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.2272624698571, -1.51864645980369, N'Véloparc Mairie Doulon', 4667, Véloparc Mairie Doulon,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(47.1940516820235, -1.52885905186375, N'Véloparc Clos Toreau', 4668, Véloparc Clos Toreau,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Mobiliers urbains : Stationnement deux-roues'))

