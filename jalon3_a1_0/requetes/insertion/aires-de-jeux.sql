DELETE FROM POINTS WHERE Num_Categorie IN (SELECT Num_Categorie FROM CATEGORIES WHERE Nom_Categorie = 'Aires de jeux')

DELETE FROM CATEGORIES WHERE Nom_Categorie='Aires de jeux'

INSERT INTO CATEGORIES(Nom_Categorie, Chemin_icone, Date_Publication, Num_Ville) VALUES(N'Aires de jeux', N'themepark.png', '01/01/2017',1)

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6053025485, 1.42959546869, N'BAZACLE 2', A0005, 1,42959546869,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6486196573, 1.43188582865, N'PAUL RICHE', A0034, 1,43188582865,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5794383525, 1.4298758559, N'ILE DU RAMIER', A0081, 1,4298758559,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5740991045, 1.41835400444, N'MERLETTES', A0087, 1,41835400444,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5739461995, 1.45285176411, N'JULES JULIEN THEATRE', A0124, 1,45285176411,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5869987465, 1.48200442625, N'MONT  AIGUAL', A0129, 1,48200442625,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6057201344, 1.42566128863, N'BAZACLE 1', A0009, 1,42566128863,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6126763406, 1.43251484313, N'COMPANS', A0012, 1,43251484313,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6091742949, 1.43786480313, N'EMBARTHE', A0016, 1,43786480313,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6260310934, 1.46829541892, N'MICHOUN', A0029, 1,46829541892,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6417163766, 1.44168380202, N'RIGAL', A0039, 1,44168380202,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6239589106, 1.46823533181, N'ROSINE BET', A0040, 1,46823533181,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6107866162, 1.46097780373, N'VILLA MERICANT', A0046, 1,46097780373,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5883402533, 1.38520739177, N'ALEMBERT', A0048, 1,38520739177,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5603976409, 1.3970369147, N'DOTTIN', A0057, 1,3970369147,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5850248711, 1.42235498373, N'FONTAINES LESTANG', A0072, 1,42235498373,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5839749654, 1.40902983553, N'GIRONDE', A0077, 1,40902983553,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5901642434, 1.41051183906, N'HIPPODROME CEPIERE', A0080, 1,41051183906,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5825766215, 1.40866461646, N'ORNE', A0090, 1,40866461646,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5612387866, 1.39767229974, N'TINTORET', A0055, 1,39767229974,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.58180897, 1.44258144519, N'DASTE BAT 17', A0116, 1,44258144519,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.578404265, 1.43882173344, N'EMPALOT MAIL', A0119, 1,43882173344,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5704709163, 1.44833526074, N'LE FRANC DE POMPIGNAN', A0125, 1,44833526074,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5884884217, 1.44147796796, N'PAYS D''OC', A0131, 1,44147796796,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5948305558, 1.44197349516, N'PORT-GARAUD', A0135, 1,44197349516,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5834677971, 1.46976352913, N'SAINT-EXUPERY', A0141, 1,46976352913,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6032198763, 1.44963152946, N'PLACE  OCCITANE', A0153, 1,44963152946,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5796382523, 1.45909866629, N'SAOUZELONG', A0155, 1,45909866629,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5806099254, 1.41589991822, N'LA FAOURETTE', A0164, 1,41589991822,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6351575953, 1.45105761418, N'JARDIN DE GAVE', A0170, 1,45105761418,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6083519301, 1.45101105497, N'CASTELET', A0176, 1,45101105497,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6222972879, 1.4774591186, N'ARGOULETS  TENNIS', A0180, 1,4774591186,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5807538648, 1.46194992771, N'BEDOUCE', A0177, 1,46194992771,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5631177336, 1.4136086301, N'GIRONIS  PARCOURS SPORTIF', A0197, 1,4136086301,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5503372209, 1.38616025846, N'TIBAOUS', A0201, 1,38616025846,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5866021435, 1.38125219369, N'CATALA', A0210, 1,38125219369,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5809897959, 1.41534349223, N'LA FAOURETTE 2', A0215, 1,41534349223,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5759719768, 1.43767039896, N'PARC  POUDRERIE', A0219, 1,43767039896,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5806497195, 1.41174853654, N'LA MARTINIQUE', A0083, 1,41174853654,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5618990916, 1.39601511959, N'CANDELIS', A0220, 1,39601511959,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6394974661, 1.45344608855, N'BORDEROUGE', A0224, 1,45344608855,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6288942074, 1.44964932423, N'FROIDURE', A0149, 1,44964932423,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6174520843, 1.40642023022, N'PARC JOB', A0033, 1,40642023022,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6361517134, 1.46159142313, N'RAMELET', A0037, 1,46159142313,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.598420073, 1.38916150446, N'BALUFFET', A0050, 1,38916150446,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5943702738, 1.40806865116, N'BERGOUGNAN', A0102, 1,40806865116,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5698607078, 1.40084726428, N'DOUVES', A0068, 1,40084726428,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6018207904, 1.37035266558, N'GRIMAUD', A0070, 1,37035266558,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5938766402, 1.43588787347, N'PRAIRIE DES FILTRES', A0095, 1,43588787347,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5811470601, 1.49203178417, N'SARABELLE', A0142, 1,49203178417,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.631318113, 1.46384166881, N'AVRANCHES', A0004, 1,46384166881,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6132304415, 1.42597535167, N'BEARNAIS', A0006, 1,42597535167,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6419976483, 1.46195886379, N'EBELOT', A0015, 1,46195886379,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6361453984, 1.44283040732, N'ERNEST RENAN', A0017, 1,44283040732,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6358671671, 1.43195306804, N'FERDINAND FAURE', A0020, 1,43195306804,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.615951411, 1.45453583194, N'MICHELET', A0028, 1,45453583194,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6184787191, 1.46526549211, N'PIERRE ROUS', A0035, 1,46526549211,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6137486759, 1.41697169367, N'PONTS JUMEAUX', A0036, 1,41697169367,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6080053666, 1.47761484262, N'SEYVEROLLES', A0043, 1,47761484262,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6013571445, 1.40230943923, N'BARRY', A0051, 1,40230943923,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6013339, 1.41929258218, N'FONTAINES', A0071, 1,41929258218,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5740166805, 1.39847387547, N'REYNERIE COLLINES', A0099, 1,39847387547,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.58782409, 1.41614238778, N'VESTREPAIN', A0108, 1,41614238778,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6006876082, 1.47421806627, N'ACHIARY', A0191, 1,47421806627,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5897030594, 1.49130168283, N'GRANDE PLAINE', A0121, 1,49130168283,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5806677025, 1.44085776682, N'MENTON', A0128, 1,44085776682,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5734620915, 1.4873095448, N'PELLETRIE 1', A0132, 1,4873095448,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5946661054, 1.46027674489, N'RASPAIL', A0138, 1,46027674489,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6254990409, 1.46901972682, N'SQUARE DU TOIT ROULANT', A0158, 1,46901972682,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6424741525, 1.44423559677, N'JARDIN  BLEU', A0169, 1,44423559677,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5631775077, 1.40470830704, N'LES  MURIERS', A0174, 1,40470830704,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5620314416, 1.40074683722, N'LETITIEN  GOYA', A0168, 1,40074683722,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5764427508, 1.41169412533, N'PAPUS', A0179, 1,41169412533,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5553719843, 1.44956711761, N'PECH DAVID 2', A0182, 1,44956711761,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6322271869, 1.4273569988, N'CERVANTES', A0186, 1,4273569988,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5838611682, 1.40712919073, N'RONSARD', A0189, 1,40712919073,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6047161333, 1.40966894846, N'BAUDIN', A0052, 1,40966894846,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5662877763, 1.34428899464, N'BASE DE LOISIRS DE LA RAMEE', A0213, 1,34428899464,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6424929572, 1.44031310324, N'RIGAL 2', A0216, 1,44031310324,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6379399617, 1.44273555265, N'MICOULAUD PLACE', A0217, 1,44273555265,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5838015439, 1.40697945472, N'CAMPAN', A0223, 1,40697945472,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5499007418, 1.38333918121, N'TIBAOUS 2', A0226, 1,38333918121,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6220355105, 1.43864852209, N'ANGES', A0002, 1,43864852209,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6350675092, 1.48892730989, N'GRAMONT', A0023, 1,48892730989,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.621479098, 1.43421916882, N'NOUGARO', A0031, 1,43421916882,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6122368615, 1.46318057644, N'OBSERVATOIRE', A0032, 1,46318057644,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6034138055, 1.45545642131, N'SAINT AUBIN', A0145, 1,45545642131,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6022238845, 1.44779099738, N'SAINT GEORGES', A0041, 1,44779099738,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5782721458, 1.3861738283, N'CEDRES 2', A0147, 1,3861738283,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5821930379, 1.40572532341, N'CHER', A0063, 1,40572532341,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5567840589, 1.3804164636, N'GARDERIE ST SIMON', A0075, 1,3804164636,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.589481436, 1.3773865106, N'SAUVEGRAIN', A0103, 1,3773865106,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5813529696, 1.39943836052, N'VERGERS', A0107, 1,39943836052,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5801830151, 1.45958842317, N'BONNAT', A0113, 1,45958842317,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5712048197, 1.50279666655, N'MARCAISSONNE', A0146, 1,50279666655,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6277068579, 1.42751622251, N'FONQUERNE PLACE', A0021, 1,42751622251,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6190020786, 1.45208485476, N'LAPUJADE', A0027, 1,45208485476,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6024986741, 1.41653580686, N'BOURRASSOL', A0058, 1,41653580686,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5621855658, 1.41415321912, N'GIRONIS', A0078, 1,41415321912,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5804438424, 1.39658455462, N'MAILLOL RESIDENCE', A0085, 1,39658455462,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6062692161, 1.38667190818, N'MARMANDE', A0086, 1,38667190818,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6096315359, 1.3795473523, N'PLATANES', A0092, 1,3795473523,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5727876377, 1.40186919721, N'TABAR', A0104, 1,40186919721,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5829550856, 1.44130750172, N'DASTE ROND POINT', A0118, 1,44130750172,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5926036298, 1.44979261968, N'JARDIN DES PLANTES', A0122, 1,44979261968,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6316223845, 1.45057168645, N'LA MAOURINE', A0150, 1,45057168645,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6087287359, 1.45033822289, N'BELFORT', A0156, 1,45033822289,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5993725468, 1.4549268237, N'HALLE AUX GRAINS', A0163, 1,4549268237,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5755281881, 1.49980655903, N'RIBAUTE', A0161, 1,49980655903,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6342852642, 1.45289609105, N'LA  MAOURINE 2', A0165, 1,45289609105,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.636035412, 1.44329532577, N'ERNEST RENAN 2', A0167, 1,44329532577,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5718421603, 1.41398956384, N'TABAR  PAPUS', A0172, 1,41398956384,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5776441426, 1.43932102971, N'CENTRE COMMERCIAL EMPALOT', A0173, 1,43932102971,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.564694997, 1.3971867096, N'BASTIDE  BELLEFONTAINE', A0183, 1,3971867096,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6146566538, 1.40691062103, N'SOLEIL D OR 2', A0185, 1,40691062103,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.585797723, 1.44905745079, N'ROBESPIERRE', A0188, 1,44905745079,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5891039891, 1.46499312729, N'BOISSERAIE', A0112, 1,46499312729,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6048130252, 1.44484511272, N'SQUARE DE GAULLE', A0154, 1,44484511272,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5847156328, 1.42348205897, N'BECANNE', A0196, 1,42348205897,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5696164806, 1.41577077, N'BORDELONGUE', A0205, 1,41577077,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5940262219, 1.46618924552, N'VILLA  DES  ROSIERS', A0207, 1,46618924552,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6180883631, 1.48027421615, N'ARGOULET  JMS', A0214, 1,48027421615,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5840648613, 1.4841630375, N'SCHRADER 2', A0218, 1,4841630375,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5657452873, 1.39583208845, N'LA TOURRASSE  2', A0221, 1,39583208845,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.624305413, 1.45231096477, N'AUSONNE', A0003, 1,45231096477,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6067305525, 1.47176383175, N'COQUILLE', A0013, 1,47176383175,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6174129326, 1.43854291127, N'GIESPER', A0022, 1,43854291127,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6135266437, 1.43905199967, N'GODOLIN', A0148, 1,43905199967,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6447446902, 1.43758596993, N'IBOS', A0026, 1,43758596993,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6346604902, 1.43608448462, N'PARC DE LA VACHE', A0047, 1,43608448462,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.638433721, 1.46415218784, N'PARC OZENNE', A0014, 1,46415218784,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6226916769, 1.42808261055, N'RAUZY', A0038, 1,42808261055,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6068743991, 1.40574917307, N'CHARDONNETS', A0062, 1,40574917307,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5979190442, 1.42443202082, N'COLL', A0065, 1,42443202082,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5664355104, 1.40184812524, N'FOULQUIER', A0073, 1,40184812524,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.564137008, 1.39499845632, N'LA TOURASSE', A0084, 1,39499845632,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5756644718, 1.48990346596, N'MONTAUDRAN', A0130, 1,48990346596,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6023166792, 1.4668579533, N'PINEL', A0134, 1,4668579533,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6397501668, 1.44337488641, N'FAONS', A0018, 1,44337488641,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6126227371, 1.46040257224, N'FELIX LAVIT', A0019, 1,46040257224,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5783468891, 1.38642725486, N'CEDRES', A0061, 1,38642725486,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5813210084, 1.38014157648, N'DUPOUY', A0069, 1,38014157648,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5907774711, 1.41570790134, N'PECH', A0091, 1,41570790134,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5821260546, 1.38364489001, N'PORTAL', A0094, 1,38364489001,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5601627448, 1.39524585397, N'ZONE 16', A0056, 1,39524585397,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5496268289, 1.45347606137, N'POUVOURVILLE', A0136, 1,45347606137,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5726369978, 1.45911620504, N'SACRE COEUR', A0140, 1,45911620504,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.604979561, 1.41223730486, N'PRESSENSE', A0152, 1,41223730486,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.633171836, 1.44093190678, N'PARCOURS DE SANTE', A0159, 1,44093190678,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5761350342, 1.48242262113, N'GUERRERO', A0162, 1,48242262113,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.617727552, 1.4792268511, N'ARGOULETS JUDO', A0181, 1,4792268511,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.592566083, 1.4167050706, N'LE CRISTAL', A0187, 1,4167050706,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6093284207, 1.45971659049, N'PARC  CHARLES  GASPARD', A0190, 1,45971659049,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6103894117, 1.45690324049, N'MOIROUD', A0195, 1,45690324049,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.559265018, 1.37772445357, N'REGUELONGUE', A0198, 1,37772445357,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.617046761, 1.43039322007, N'JARDIN  SAUVAGE', A0199, 1,43039322007,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6011671284, 1.48486415666, N'SQUARE ROUGENET', A0194, 1,48486415666,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6148547158, 1.42236865845, N'PONTS JUMEAUX 2', A0200, 1,42236865845,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5776710333, 1.44461268867, N'NIEL', A0202, 1,44461268867,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5767548252, 1.39696333237, N'CASTALIDES', A0206, 1,39696333237,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5644584533, 1.39980585835, N'JARDIN  LUTHER KING', A0208, 1,39980585835,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6495135211, 1.44887361881, N'GRAN SELVE', A0209, 1,44887361881,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6654749529, 1.39120958747, N'BASE DE LOISIRS  DES QUINZE SO', A0212, 1,39120958747,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5648151445, 1.40317970512, N'ROUAULT', A0204, 1,40317970512,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5673246894, 1.41095237218, N'VENDOME', A0106, 1,41095237218,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6013706808, 1.48489383316, N'CITE DE L''HERS', A0115, 1,48489383316,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6550950661, 1.41929890645, N'BASE DE LOISIRS DE SESQUIERES', A0211, 1,41929890645,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6007763408, 1.44628359654, N'PRIVAT', A0144, 1,44628359654,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6420258285, 1.43392707573, N'SEMINAIRE', A0042, 1,43392707573,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5719605453, 1.40616338084, N'MESSAGER', A0088, 1,40616338084,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5965939137, 1.4173723788, N'POLYGONE', A0093, 1,4173723788,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5779281946, 1.4411754013, N'JEAN MOULIN', A0123, 1,4411754013,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5964541884, 1.45866681224, N'LAROUSSE', A0127, 1,45866681224,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5734666709, 1.48752888944, N'PELLETRIE 2', A0133, 1,48752888944,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5732696271, 1.46197431955, N'RANGUEIL', A0137, 1,46197431955,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6020150059, 1.46387465181, N'SACCARIN', A0139, 1,46387465181,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6169868668, 1.44563690251, N'ABADIE', A0001, 1,44563690251,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6240685092, 1.43098273987, N'BOURBAKI', A0008, 1,43098273987,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6387335748, 1.44248938689, N'CHAMOIS', A0010, 1,44248938689,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6381743009, 1.4400016292, N'CITE BLANCHE', A0011, 1,4400016292,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6230610352, 1.45768389167, N'HAUTS DE BONNEFOY', A0024, 1,45768389167,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6088654322, 1.47195254742, N'SOUPETARD', A0045, 1,47195254742,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5611897219, 1.37888769988, N'CTRE ANIMATION ST-SIMON', A0066, 1,37888769988,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5697350069, 1.40618040311, N'D''INDY', A0067, 1,40618040311,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6011548506, 1.43131040786, N'RAYMOND VI', A0097, 1,43131040786,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5816368406, 1.39232265172, N'VIOLLET LE DUC', A0110, 1,39232265172,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5761628664, 1.48546005193, N'ALALOUF', A0111, 1,48546005193,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5797441192, 1.47321323177, N'BUTTE', A0114, 1,47321323177,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5819390595, 1.4418025069, N'DASTE BAT 18', A0117, 1,4418025069,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5754722113, 1.43808929471, N'EMPALOT POUDRERIE', A0120, 1,43808929471,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5887057285, 1.48393924329, N'LIMAYRAC', A0126, 1,48393924329,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5840190149, 1.48413502217, N'SCHRADER', A0143, 1,48413502217,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5979181166, 1.45232178022, N'VERDIE', A0157, 1,45232178022,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5556278187, 1.44978702175, N'PECH DAVID', A0178, 1,44978702175,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6413580137, 1.44930028626, N'JARDIN DE CHANTECLER', A0184, 1,44930028626,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5981722877, 1.43468815593, N'PLACE OLIVIER', A0192, 1,43468815593,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5689152558, 1.40336070971, N'CAMBERT', A0060, 1,40336070971,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.6240137361, 1.43720240462, N'BORDEAUX', A0007, 1,43720240462,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

INSERT INTO POINTS(X_Longitude,Y_Latitude,Texte_Point,Cle_Opendata,Num_Categorie) VALUES(43.5784269999, 1.41453788578, N'EX POSTE FAOURETTE', A0203, 1,41453788578,(SELECT Num_Categorie FROM Categories WHERE Nom_Categorie = N'Aires de jeux'))

